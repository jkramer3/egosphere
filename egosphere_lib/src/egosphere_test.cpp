/*
 * Put copyright notice here
 */

#include <egosphere/geodesic.h>
#include <egosphere/ego_search.h>
#include <egosphere/ego_vertex.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <algorithm>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>

int DEF_FREQ = 2;
int DEF_TEST = 10000;

void showHelp(std::string prg) {
	std::cout << prg << ": [-f freq (default " << DEF_FREQ << ")]";
	std::cout << " [-n tests (default " << DEF_TEST << ")]" << std::endl;
}

std::string vert_str(std::vector<egosphere::EgoVertex*> vertices) {
	std::vector<egosphere::EgoVertex*>::const_iterator it; //, nit;
	std::ostringstream oss;
	int count = 1;
	
	for (it=vertices.begin(); it<vertices.end(); it++) {
		oss << count << ":" << *it << ":\t" << (*it)->str() << std::endl;
		count++;
	}
	return oss.str();
}

egosphere::EgoVertex* minDistanceVertexExhaustive(egosphere::Biangle& b, std::vector<egosphere::EgoVertex*>& verts) {
	double d, tmp;
	egosphere::EgoVertex* ev = 0;
	d = tmp = 2*M_PI;
	std::vector<egosphere::EgoVertex*>::const_iterator it;
	for (it=verts.begin(); it<verts.end(); it++) {
		tmp = (*it)->distance(b);
		d = std::min(d, tmp);
		if (d == tmp) {
			ev = *it;
		}
	}
	return ev;
}

double minDistanceExhaustive(egosphere::Biangle& b, std::vector<egosphere::EgoVertex*> verts) {
	egosphere::EgoVertex* ev = minDistanceVertexExhaustive(b, verts);
	if (ev == 0) return M_PI;
	return ev->distance(b);
}

void test_search(int n,
                 std::vector<egosphere::EgoVertex*> egoverts,
                 egosphere::EgoSearch& egosearch) {
	int tick = n < 99999 ? 1000 : (n < 999999 ? 10000 : 50000);
	std::ostringstream oss;
	egosphere::Biangle *b;
	egosphere::Point *p;
	egosphere::EgoVertex *ex;
	int evix;
	double tsrchtot=0.0, tsrch_carttot=0.0, texhtot=0.0;
	double dsrch, dexh;
	timeval tsrchst, tsrched, tsrch_cartst, tsrch_carted, texhst, texhed;
	double t1, t2, tsrchelapsed, tsrch_cartelapsed, texhelapsed;
	double tsrchmin=9999.0, tsrchmax=0.0, tsrchavg=0.0;
	double tsrch_cartmin=9999.0, tsrch_cartmax=0.0, tsrch_cartavg=0.0;
	double texhmin=9999.0, texhmax=0.0, texhavg=0.0;
	std::cout << "Performing " << n << " randomized searches...";
	srand(time(0));
	for (int i=0; i<n; i++) {
		if (i%tick == 0) {
			std::cout << i << "..." << std::endl;
		}
		b = new egosphere::Biangle(
			(((double)(rand() % 1000000) / 1000000.0) * 2*M_PI) - M_PI,
			(((double)(rand() % 1000000) / 1000000.0) * M_PI) - (M_PI / 2.0));
		p = egosphere::Utils::calcPoint(b);
		
		gettimeofday(&tsrchst, NULL);
		evix = egosearch.nearest(*b);
		gettimeofday(&tsrched, NULL);
		t1 = tsrchst.tv_sec+(tsrchst.tv_usec/1000000.0);
		t2 = tsrched.tv_sec+(tsrched.tv_usec/1000000.0);
		tsrchelapsed = t2-t1;
		tsrchtot += tsrchelapsed;
		tsrchmin = std::min(tsrchmin, tsrchelapsed);
		tsrchmax = std::max(tsrchmax, tsrchelapsed);
		tsrchavg += tsrchelapsed;
		
		gettimeofday(&tsrch_cartst, NULL);
		evix = egosearch.nearest(*p);
		gettimeofday(&tsrch_carted, NULL);
		t1 = tsrch_cartst.tv_sec+(tsrch_cartst.tv_usec/1000000.0);
		t2 = tsrch_carted.tv_sec+(tsrch_carted.tv_usec/1000000.0);
		tsrch_cartelapsed = t2-t1;
		tsrch_carttot += tsrch_cartelapsed;
		tsrch_cartmin = std::min(tsrch_cartmin, tsrch_cartelapsed);
		tsrch_cartmax = std::max(tsrch_cartmax, tsrch_cartelapsed);
		tsrch_cartavg += tsrch_cartelapsed;
		
		gettimeofday(&texhst, NULL);
		ex = minDistanceVertexExhaustive(*b, egoverts);
		gettimeofday(&texhed, NULL);
		t1 = texhst.tv_sec+(texhst.tv_usec/1000000.0);
		t2 = texhed.tv_sec+(texhed.tv_usec/1000000.0);
		texhelapsed = t2-t1;
		texhtot += texhelapsed;
		texhmin = std::min(texhmin, texhelapsed);
		texhmax = std::max(texhmax, texhelapsed);
		texhavg += texhelapsed;
		
		if (evix >= 0) {
			if (egoverts[evix] != ex) {
				dsrch = egoverts[evix]->distance(*b);
				dexh  = ex->distance(*b);
				oss << "Searched for (" << b->str(15) << "): ";
				if (dsrch != dexh) {
					oss << "*** INCORRECT! ***" << std::endl;
				} else {
					oss << "same distance, different vertices" << std::endl;
				}
				oss << "\tFound (" << egoverts[evix]->ang_str(15) << "), d=" << egosphere::toDeg(dsrch) << std::endl;
				oss << "\tExhau (" << ex->ang_str(15) << "), d=" << egosphere::toDeg(dexh) << std::endl;
			}
		} else {
			oss << "Searched for (" << b->str(15) << "): MISSING!" << std::endl;
			dexh  = ex->distance(*b);
			oss << "  Exhaustive is (" << ex->ang_str(15) << "), d=" << egosphere::toDeg(dexh) << std::endl;
		}
		delete b;
		delete p;
	}
	std::cout << oss.str() << std::endl;
	std::cout << "Total of " << n << " locations; total search=" << tsrchtot;
	std::cout << ", total exhaust=" << texhtot << std::endl;
	std::cout << "Search times:  min / max / avg -> " << (tsrchmin*1000) << "\t/ ";
	std::cout << (tsrchmax*1000) << "\t/ " << (tsrchavg * 1000 / n) << std::endl;
	std::cout << "Cartesn times: min / max / avg -> " << (tsrch_cartmin*1000) << "\t/ ";
	std::cout << (tsrch_cartmax*1000) << "\t/ " << (tsrch_cartavg * 1000 / n) << std::endl;
	std::cout << "Exhaust times: min / max / avg -> " << (texhmin*1000) << "\t/ ";
	std::cout << (texhmax*1000) << "\t/ " << (texhavg*1000 / n) << std::endl;
}
	
/** Construct the geodesic structures (vertex vector and search object),
 * display its information, transform them into pared-down objects, then
 * run randomized point-location queries to test. */
int main(int argc, char **argv) {
	int freq;
	int test_num;
	
	freq = DEF_FREQ;
	test_num = DEF_TEST;
	
	for (int i=1; i<argc; i++) {
		if (strcmp(argv[i], "-h") == 0) {
			showHelp(argv[0]);
			exit(0);
		} else if (i+1 != argc) {
			if (strcmp(argv[i], "-f") == 0) {
				freq = atoi(argv[++i]);
			} else if (strcmp(argv[i], "-n") == 0) {
				test_num = atoi(argv[++i]);
			} else {
				std::cout << "Missing arguments" << std::endl;
				exit(1);
			}
		}
	}
	
	std::cout << "Constructing geosphere (freq=" << freq << ")" << std::endl;
	egosphere::Geosphere geo(freq);
	std::cout << std::endl << geo.metainfo_str() << std::endl;
	std::cout << geo.search_sum_str() << std::endl;
	//std::cout << "Geosphere vertices:" << std::endl;
	//std::cout << geo.vert_str() << std::endl;
	//std::cout << "(Should have printed " << (10 * pow(geo.frequency(), 2) + 2) << " vertices)" << std::endl;
	//std::cout << "Geosphere search structure:" << std::endl;
	//std::cout << geo.search_str(9) << std::endl;
	
	std::cout << "Constructing egosphere from geosphere" << std::endl;
	//std::vector<egosphere::EgoVertex*> ego_verts;
	std::vector<egosphere::SesVertex*> ego_verts;
	egosphere::EgoSearch ego_search = geo.getEgoStructs(ego_verts);
	//std::cout << "Egosphere vertices:" << std::endl;
	//std::cout << vert_str(ego_verts) << std::endl;
	//std::cout << "Egosearch structure:" << std::endl;
	//std::cout << ego_search.str() << std::endl;
	
	test_search(test_num, ego_verts, ego_search);
	
	return 0;
}
