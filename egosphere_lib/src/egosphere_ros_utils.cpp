/*
 * Put copyright notice here
 */

#include <egosphere/egosphere_ros_utils.hpp>
//#include <egosphere/egosphere_utils.h>
#include <egosphere/ego_vertex.h>
#include <egosphere/ego_path.hpp>
#include <egosphere/ego_search.h>
#include <tf/transform_datatypes.h>
#include <iostream>

namespace egosphere {
	
	std::string RosUtils::showOrientations(bool deg, bool rpy) {
		return showOrientations(M_PI/2.0, M_PI/4.0, deg, rpy);
	}
	
	std::string RosUtils::showOrientations(double thInc, double phInc,
	                                       bool deg, bool rpy) {
		std::ostringstream oss;
		if (deg) {
			oss << std::fixed << std::setprecision(2);
		} else {
			oss << std::fixed << std::setprecision(6);
		}
		geometry_msgs::Quaternion q;
		Biangle b(0.0, 0.0);
		while (b.phi <= M_PI/2.0) {
		//while (b.phi >= -M_PI/2.0) {
			oss << "--- start phi=" << (deg ? toDeg(b.phi) : b.phi) << std::endl;
			while (b.theta < 2*M_PI) {
				double nTh = unsignZero(norm_ang(-b.theta));
				oss << "biangle [" << (deg ? toDeg(b.theta) : b.theta) << ", ";
				oss << (deg ? toDeg(b.phi) : b.phi) << "], orientation:" << std::endl;
				if (rpy) {
					double r, p, y;
					biangleToRPY(&b, r, p, y);
					oss << "  r: " << r << std::endl;
					oss << "  p: " << (deg ? toDeg(p) : p) << std::endl;
					oss << "  y: " << (deg ? toDeg(y) : y) << std::endl;
				} else {
					biangleToQuaternionMsg(&b, q);
					oss << std::fixed << std::setprecision(6);
					oss << q;
					if (deg) {
						oss << std::fixed << std::setprecision(2);
					}
				}
				if (anglesWithin(b.phi, M_PI/2.0, 2.8e-8)) {
				//if (anglesWithin(b.phi, -M_PI/2.0, 2.8e-8)) {
					b.theta = 2*M_PI;
				} else {
					b.theta += thInc;
				}
			}
			b.theta = 0.0;
			b.phi += phInc;
			//b.phi -= phInc;
		}
		return oss.str();
	}
	
	void RosUtils::timeToRosTime(const Time& ego, ros::Time& ros) {
		ros.sec = ego.sec;
		ros.nsec = ego.nsec;
	}
	
	void RosUtils::timeRosToTime(const ros::Time& ros, Time& ego) {
		ego.sec = ros.sec;
		ego.nsec = ros.nsec;
	}
	
	std::string RosUtils::coordinateStr(const egosphere_msgs::Coordinate &msg,
	                                    bool nomag) {
		std::stringstream oss;
		oss << toDeg(msg.theta) << ", " << toDeg(msg.phi);
		if (!nomag) oss << ", " << msg.mag;
		return oss.str();
	}
	
	std::string RosUtils::coordinateStr(const egosphere_msgs::Coordinate* msg,
	                                    bool nomag) {
		std::stringstream oss;
		oss << toDeg(msg->theta) << ", " << toDeg(msg->phi);
		if (!nomag) oss << ", " << msg->mag;
		return oss.str();
	}
	
	void RosUtils::pointToPointMsg(const Point &p, geometry_msgs::Point &msg) {
		msg.x = p.x; msg.y = unsignZero(-p.y); msg.z = p.z;
	}
	
	void RosUtils::pointToPointMsg(Point const *p, geometry_msgs::Point &msg) {
		pointToPointMsg(*p, msg);
	}
	
	void RosUtils::pointMsgToPoint(geometry_msgs::Point const &msg, Point &p) {
		p.x = msg.x; p.y = unsignZero(-msg.y); p.z = msg.z;
	}
	
	void RosUtils::stampedPointToPointStampedMsg(const StampedPoint& p,
	                                             geometry_msgs::PointStamped &msg) {
		timeToRosTime(p.t, msg.header.stamp);
		pointToPointMsg(p, msg.point);
	}
	
	void RosUtils::pointStampedMsgToStampedPoint(const geometry_msgs::PointStamped& msg,
	                                             StampedPoint& p) {
		timeRosToTime(msg.header.stamp, p.t);
		pointMsgToPoint(msg.point, p);
	}
	
	void RosUtils::biangleToPointMsg(Biangle const *b, geometry_msgs::Point &msg, double radius) {
		Point* p = Utils::calcPoint(b, radius);
		pointToPointMsg(p, msg); 
		delete p;
	}
	
	void RosUtils::biangleMsgToPointMsg(const egosphere_msgs::BiangleMsg &b,
	                                 geometry_msgs::Point &msg, double radius) {
		Biangle b1;
		biangleMsgToBiangle(b, b1);
		biangleToPointMsg(&b1, msg, radius);
	}
	
	void RosUtils::vertToPointMsg(EgoVertex const *vert,
	                              geometry_msgs::Point &msg, double radius) {
		// vertex's biangle represents (theta,phi) projection from origin;
		// want the point at 'radius' distance out from origin
		biangleToPointMsg(vert->getBiangle(), msg, radius);
	}
	
	void RosUtils::biangleToQuaternionMsg(Biangle const *b,
	                            geometry_msgs::Quaternion &msg) {
		double adjTheta = unsignZero(norm_ang(-b->theta));
		double adjPhi = unsignZero(norm_ang(-b->phi));
		msg = tf::createQuaternionMsgFromRollPitchYaw(0.0, adjPhi, adjTheta);
	}
	
	void RosUtils::biangleMsgToQuaternionMsg(const egosphere_msgs::BiangleMsg &b,
	                            geometry_msgs::Quaternion &msg) {
		Biangle b1;
		biangleMsgToBiangle(b, b1);
		biangleToQuaternionMsg(&b1, msg);
	}
	
	void RosUtils::vertToQuaternionMsg(EgoVertex const *vert,
	                                   geometry_msgs::Quaternion &msg) {
		biangleToQuaternionMsg(vert->getBiangle(), msg);
	}
	
	void RosUtils::biangleToRPY(Biangle const *b,
	                            double &r, double &p, double &y) {
		//egosphere_msgs::BiangleMsg msg;
		//biangleToBiangleMsg(*b, msg);
		//biangleMsgToRPY(msg, r, p, y);
		geometry_msgs::Quaternion q;
		biangleToQuaternionMsg(b, q);
		tf::Quaternion tq;
		tf::quaternionMsgToTF(q, tq);
		tf::Matrix3x3(tq).getRPY(r, p, y);
		unsignZero(r);
		unsignZero(p);
		unsignZero(y);
	}
	
	void RosUtils::biangleMsgToRPY(const egosphere_msgs::BiangleMsg &b,
	                               double &r, double &p, double &y) {
		Biangle b1;
		biangleMsgToBiangle(b, b1);
		biangleToRPY(&b1, r, p, y);
		/*
		geometry_msgs::Quaternion q;
		biangleMsgToQuaternionMsg(b, q);
		tf::Quaternion tq;
		tf::quaternionMsgToTF(q, tq);
		tf::Matrix3x3(tq).getRPY(r, p, y);
		unsignZero(r);
		unsignZero(p);
		unsignZero(y);
		*/
	}
	
	void RosUtils::biangleToPoseMsg(Biangle const *b, geometry_msgs::Pose &msg, double radius) {
		//ROS_INFO_STREAM("biangleToPoseMsg b: [" << b->str() << "]");
		biangleToPointMsg(b, msg.position, radius);
		biangleToQuaternionMsg(b, msg.orientation);
		//ROS_INFO_STREAM("biangleToPoseMsg msg:" << std::endl << msg);
	}
	
	void RosUtils::biangleMsgToPoseMsg(const egosphere_msgs::BiangleMsg &b,
	                                   geometry_msgs::Pose &msg, double radius) {
		biangleMsgToPointMsg(b, msg.position, radius);
		biangleMsgToQuaternionMsg(b, msg.orientation);
	}
	
	void RosUtils::vertToPoseMsg(EgoVertex const *vert, geometry_msgs::Pose &msg,
	                             double radius) {
		biangleToPoseMsg(vert->getBiangle(), msg, radius);
	}
	
	std::string RosUtils::biangleMsgStr(const egosphere_msgs::BiangleMsg &msg, bool nomag) {
		std::stringstream oss;
		oss << toDeg(msg.theta) << ", " << toDeg(msg.phi);
		if (!nomag) oss << ", " << msg.mag;
		return oss.str();
	}
	
	void RosUtils::biangleMsgToBiangle(const egosphere_msgs::BiangleMsg &msg, Biangle &b) {
		b.theta = msg.theta;
		b.phi = msg.phi;
		b.mag = msg.mag;
		b.costh = msg.costh;
		b.sinth = msg.sinth;
		b.cosph = msg.cosph;
		b.sinph = msg.sinph;
	}
	
	void RosUtils::biangleToBiangleMsg(const Biangle &b, egosphere_msgs::BiangleMsg &msg) {
		msg.theta = b.theta;
		msg.phi = b.phi;
		msg.mag = b.mag;
		msg.costh = b.costh;
		msg.sinth = b.sinth;
		msg.cosph = b.cosph;
		msg.sinph = b.sinph;
	}
	
	std::string RosUtils::stampedBiangleMsgStr(const egosphere_msgs::StampedBiangleMsg &msg,
	                                           bool nomag) {
		return biangleMsgStr(msg.biangle, nomag);
	}
	
	void RosUtils::stampedBiangleToStampedBiangleMsg(const StampedBiangle &sb,
   	                                    egosphere_msgs::StampedBiangleMsg &msg) {
		msg.sec = sb.t.sec;
		msg.nsec = sb.t.nsec;
		biangleToBiangleMsg(sb, msg.biangle);
	}
	
	void RosUtils::stampedBiangleMsgToStampedBiangle(const egosphere_msgs::StampedBiangleMsg &msg,
	                                       StampedBiangle &sb) {
		sb.t.sec = msg.sec;
		sb.t.nsec = msg.nsec;
		biangleMsgToBiangle(msg.biangle, sb);
	}
	
	void RosUtils::egoPathToStampedBiangleMsgArray(const EgoPath &ep,
	                                     egosphere_msgs::StampedBiangleMsgArray &msg) {
		msg.biangles.clear();
		std::vector<StampedBiangle> sb_v;
		ep.toVector(sb_v);
		for (unsigned int i=0; i<sb_v.size(); i++) {
			egosphere_msgs::StampedBiangleMsg sb_msg;
			stampedBiangleToStampedBiangleMsg(sb_v[i], sb_msg);
			msg.biangles.push_back(sb_msg);
		}
	}
	
	void RosUtils::stampedBiangleMsgArrayToEgoPath(const egosphere_msgs::StampedBiangleMsgArray &msg,
	                                     EgoPath &ep) {
		ep.clear();
		for (unsigned int i=0; i<msg.biangles.size(); i++) {
			StampedBiangle sb;
			stampedBiangleMsgToStampedBiangle(msg.biangles[i], sb);
			ep.pushBack(sb);
		}
	}
	
	void RosUtils::fillFaceMarkerPoints(const Point& egoPoint,
	                                    const std::vector<Point>& neighbors,
	                                    Marker& faceMarker) {
		faceMarker.points.clear();
		geometry_msgs::Point rp1, rp2, rms;
		pointToPointMsg(egoPoint, rms);
		unsigned int ixStart = neighbors.size()-1;
		pointToPointMsg(neighbors[ixStart], rp1);
		for (unsigned int i=0; i<ixStart; ++i) {
			pointToPointMsg(neighbors[i], rp2);
			faceMarker.points.push_back(rp1);
			faceMarker.points.push_back(rp2);
			faceMarker.points.push_back(rms);
			rp1 = rp2;
		}
		pointToPointMsg(neighbors[ixStart], rp2);
		faceMarker.points.push_back(rms);
		faceMarker.points.push_back(rp1);
		faceMarker.points.push_back(rp2);
	}
	
	void RosUtils::fillFacePointMsgVec(EgoVertex const *ev,
	                                   std::vector<geometry_msgs::Point> &vec) {
		vec.clear();
		geometry_msgs::Point rp1, rp2, rms;
		unsigned int ixStart = ev->numNeighbors()-1;
		pointToPointMsg(ev->getPoint(), rms);
		pointToPointMsg(ev->getNeighborByIndex(ixStart)->getPoint(), rp1);
		for (unsigned int i=0; i<ixStart; ++i) {
			pointToPointMsg(ev->getNeighborByIndex(i)->getPoint(), rp2);
			vec.push_back(rp1);
			vec.push_back(rp2);
			vec.push_back(rms);
			rp1 = rp2;
		}
		pointToPointMsg(ev->getNeighborByIndex(ixStart)->getPoint(), rp2);
		vec.push_back(rms);
		vec.push_back(rp1);
		vec.push_back(rp2);
	}
	
	void RosUtils::fillMarkerFace(EgoVertex const *ev, Marker &m) {
		m.points.clear();
		geometry_msgs::Point rp1, rp2, rme;
		pointToPointMsg(ev->getPoint(), rme);
		pointToPointMsg(ev->getNeighborByIndex(0)->getPoint(), rp1);
		// go backwards for gl draw ordering (convex highlighting)
		for (unsigned int i=(ev->numNeighbors()-1); i>0; i--) {
			pointToPointMsg(ev->getNeighborByIndex(i)->getPoint(), rp2);
			m.points.push_back(rme);
			m.points.push_back(rp1);
			m.points.push_back(rp2);
			rp1 = rp2;
		}
		pointToPointMsg(ev->getNeighborByIndex(0)->getPoint(), rp2);
		m.points.push_back(rme);
		m.points.push_back(rp1);
		m.points.push_back(rp2);
		biangleToPointMsg(ev->getBiangle(), m.pose.position, ev->getBiangle()->mag);
		m.pose.orientation.w = 1.0;
	}
	
	void RosUtils::fillMarkerEgoVerts(const std::string &frame, double radius,
		                        const std::vector<EgoVertex*> &verts,
										const EgoSearch &search,
	                           MarkerArray &marr) {
		marr.markers.clear();
		// - set up forward and up markers with directional indicators
		// the sphere 'top'
		Biangle biFront(0.0, 0.0, true);
		Biangle biLeft(-M_PI/2.0, 0.0, true);
		//ROS_INFO("fillVertMarkers: *******>>> up marker");
		Biangle biUp(0.0, M_PI/2.0, true);
		//ROS_INFO("fillVertMarkers: <<<******* up marker");
		unsigned int ixFront = search.nearest(biFront);
		unsigned int ixLeft = search.nearest(biLeft);
		unsigned int ixUp = search.nearest(biUp);
		
		// - put in center marker
		Marker ev_c;
		ev_c.id = 0;
		geometry_msgs::Pose ev_pose;
		ev_pose.orientation.w = 1.0;
		initMarker(highlite::CENTER, frame, ev_pose, ev_c);
		ev_c.scale = RosUtils::getScale(highlite::CENTER, 0.1);
		marr.markers.push_back(ev_c);
		
		// - set up a POINTS marker for regular vertices
		//   note: re-using 'ev_pose'
		Marker ev_m;
		ev_m.id = 1;
		initMarker(highlite::VERTEX, frame, ev_pose, ev_m);
		ev_m.scale = RosUtils::getScale(highlite::VERTEX, radius);
		for (unsigned int i=0; i<verts.size(); i++) {
			if (i != ixFront && i != ixUp) {
				geometry_msgs::Point rosp;
				vertToPointMsg(verts[i], rosp, radius);
				ev_m.points.push_back(rosp);
			}
		}
		marr.markers.push_back(ev_m);
		
		// - put in directional indicators
		//   note: re-using 'ev_pose'
		Marker ev_f;
		ev_f.id = 2;
		biangleToPoseMsg(&biFront, ev_pose);
		initMarker(highlite::DIRECTION, frame, ev_pose, ev_f);
		ev_f.color.r = 1.0; ev_f.color.g = 0.0;
		ev_f.scale = RosUtils::getScale(highlite::DIRECTION, 0.3 * radius);
		ev_f.pose.position.x = ev_f.pose.position.x - ev_f.scale.x;
		marr.markers.push_back(ev_f);
		
		Marker ev_l;
		ev_l.id = 3;
		biangleToPoseMsg(&biLeft, ev_pose);
		initMarker(highlite::DIRECTION, frame, ev_pose, ev_l);
		ev_l.color.r = 0.0; ev_l.color.g = 1.0;
		ev_l.scale = RosUtils::getScale(highlite::DIRECTION, 0.3 * radius);
		ev_l.pose.position.y = ev_l.pose.position.y - ev_l.scale.x;
		marr.markers.push_back(ev_l);
		
		//ROS_INFO_STREAM("fillVertMarkers: *******>>> up marker, b [" << biUp.str() << "]");
		Marker ev_t;
		ev_t.id = 4;
		biangleToPoseMsg(&biUp, ev_pose);
		initMarker(highlite::DIRECTION, frame, ev_pose, ev_t);
		ev_t.color.b = 1.0; ev_t.color.g = 0.0;
		ev_t.scale = RosUtils::getScale(highlite::DIRECTION, 0.3 * radius);
		ev_t.pose.position.z = ev_t.pose.position.z - ev_t.scale.x;
		marr.markers.push_back(ev_t);
		//ROS_INFO("fillVertMarkers: <<<******* up marker");
		
		/*
		ROS_INFO("fillMarkerEgoVerts: **** 45 marker");
		Marker ev_45;
		ev_45.id = 5;
		biangleToPoseMsg(&bi45, ev_pose);
		initMarker(highlite::DIRECTION, frame, ev_pose, ev_45);
		ev_45.color.b = 1.0; ev_45.color.g = 1.0;
		ev_45.scale = RosUtils::getScale(highlite::DIRECTION, 0.3 * radius);
		//biangleToQuaternionMsg(&bi45, ev_45.pose.orientation);
		//ev_45.pose.position.x = ev_45.pose.position.x - ev_45.scale.x;
		//ev_45.pose.position.y = ev_45.pose.position.y - ev_45.scale.x;
		//ev_45.pose.position.z = ev_45.pose.position.z - ev_45.scale.x;
		marr.markers.push_back(ev_45);
		
		ROS_INFO("fillMarkerEgoVerts: **** global z marker");
		Marker ev_z;
		ev_z.id = 6;
		ev_pose = ev_t.pose;
		ev_pose.position.x += 0.5;
		initMarker(highlite::DIRECTION, frame, ev_pose, ev_z);
		//ev_z.header.frame_id = "gloabl";
		ev_z.color.b = 1.0; ev_z.color.g = 0.8;
		ev_z.scale = RosUtils::getScale(highlite::DIRECTION, 0.3 * radius);
		ev_z.pose.orientation.x = 0.0;
		ev_z.pose.orientation.y = -0.7071;
		ev_z.pose.orientation.z = 0.0;
		ev_z.pose.orientation.w = 0.7071;
		marr.markers.push_back(ev_z);
		
		ROS_INFO("fillMarkerEgoVerts: **** global 45 marker");
		Marker ev_g;
		ev_g.id = 7;
		ev_pose = ev_45.pose;
		ev_pose.position.x += 0.5;
		initMarker(highlite::DIRECTION, frame, ev_pose, ev_g);
		ev_g.header.frame_id = "global";
		ev_g.color.b = 0.8; ev_z.color.g = 1.0;
		ev_g.scale = RosUtils::getScale(highlite::DIRECTION, 0.3 * radius);
		ev_g.pose.orientation.x = 0.0;
		ev_g.pose.orientation.y = 0.3728217;
		ev_g.pose.orientation.z = 0.3728217;
		ev_g.pose.orientation.w = 0.8497105;
		marr.markers.push_back(ev_g);
		*/
		
		/*
		// *
		geometry_msgs::Point rme;
		RosUtils::pointToPointMsg(pMe-pMe, rme);
		m.points.push_back(rme);
		RosUtils::biangleToPointMsg(&biangle, m.pose.position, radius);
		m.scale.x = 0.04 * radius;
		m.scale.y = 0.04 * radius;
		m.scale.z = 0.04 * radius;
		m.ns = "RVizVertex";
		m.action = visualization_msgs::Marker::MODIFY;
		m.pose.orientation.w = 1.0;
		m.color.r = 0;
		m.color.g = 1;
		m.color.b = 1;
		m.color.a = 1;
		*/
	}
	
	void RosUtils::initMarker(highlite::MarkerType type,
	                          const std::string &frame,
	                          const geometry_msgs::Pose &pose, Marker &m) {
		m.header.frame_id = frame;
		m.action = Marker::MODIFY;
		m.pose = pose;
		m.color = RosUtils::getColor(type, ros::Duration(1), ros::Duration(1));
		m.scale = RosUtils::getScale(type, 0.05);
		switch (type) {
		case highlite::VERTEX:
			m.type = Marker::POINTS;
			m.ns = "EgoVertex";
			m.lifetime = ros::Duration(0);
			break;
		case highlite::DIRECTION:
			m.type = Marker::ARROW;
			m.ns = "EgoRay";
			m.lifetime = ros::Duration(0);
			break;
		case highlite::DIR_ARROW:
			m.type = Marker::ARROW;
			m.ns = "EgoRay";
			m.lifetime = ros::Duration(0);
		case highlite::DIR_SHAFT:
			m.type = Marker::CYLINDER;
			m.ns = "EgoRay";
			m.lifetime = ros::Duration(0);
			break;
		case highlite::CENTER:
			m.type = Marker::CUBE;
			m.ns = "EgoCenter";
			m.lifetime = ros::Duration(0);
			break;
		case highlite::FACE:
			m.type = Marker::TRIANGLE_LIST;
			m.ns = "EgoFace";
			m.scale.x = 1.0;
			m.scale.y = 1.0;
			m.scale.z = 1.0;
			break;
		case highlite::READING:
			m.type = Marker::SPHERE;
			m.ns = "EgoData";
			break;
		case highlite::IMPRESSION:
			m.type = Marker::CYLINDER;
			m.ns = "EgoImpression";
			m.scale.x = 0.1;
			m.scale.y = 0.1;
			m.scale.z = 0.1;
			break;
		case highlite::SPHERES:
			m.type = Marker::SPHERE_LIST; // eventually, make arrow
			m.ns = "EgoBalls";
			m.lifetime = ros::Duration(0);
			break;
		default:
			m.type = Marker::CUBE;
			m.lifetime = ros::Duration(0);
			break;
		}
	}
	
	std_msgs::ColorRGBA RosUtils::getColor(highlite::MarkerType type,
	                                      const ros::Duration &decay,
	                                      const ros::Duration &tleft) {
		std_msgs::ColorRGBA color;
		switch (type) {
		case highlite::VERTEX:
			color.r = 0.8;//1.0;
			color.g = 0.2;//1.0;
			color.b = 1.0;
			color.a = 0.7;//0.3;
			break;
		case highlite::DIRECTION:
		case highlite::DIR_ARROW:
		case highlite::DIR_SHAFT:
			color.r = 0.0;
			color.g = 1.0;
			color.b = 0.0;
			color.a = 1.0;
			break;
		case highlite::CENTER:
			color.r = 0.6;
			color.g = 0.6;
			color.b = 0.6;
			color.a = 1.0;
			break;
		case highlite::FACE:
			color.r = 0.0;
			color.g = 1.0;
			color.b = 1.0;
			color.a = 1.0;
			break;
		case highlite::READING:
			color.r = 1.0;
			color.g = 0.0;
			color.b = 0.0;
			color.a = tleft.toSec() / decay.toSec();
			break;
		case highlite::SPHERES:
			color.r = 1.0;
			color.g = 0.0;
			color.b = 0.0;
			color.a = 0.8;
			break;
		default:
			color.r = 0.0;
			color.g = 0.0;
			color.b = (float)(204/255);
			color.a = tleft.toSec() / decay.toSec();
			break;
		}
		return color;
	}
	
	geometry_msgs::Vector3 RosUtils::getScale(highlite::MarkerType type, double unit) {
		geometry_msgs::Vector3 scale;
		double pscale;
		switch (type) {
		case highlite::VERTEX:
			pscale = 0.02 * unit;
			scale.x = pscale; scale.y = pscale; scale.z = pscale;
			break;
		case highlite::DIRECTION:
			scale.x = unit; scale.y = 0.1 * unit; scale.z = 0.1 * unit;
			break;
		case highlite::DIR_ARROW:
			scale.x = unit; scale.y = 0.1 * unit; scale.z = 0.1 * unit;
			break;
		case highlite::DIR_SHAFT:
			scale.x = 0.1 * unit; scale.y = 0.1 * unit; scale.z = unit;
			break;
		//case highlite::FACE:
		//	break;
		//case highlite::READING:
		//	break;
		//case highlite::SPHERES:
		//	break;
		default:
			scale.x = unit; scale.y = unit; scale.z = unit;
			break;
		}
		return scale;
	}
	
};

