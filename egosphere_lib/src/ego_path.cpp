/*
 * Put copyright notice here
 */
#include <egosphere/ego_path.hpp>
#include <egosphere/egosphere_utils.h>
#include <algorithm>

namespace egosphere {
	
	EgoPath::EgoPath() { }
	
	EgoPath::EgoPath(std::list<StampedBiangle> steps)
			: m_path(steps) { }
	
	std::list<StampedBiangle>::iterator EgoPath::begin() {
		return m_path.begin();
	}
	
	std::list<StampedBiangle>::const_iterator EgoPath::begin() const {
		return m_path.begin();
	}
	
	std::list<StampedBiangle>::iterator EgoPath::end() {
		return m_path.end();
	}
	
	std::list<StampedBiangle>::const_iterator EgoPath::end() const {
		return m_path.end();
	}
	
	bool EgoPath::empty() const {
		return m_path.empty();
	}
	
	unsigned int EgoPath::size() const {
		return m_path.size();
	}
	
	const StampedBiangle& EgoPath::peekFront() const {
		return m_path.front();
	}
	
	const StampedBiangle& EgoPath::peekBack() const {
		return m_path.back();
	}
	
	void EgoPath::clear() {
		m_path.clear();
	}
	
	StampedBiangle EgoPath::popFront() {
		StampedBiangle sb(m_path.front());
		m_path.pop_front();
		return sb;
	}
	
	int EgoPath::popFront(int n) {
		// TODO: perhaps return a list of removed values?
		if (n <= 0) return 0;
		int pops = std::min(n, (int)m_path.size());
		std::list<StampedBiangle>::iterator it = m_path.begin();
		std::advance(it, pops);
		m_path.erase(m_path.begin(), it);
		return pops;
	}
	
	StampedBiangle EgoPath::popBack() {
		StampedBiangle sb(m_path.back());
		m_path.pop_back();
		return sb;
	}
	
	int EgoPath::popBack(int n) {
		// TODO: perhaps return a list of removed values?
		if (n <= 0) return 0;
		int pops = std::min(n, (int)m_path.size());
		std::list<StampedBiangle>::iterator it = m_path.end();
		std::advance(it, -pops);
		m_path.erase(it, m_path.end());
		return pops;
	}
	
	void EgoPath::pushFront(const StampedBiangle &b) {
		this->m_path.insert(m_path.begin(), b);
	}
	
	void EgoPath::pushBack(const StampedBiangle &b) {
		this->m_path.insert(m_path.end(), b);
	}
	
	void EgoPath::pushFront(const std::list<StampedBiangle> &l) {
		this->m_path.insert(m_path.begin(), l.begin(), l.end());
	}
	
	void EgoPath::pushBack(const std::list<StampedBiangle> &l) {
		this->m_path.insert(m_path.end(), l.begin(), l.end());
	}
	
	Time EgoPath::timespan() {
		Time t(m_path.back().t);
		t -= m_path.front().t;
		return t;
	}
	
	void EgoPath::toVector(std::vector<StampedBiangle> &v) const {
		v.clear();
		v.reserve(m_path.size());
		std::list<StampedBiangle>::const_iterator it;
		for (it=m_path.begin(); it!=m_path.end(); it++) {
			v.push_back(*it);
		}
	}
	
};

