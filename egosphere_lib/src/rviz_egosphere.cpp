/**
 * Put copyright notice here
 */
#include <egosphere/rviz_egosphere.hpp>

namespace egosphere {
	
	RVizEgosphere::RVizEgosphere(int f)
			: Egosphere<RVizVertex>(f)
	{ }
	
	void RVizEgosphere::stuffMarker(const int id, Marker &m, double radius,
	                                marker_type::MarkerType t) {
		if (isId(id)) {
			m_egoVerts[id]->stuffMarker(m, radius, t);
		}
	}
	
	void RVizEgosphere::fillEgoVertsMarkerArray(const std::string &frame,
						MarkerArray &marr, double radius) {
		// get the nearest vertices to the compass points (to filter out)
		std::map<unsigned int, RVizVertex*> cpVerts;
		std::vector<CompassInfo> cpIxes;
		getCompassVerts(cpIxes);
		for (unsigned int i=0; i<cpIxes.size(); i++) {
			cpVerts[i] = m_egoVerts[cpIxes[i].cpIx];
		}
		marr.markers.clear();
		// set up a marker for vertices, filtering out compass points
		Marker ev_m, ev_tmp;
		ev_m.header.frame_id = frame;
		ev_m.ns = "EgoVertices";
		ev_m.id = 0;
		ev_m.type = visualization_msgs::Marker::POINTS;
		ev_m.action = visualization_msgs::Marker::MODIFY;
		ev_m.pose.orientation.w = 1.0;
		//ev_m.scale.x = 0.01; ev_m.scale.y = 0.01; ev_m.scale.z = 0.01;
		double pscale = 0.02 * radius;
		ev_m.scale.x = pscale; ev_m.scale.y = pscale; ev_m.scale.z = pscale;
		ev_m.color.r = 1; ev_m.color.g = 1; ev_m.color.b = 1; ev_m.color.a = 0.3;
		ev_m.lifetime = ros::Duration(0);
		ev_m.points.clear();
		for (unsigned int i=0; i<m_egoVerts.size(); i++) {
			if (cpVerts.count(i) < 1) {
				m_egoVerts[i]->stuffMarker(ev_tmp, radius, marker_type::SPHERE);
				ev_m.points.push_back(ev_tmp.pose.position);
			}
		}
		marr.markers.push_back(ev_m);
		// fill in the compass point vertices
		ev_m.id = 1;
		ev_m.type = visualization_msgs::Marker::SPHERE_LIST;
		//ev_m.scale.x = 0.02; ev_m.scale.y = 0.02; ev_m.scale.z = 0.02;
		pscale = 0.04 * radius;
		ev_m.scale.x = pscale; ev_m.scale.y = pscale; ev_m.scale.z = pscale;
		ev_m.points.clear();
		ev_tmp = ev_m;
		std::map<unsigned int, RVizVertex*>::iterator it;
		for (it=cpVerts.begin(); it!=cpVerts.end(); it++) {
			it->second->stuffMarker(ev_m, radius, marker_type::SPHERE);
			ev_tmp.colors.push_back(cpIxes[it->first].color);
			ev_tmp.points.push_back(ev_m.pose.position);
		}
		marr.markers.push_back(ev_tmp);
	}
	
	void RVizEgosphere::getCompassVerts(std::vector<CompassInfo> &ixvec) {
		CompassInfo ci;
		Biangle b;
		// the sphere 'top'
		b = Biangle(0.0, -M_PI/2, false);
		ci.cpIx = m_egoSearch.nearest(b);
		ci.color.r = 0.5; ci.color.g = 0.5; ci.color.b = 0.5; ci.color.a = 0.8;
		ixvec.push_back(ci);
		// the sphere 'bottom'
		b = Biangle(0.0, M_PI/2, false);
		ci.cpIx = m_egoSearch.nearest(b);
		ci.color.r = 1; ci.color.g = 1; ci.color.b = 1; ci.color.a = 0.8;
		ixvec.push_back(ci);
		
		// get the 4 prime horizon points
		b = Biangle(0.0, 0.0);
		ci.cpIx = m_egoSearch.nearest(b);
		ci.color.r = 0.2; ci.color.g = 1; ci.color.b = 0.2; ci.color.a = 0.5;
		ixvec.push_back(ci);
		b = Biangle(-M_PI/2, 0.0);
		ci.cpIx = m_egoSearch.nearest(b);
		ci.color.r = 0.2; ci.color.g = 0.2; ci.color.b = 1; ci.color.a = 0.5;
		ixvec.push_back(ci);
		b = Biangle(M_PI, 0.0);
		ci.cpIx = m_egoSearch.nearest(b);
		ci.color.r = 1; ci.color.g = 0.2; ci.color.b = 1; ci.color.a = 0.5;
		ixvec.push_back(ci);
		b = Biangle(M_PI/2, 0.0);
		ci.cpIx = m_egoSearch.nearest(b);
		ci.color.r = 1; ci.color.g = 0.2; ci.color.b = 0.2; ci.color.a = 0.5;
		ixvec.push_back(ci);
		
		// any additional points
		/* something's not right in one of these...wipes out an entire
		 * quarter arc of non-compass verts (from phi=0 to phi=90)
		 * to heck with it...it looks better with less points
		ci.color.r = 1; ci.color.g = 1; ci.color.b = 0.8; ci.color.a = 0.4;
		// secondary horizon points
		for (double t=-(7*M_PI/8); t<2*M_PI; t+=M_PI/8) {
			if (t != -M_PI/2 && t != M_PI/2) {
				b = Biangle(t, 0.0);
				ci.cpIx = m_egoSearch.nearest(b);
				ixvec.push_back(ci);
			}
		}
		// phi=M_PI/6 points
		for (double t=-M_PI; t<2*M_PI; t+=M_PI/4) {
			b = Biangle(t, M_PI/4);
			ci.cpIx = m_egoSearch.nearest(b);
			ixvec.push_back(ci);
			b = Biangle(t, -M_PI/4);
			ci.cpIx = m_egoSearch.nearest(b);
			ixvec.push_back(ci);
		}
		*/
	}
	
};

