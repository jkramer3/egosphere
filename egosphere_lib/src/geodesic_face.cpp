/*
 * Put copyright notice here
 */
#include <egosphere/egosphere_utils.h>
#include <egosphere/geodesic_face.h>
#include <iostream>
#include <algorithm>

namespace egosphere {
	
	GeoFace::GeoFace(GeoVertex *v1_ptr, GeoVertex *v2_ptr, GeoVertex *v3_ptr) :
	            a1(NULL), a2(NULL), a3(NULL) {
		v1 = v1_ptr; v2 = v2_ptr; v3 = v3_ptr;
		m1 = m2 = m3 = false;
	}
	
	GeoFace::~GeoFace() { }
	
	int GeoFace::adjacent(GeoFace *other) {
		// adjacent if any two vertices are shared
		//   side 1 shares v1/v2, 2 shares v1/v3, 3 shares v2/v3
		int side=0;
		if ((v1 == other->v1 || v1 == other->v2 || v1 == other->v3) &&
		    (v2 == other->v1 || v2 == other->v2 || v2 == other->v3) &&
			 (v1 != NULL && v2 != NULL)) {
			side = 1;
		} else if ((v1 == other->v1 || v1 == other->v2 || v1 == other->v3) &&
		           (v3 == other->v1 || v3 == other->v2 || v3 == other->v3) &&
			        (v1 != NULL && v3 != NULL)) {
			side = 2;
		} else if ((v2 == other->v1 || v2 == other->v2 || v2 == other->v3) &&
		           (v3 == other->v1 || v3 == other->v2 || v3 == other->v3) &&
			        (v2 != NULL && v3 != NULL)) {
			side = 3;
		}
		return side;
	}
	
	void GeoFace::setAdjacent(int side, GeoFace *other) {
		if (side == 1) a1 = other;
		else if (side == 2) a2 = other;
		else if (side == 3) a3 = other;
	}
	
	void GeoFace::setSubVerts(GeoVertex *p1, GeoVertex *p2, std::vector<GeoVertex*>& subverts) {
		if (p1 == v1 && p2 == v2) {
			s1_subverts.assign(subverts.begin(), subverts.end());
		} else if (p1 == v2 && p2 == v1) {
			reverse(subverts.begin(), subverts.end());
			s1_subverts.assign(subverts.begin(), subverts.end());
		} else if (p1 == v1 && p2 == v3) {
			s2_subverts.assign(subverts.begin(), subverts.end());
		} else if (p1 == v3 && p2 == v1) {
			reverse(subverts.begin(), subverts.end());
			s2_subverts.assign(subverts.begin(), subverts.end());
		} else if (p1 == v2 && p2 == v3) {
			s3_subverts.assign(subverts.begin(), subverts.end());
		} else if (p1 == v3 && p2 == v2) {
			reverse(subverts.begin(), subverts.end());
			s3_subverts.assign(subverts.begin(), subverts.end());
		} else {
			std::cout << "GeoFace::setSubVerts: incorrect endpoints!" << std::endl;
		}
	}
	
	void GeoFace::getSubVerts(GeoVertex *p1, GeoVertex *p2, std::vector<GeoVertex*>& subverts) {
		if (p1 == v1 && p2 == v2) {
			subverts.assign(s1_subverts.begin(), s1_subverts.end());
		} else if (p1 == v2 && p2 == v1) {
			subverts.assign(s1_subverts.begin(), s1_subverts.end());
			reverse(subverts.begin(), subverts.end());
		} else if (p1 == v1 && p2 == v3) {
			subverts.assign(s2_subverts.begin(), s2_subverts.end());
		} else if (p1 == v3 && p2 == v1) {
			subverts.assign(s2_subverts.begin(), s2_subverts.end());
			reverse(subverts.begin(), subverts.end());
		} else if (p1 == v2 && p2 == v3) {
			subverts.assign(s3_subverts.begin(), s3_subverts.end());
		} else if (p1 == v3 && p2 == v2) {
			subverts.assign(s3_subverts.begin(), s3_subverts.end());
			reverse(subverts.begin(), subverts.end());
		} else {
			std::cout << "GeoFace::getSubVerts: incorrect endpoints!" << std::endl;
		}
	}
	
	void GeoFace::setDivided(int side, bool propagate) {
		if (side == 1) {
			m1 = true;
			if (propagate) {
				int oside = a1->adjacent(this);
				a1->setSubVerts(v1, v2, s1_subverts);
				a1->setDivided(oside, false);
			}
		} else if (side == 2) {
			m2 = true;
			if (propagate) {
				int oside = a2->adjacent(this);
				a2->setSubVerts(v1, v3, s2_subverts);
				a2->setDivided(oside, false);
			}
		} else if (side == 3) {
			m3 = true;
			if (propagate) {
				int oside = a3->adjacent(this);
				a3->setSubVerts(v2, v3, s3_subverts);
				a3->setDivided(oside, false);
			}
		}
	}
	
};
