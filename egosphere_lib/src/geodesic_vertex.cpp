/*
 * Put copyright notice here
 */
//#include <egosphere/egosphere_utils.h>
#include <egosphere/geodesic_vertex.h>
#include <algorithm>

namespace egosphere {
	
	GeoVertex::GeoVertex(double t, double p) :
			minVoroTheta(toRad(181.0)), maxVoroTheta(toRad(-181.0)),
			minVoroPhi(toRad(91.0)), maxVoroPhi(toRad(-91.0)),
			voroWrap(false)
	{
		// note: default voro values init min to max values and max to
		//    min values; actual comparisons will then be in range
		biangle = new Biangle(norm_ang(t), norm_ang(p), false);
		calcPoint();
	}
	
	GeoVertex::GeoVertex(Point* p_ptr) :
			minVoroTheta(toRad(181.0)), maxVoroTheta(toRad(-181.0)),
			minVoroPhi(toRad(91.0)), maxVoroPhi(toRad(-91.0)),
			voroWrap(false)
	{
		// note: default voro values init min to max values and max to
		//    min values; actual comparisons will then be in range
		point = p_ptr;
		calcAngles();
	}
	
	GeoVertex::GeoVertex(const GeoVertex& other) :
			index(other.index),
			voroThetas(other.voroThetas),
			minVoroTheta(other.minVoroTheta),
			maxVoroTheta(other.maxVoroTheta),
			minVoroPhi(other.minVoroPhi),
			maxVoroPhi(other.maxVoroPhi),
			voroWrap(other.voroWrap)
	{
		biangle = new Biangle(*(other.getBiangle()));
		point = new Point(*(other.getPoint()));
		neighbors.assign(other.neighbors.begin(), other.neighbors.end());
	}

	GeoVertex::~GeoVertex() {
		if (biangle) delete biangle;
		if (point) delete point;
	}
	
	GeoVertex& GeoVertex::operator=(GeoVertex other) {
		// by-value parameter allows immediate swap
		swap(*this, other);
		return *this;
	}
	
	bool GeoVertex::operator<(const GeoVertex &other) {
		Biangle const *obi = other.getBiangle();
		return *biangle < *obi;
	}
	
	bool GeoVertex::operator==(const GeoVertex &other) {
		Biangle const *obi = other.getBiangle();
		return biangle->theta == obi->theta && biangle->phi == obi->phi;
	}
	
	bool GeoVertex::operator==(EgoVertex &other) {
		Biangle obi;
		other.getBiangle(obi);
		return biangle->theta == obi.theta && biangle->phi == obi.phi;
	}
	
	void GeoVertex::swap(GeoVertex& first, GeoVertex& second) {
		using std::swap;
		swap(first.biangle, second.biangle);
		swap(first.point, second.point);
		swap(first.index, second.index);
		swap(first.neighbors, second.neighbors);
		swap(first.voroThetas, second.voroThetas);
		swap(first.minVoroTheta, second.minVoroTheta);
		swap(first.maxVoroTheta, second.maxVoroTheta);
		swap(first.minVoroPhi, second.minVoroPhi);
		swap(first.maxVoroPhi, second.maxVoroPhi);
		swap(first.voroWrap, second.voroWrap);
	}
	
	Biangle const * GeoVertex::getBiangle() const { return biangle; }
	
	Point const * GeoVertex::getPoint() const { return point; }
	
	void GeoVertex::setIndex(int ix) { index = ix; }
	
	int GeoVertex::numNeighbors() { return (int)neighbors.size(); }
	
	void GeoVertex::addNeighbor(GeoVertex* v_ptr) {
		addNeighbor(v_ptr, false);
	}
	
	void GeoVertex::addNeighbor(GeoVertex *v_ptr, bool propagate) {
		if (this == v_ptr) return;
		// use pointers to make sure not already a neighbor
		std::vector<GeoVertex*>::iterator it;
		bool found = false;
		for (it=neighbors.begin(); it<neighbors.end(); it++) {
			if (v_ptr == *it) {
				found = true;
				break;
			}
		}
		if (!found) {
			neighbors.push_back(v_ptr);
		}
		// if we want propagation, we don't care if it was already in our
		// neighbor list or not; also, v_ptr's function will filter out this
		if (propagate) {
			v_ptr->addNeighbor(this, false);
		}
	}
	
	void GeoVertex::getNeighbors(std::vector<GeoVertex*>& neighs) {
		neighs.assign(neighbors.begin(), neighbors.end());
		unsigned int i, j, k;
		GeoVertex* tmp;
		std::vector<unsigned int> ixs;
		std::vector<double> dirs;
		bool npole = false;
		double dir;
		// north pole needs to reverse sort to have proper reflectance
		//   when drawing 
		if (biangle->phi == M_PI/2) {
			npole = true;
		}
		for (i=0; i<neighs.size(); i++) {
			dir = Utils::bearing(biangle, neighs[i]->getBiangle());
			ixs.push_back(i);
			dirs.push_back(dir);
		}
		if (npole) {
			std::reverse(neighs.begin(), neighs.end());
		} else {
			for (i=1; i<dirs.size(); i++) {
				j=i;
				while (j>0 && dirs[j-1] < dirs[j]) {
					k = ixs[j];
					ixs[j] = ixs[j-1];
					ixs[j-1] = k;
					dir = dirs[j];
					dirs[j] = dirs[j-1];
					dirs[j-1] = dir;
					tmp = neighs[j];
					neighs[j] = neighs[j-1];
					neighs[j-1] = tmp;
					j--;
				}
			}
		}
	}
	
	void GeoVertex::setVoroAngs(Biangle const *b, bool wrap) {
		std::vector<double>::const_iterator it;
		if (wrap) voroWrap = true;
		voroThetas.push_back(b->theta);
		// clumsy but functional way to handle setting the voronoi bounds;
		// if wrapped around the -PI/PI discontinuity, use the following:
		// - minTheta: maximum negative theta
		// - maxTheta: minimum positive number
		if (voroWrap) {
			minVoroTheta = -M_PI;
			maxVoroTheta = M_PI;
			for (it=voroThetas.begin(); it<voroThetas.end(); it++) {
				if (*it < 0.0) {
					minVoroTheta = std::max(*it, minVoroTheta);
				} else {
					maxVoroTheta = std::min(*it, maxVoroTheta);
				}
			}
		} else {
			minVoroTheta = M_PI;
			maxVoroTheta = -M_PI;
			for (it=voroThetas.begin(); it<voroThetas.end(); it++) {
				minVoroTheta = std::min(*it, minVoroTheta);
				maxVoroTheta = std::max(*it, maxVoroTheta);
			}
		}
		minVoroPhi = std::min(b->phi, minVoroPhi);
		maxVoroPhi = std::max(b->phi, maxVoroPhi);
	}
	
	double GeoVertex::getVoroAng(bool min, bool phi) const {
		if (min) {
			if (phi) return minVoroPhi;
			else return minVoroTheta;
		} else {
			if (phi) return maxVoroPhi;
			else return maxVoroTheta;
		}
	}
	
	bool GeoVertex::getVoroWrap() const { return voroWrap; }
	
	double GeoVertex::distance(double th, double ph) const {
		Biangle *biang = new Biangle(th, ph);
		double d = hypot(*biangle, *biang);
		delete biang;
		return d;
	}
	
	double GeoVertex::distance(Biangle& other) const {
		return hypot(*biangle, other);
	}
	
	double GeoVertex::distance(GeoVertex *other) const {
		Biangle const *obi = other->getBiangle();
		return distance(obi->theta, obi->phi);
	}
	
	double GeoVertex::minNeighborDistance() const {
		std::vector<GeoVertex*>::const_iterator it;
		double d=M_PI; // unit sphere; calc'd dist always < PI
		for (it=neighbors.begin(); it<neighbors.end(); it++) {
			d = std::min(d, hypot(*biangle, *((*it)->getBiangle())));
		}
		return d;
	}
	
	double GeoVertex::maxNeighborDistance() const {
		std::vector<GeoVertex*>::const_iterator it;
		double d=0.0;
		for (it=neighbors.begin(); it<neighbors.end(); it++) {
			d = std::max(d, hypot(*biangle, *((*it)->getBiangle())));
		}
		return d;
	}
	
	std::string GeoVertex::str() const { return str(2); }
	
	std::string GeoVertex::str(int dec) const {
		std::ostringstream oss;
		oss << "(" << ang_str(dec) << "),  \t" << (voroWrap ? "*" : " ");
		oss << "Min/max voros: " << voro_str(dec);
		return oss.str();
	}
	
	std::string GeoVertex::ang_str() const { return ang_str(2); }
	
	std::string GeoVertex::ang_str(int dec) const {
		std::ostringstream oss;
		oss << std::fixed << std::setprecision(dec);
		oss << toDeg(biangle->theta) << ", " << toDeg(biangle->phi);
		return oss.str();
	}
	
	std::string GeoVertex::point_str() const { return point_str(2); }
	
	std::string GeoVertex::point_str(int dec) const {
		std::ostringstream oss;
		oss << std::fixed << std::setprecision(dec);
		oss << point->x << ", " << point->y << ", " << point->z;
		return oss.str();
	}
	
	std::string GeoVertex::voro_str() const { return voro_str(2); }
	
	std::string GeoVertex::voro_str(int dec) const {
		std::ostringstream oss;
		oss << std::fixed << std::setprecision(dec);
		oss << "theta: (" << toDeg(minVoroTheta) << ", " << toDeg(maxVoroTheta) << "), ";
		oss << "phi: (" << toDeg(minVoroPhi) << ", " << toDeg(maxVoroPhi) << ")";
		return oss.str();
	}
	
	std::string GeoVertex::neigh_str() const { return neigh_str(2); }
	
	std::string GeoVertex::neigh_str(int dec) const {
		std::ostringstream oss;
		oss << "Vertex: " << str(dec) << std::endl;
		for (unsigned int i=0; i<neighbors.size(); i++) {
			oss << "  Neighbor " << i << ": (";
			oss << neighbors[i]->ang_str(dec) << ")" << std::endl;
		}
		return oss.str();
	}
	
	void GeoVertex::calcPoint() {
		Point* p_ptr = new Point();
		if (fabs(biangle->phi) == M_PI/2.0) {
			p_ptr->x = 0.0;
			p_ptr->y = 0.0;
			p_ptr->z = biangle->phi > 0.0 ? 1.0 : -1.0;
		} else {
			p_ptr->x = biangle->costh * biangle->cosph;
			p_ptr->y = biangle->sinth * biangle->cosph;
			p_ptr->z = biangle->sinph;
		}
		point = p_ptr;
	}
	
	void GeoVertex::calcAngles() {
		double r = sqrt((point->x*point->x) + (point->y*point->y) + (point->z*point->z));
		biangle = new Biangle(norm_ang(atan2(point->y, point->x)),
		                      norm_ang(asin(point->z / r)));
	}
	
	bool GeoVertex::cwGreaterThan(GeoVertex *g1, GeoVertex *g2, bool print) {
		// NOTE: treats (theta,phi) coordinates as *planar*; shouldn't
		//    be an issue for neighbors sorting
		// 'this' is center point, check if g1 > g2 in clockwise fashion
		// rotate biangles to make center 0,0 for comparison
		if (print) {
			std::cout << "          cwGreaterThan for g1=(" << g1->ang_str() << "), g2=(" << g2->ang_str() << "):" << std::endl;
		}
		Biangle b1(*g1->getBiangle()), b2(*g2->getBiangle());
		double pRot = -biangle->phi, tRot = -biangle->theta;
		b1.phi += pRot; b2.phi += pRot;
		if (fabs(pRot) <= M_PI/4) {
			b1.theta += tRot; b2.theta += tRot;
		} else {
			b1.theta += tRot; b2.theta += tRot;
		}
		if (print) {
			std::cout << "          b1.theta=" << b1.theta << ", b2.theta=" << b2.theta << std::endl;
			std::cout << "          b1.phi  =" << b1.phi << ", b2.phi  =" << b2.phi << std::endl;
		}
		if (b1.theta > 0 && b2.theta <= 0) {
			if (print) {
				std::cout << "          b1.theta>0 && b2.theta<=0; return true" << std::endl;
			}
			return true;
		} else if (b1.theta <= 0 && b2.theta > 0) {
			if (print) {
				std::cout << "          b1.theta<=0 && b2.theta>0; return true" << std::endl;
			}
			return false;
		} else if (b1.theta == 0 && b2.theta == 0) {
			if (print) {
				std::cout << "          thetas==0; return b1.phi > b2.phi (" << (b1.phi > b2.phi ? "true" : "false") << std::endl;
			}
			return b1.phi < b2.phi;
		}
		double det = (b1.theta * b2.phi) - (b2.theta * b1.phi);
		if (print) {
			std::cout << "          det=" << det << " (b1.theta * b2.phi) - (b2.theta * b1.phi)" << std::endl;
		}
		if (det > 0) {
			if (print) {
				std::cout << "          det > 0; return true" << std::endl;
			}
			return true;
		} else if (det < 0) {
			if (print) {
				std::cout << "          det < 0; return false" << std::endl;
			}
			return false;
		}
		double d1, d2;
		d1 = (b1.theta * b1.theta) + (b2.phi * b2.phi);
		d2 = (b2.theta * b2.theta) + (b1.phi * b1.phi);
		if (print) {
			std::cout << "          d1=" << d1 << ", d2=" << d2 << "; return (d1 < d2), (" << (d1 < d2 ? "true" : "false") << ")" << std::endl;
		}
		return d1 < d2;
	}
	
};

