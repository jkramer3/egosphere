/**
 * Put copyright notice here
 */
#include <egosphere/ego_path.hpp>
#include <egosphere/rviz_projection.hpp>
#include <tf/transform_datatypes.h>
#include <stdlib.h> /* srand, rand */
#include <time.h>   /* time, support rand */
#include <list>
#include <sstream>

namespace egosphere_demo {
	
	RVizProjection::RVizProjection(ros::NodeHandle &nh)
			: m_egosphere(NULL)
			, m_externalActive(false)
			, m_genType(vertex_generation::NONE)
			, m_lastGenIx(0)
			, m_lastGenTime(ros::Time::now())
			, m_genInterval(ros::Duration(0.5))
			, m_bearing(M_PI/2)
			, m_lastMsgTime(ros::Time::now())
	{
		ROS_INFO("RVizProjection: in constructor...");
		int freq, decay;
		nh.param("frequency", freq, 14);
		nh.param("show_vertices", m_pubVertMarkers, true);
		nh.param("screen_saver", m_genInactiveVerts, true);
		nh.param("readings_only", m_readingsOnly, false);
		nh.param("radius", m_egoRadius, 1.0);
		nh.param("show_decay", decay, 4000);
		m_decay = ros::Duration(decay / 1000.0);
		double decayInterval;
		nh.param("decay_interval", decayInterval, 0.1);
		m_decayInterval = ros::Duration(decayInterval);
		nh.param("center_frame", m_center, std::string("center"));
		nh.param("allow_rotate", m_allowRotation, false);
		//nh.param("external_frame", m_external, std::string("external"));
		std::string base_topic(ros::this_node::getName());
		
		// create egosphere and populate vertex marker array
		nh.param("global_frame", m_ground, std::string("global"));
		m_egosphere = new egosphere::RVizEgosphere(freq);
		m_egosphere->fillEgoVertsMarkerArray(m_ground, m_egoVertMarkers, m_egoRadius);
		m_centerSub = nh.subscribe(base_topic+"/center_pose", 1, &RVizProjection::centerPoseCb, this);
		
		// set up display capabilities: path, external frame
		//if (!m_external.empty()) {
			m_externalSub = nh.subscribe<geometry_msgs::PoseStamped>("external", 1, &RVizProjection::externalPoseCb, this);
			m_externalPose.pose.orientation.w = 1.0;
		//}
		m_pathSub = nh.subscribe<egosphere_msgs::CoordinateStampedArray>("path", 1, &RVizProjection::pathCb, this);
		m_markerPub = nh.advertise<MarkerArray>(base_topic+"/markers", 1);
		
		m_timer = nh.createTimer(m_decayInterval, &RVizProjection::pubTimer, this);
		ROS_INFO("RVizProjection: exiting constructor...");
	}
	
	RVizProjection::~RVizProjection() {
		ROS_DEBUG("RVizProjection: in destructor...");
		if (m_egosphere != NULL) {
			delete m_egosphere;
		}
		ROS_DEBUG("RVizProjection: exiting destructor...");
	}
	
	void RVizProjection::centerPoseCb(const geometry_msgs::Pose::ConstPtr &msg) {
		m_centerPose = *msg;
	}
	
	void RVizProjection::externalPoseCb(const geometry_msgs::PoseStamped::ConstPtr &msg) {
		gotCallback();
		m_externalActive = true;
		m_externalPose = *msg;
		// convert relative-to-center point to InterimInfo and put in structure
		// used to pass data to visual step represention (assumes lock)
		// - get pose location relative to egosphere center
		// - put pose location into egosphere::Point
		// - get nearest vertex/biangle to point
		egosphere::Point p;
		egosphere::RosUtils::pointMsgToPoint(m_externalPose.pose.position, p);
		unsigned int vix = m_egosphere->nearest(p);
		// - push_back vert2Interim on nearest vertex
		m_interims.push_back(vert2Interim(vix));
	}
	
	void RVizProjection::pathCb(const egosphere_msgs::CoordinateStampedArray::ConstPtr &msg) {
		gotCallback();
		egosphere::StampedBiangle b1, b2;
		b1 = coordSt2BiangleSt(msg->coords[0]);
		if (msg->coords.size() < 2) {
			m_interims.push_back(biangle2Interim(b1, vertex_highlite::READING, m_decay));
		} else {
			for (int i=1; i<msg->coords.size(); ++i) {
				b2 = coordSt2BiangleSt(msg->coords[i]);
				path2Interims(b1, b2);
				b1 = b2;
			}
		}
	}
	
	egosphere::StampedBiangle RVizProjection::coordSt2BiangleSt(const egosphere_msgs::CoordinateStamped &msg) {
		egosphere::StampedBiangle biangle(msg.coord.theta, msg.coord.phi);
		biangle.t = egosphere::Time(msg.sec, msg.nsec);
		return biangle;
	}
	
	void RVizProjection::path2Interims(const egosphere::StampedBiangle &b1,
	                                   const egosphere::StampedBiangle &b2) {
		egosphere::EgoPath path = egosphere::Utils::getPath(&b1, &b2, m_egosphere->getMinAngle());
		ros::Duration timestep(path.timespan().toDouble() / path.size());
		ros::Duration decay(m_decay);
		std::list<egosphere::StampedBiangle>::const_iterator path_it;
		for (path_it=path.begin(); path_it!=path.end(); ++path_it) {
			vertex_highlite::InterimType type = vertex_highlite::INTERPOLATE;
			if (path_it == path.begin() || path_it == (--path.end())) {
				type = vertex_highlite::READING;
			}
			m_interims.push_back(biangle2Interim(*path_it, type, decay));
			decay += timestep;
		}
	}
	
	InterimInfo RVizProjection::biangle2Interim(const egosphere::Biangle &biangle,
	                                          const vertex_highlite::InterimType type,
	                                          const ros::Duration decay) {
		// convert message to InterimInfo and put in structure used to
		// pass data to visual step represention (assumes lock)
		InterimInfo info;
		info.type = type;
		info.decay = decay;
		egosphere::RosUtils::biangleToPointMsg(&biangle, info.point, m_egoRadius);
		//egosphere::RosUtils::biangleToPointMsg(&biangle, info.point, 1.0);
		// note, no value for endpoint or ego_index
		//info.category = std::string(1, motion::makeCategory(msg.desc));
		//info.traits =   std::string(1, msg.traits);
		//info.descs =     std::string(1, msg.desc);
		return info;
	}
	
	void RVizProjection::handleInactive() {
		// in "screen saver" mode, check if the next vertex is needed
		ros::Duration elapsed(ros::Time::now() - m_lastGenTime);
		if (elapsed > m_genInterval) {
			m_externalActive = false;
			fillGenVerts(m_vertIxs);
			// use front of vertex indices, add to interims
			m_lastGenIx = m_vertIxs.front();
			m_interims.push_back(vert2Interim(m_lastGenIx));
			m_vertIxs.pop_front();
		}
	}
	
	void RVizProjection::fillGenVerts(std::deque<unsigned int> &vixs) {
		// only generate vertexes if needed
		if (vixs.empty()) {
			ros::Time now = ros::Time::now();
			ros::Duration elapsed(now - m_lastGenTime);
			// change to different vertex generation scheme after decay time
			if (elapsed > m_decay) {
				m_lastGenTime = now;
				m_genType = nextGenType(m_genType, false);
			}
			// refill vertex deque according to genType
			switch (m_genType) {
			case vertex_generation::RANDOM:
				vixs.push_back(rand() % (m_egosphere->size() - 1));
				break;
			case vertex_generation::PATH: {
				unsigned int v1, v2;
				v1 = rand() % (m_egosphere->size() - 1);
				v2 = rand() % (m_egosphere->size() - 1);
				egosphere::EgospherePath path = m_egosphere->getPath(v1, v2);
				std::list<unsigned int>::iterator pit;
				for (pit=path.begin(); pit!=path.end(); ++pit) {
					vixs.push_back(*pit);
				}
				break;
			}
			case vertex_generation::SEQUENCE: {
				unsigned int v_ix = (m_lastGenIx == (m_egosphere->size()-1) ?
				                          0 : m_lastGenIx + 1);
				vixs.push_back(v_ix);
				break;
			}
			case vertex_generation::BEARING: {
				// TODO: this isn't really what I want it to do, as bearing
				// changes for each step. What I want is to get a random-length
				// path along one of the neighbor's bearings...although I haven't
				// convinced myself it's not just PATH (except start vertex)
				if (m_lastGenTime == now) {
					std::vector<int> ns = m_egosphere->getNeighbors(m_lastGenIx);
					int nId = rand() % (ns.size() - 1);
					m_bearing = m_egosphere->getNeighborBearing(m_lastGenIx, ns[nId]);
				}
				unsigned int vix = 
				      m_egosphere->getNeighborClosestToBearing(m_lastGenIx, m_bearing);
				vixs.push_back(vix);
				break;
			}
			default:
				ROS_WARN("RVizProjection: no generation type set!");
			}
		}
	}
	
	GenType RVizProjection::nextGenType(GenType cur, bool incNone) {
		GenType next;
		switch (cur) {
		case vertex_generation::NONE:
			next = vertex_generation::RANDOM;
			break;
		case vertex_generation::RANDOM:
			next = vertex_generation::PATH;
			break;
		case vertex_generation::PATH:
			next = vertex_generation::SEQUENCE;
			break;
		case vertex_generation::SEQUENCE:
			next = vertex_generation::BEARING;
			break;
		case vertex_generation::BEARING:
			next = (incNone ? vertex_generation::NONE : vertex_generation::RANDOM);
			break;
		default:
			next = vertex_generation::RANDOM;
			break;
		}
		return next;
	}
	
	InterimInfo RVizProjection::vert2Interim(unsigned int vix) {
		InterimInfo info;
		info.type = vertex_highlite::FACE;
		info.decay = m_decay;
		info.ego_index = vix;
		return info;
	}
	
	void RVizProjection::gotCallback() {
		ros::Duration elapsed(ros::Time::now() - m_lastMsgTime);
		if (elapsed > m_decay) {
			// was in screen-saver mode
			MarkerArray marr;
			processInterims(m_interims, marr);
			if (!marr.markers.empty()) {
				for (unsigned int i=0; i<marr.markers.size(); ++i) {
					marr.markers[i].action = Marker::DELETE;
				}
				m_markerPub.publish(marr);
			}
			m_interims.clear();
			m_vertIxs.clear();
		}
		m_lastMsgTime = ros::Time::now();
	}
	
	void RVizProjection::pubTimer(const ros::TimerEvent &te) {
		// check for "screen saver" mode
		ros::Duration elapsed(ros::Time::now() - m_lastMsgTime);
		if (elapsed > m_decay && m_genInactiveVerts) {
			handleInactive();
		}
		MarkerArray marr;
		if (m_pubVertMarkers) {
			for (unsigned int i=0; i<m_egoVertMarkers.markers.size(); ++i) {
				setMarker(vertex_highlite::VERTICES, m_egoVertMarkers.markers[i]);
				marr.markers.push_back(m_egoVertMarkers.markers[i]);
			}
		}
		processInterims(m_interims, marr);
		if (!marr.markers.empty()) {
			m_markerPub.publish(marr);
		}
	}
	
	void RVizProjection::processInterims(std::deque<InterimInfo> &ideq,
	                                     MarkerArray &marr) {
		// put the interims up into their respective marker arrays,
		// then publish each if not empty
		//ROS_INFO("RVizProjection: got %lu interims; (%lu markers to start)", ideq.size(), marr.markers.size());
		if (ideq.size() > 0) {
			ros::Duration zeroD(0,0);
			Marker projMarker, faceMarker, segSurfMarker, segMagMarker;
			// note: possible erasure, so increment manually
			std::deque<InterimInfo>::iterator it;
			for (it=ideq.begin(); it!=ideq.end();) {
				if (it->decay <= zeroD) {
					it = ideq.erase(it);
				} else {
					switch (it->type) {
					case vertex_highlite::READING:
					case vertex_highlite::INTERPOLATE:
						// one Point/Color for each projection
						processInterim(*it, projMarker);
						(*it).decay -= m_decayInterval;
						break;
					case vertex_highlite::FACE:
						// one marker for each face; get/append it
						processInterim(*it, faceMarker);
						marr.markers.push_back(faceMarker);
						(*it).decay -= m_decayInterval;
						break;
					case vertex_highlite::SEG_SURF:
						// one line/Color for each segment
						processInterim(*it, segSurfMarker);
						(*it).decay -= m_decayInterval;
						break;
					case vertex_highlite::SEG_MAG:
						// one line/Color for each segment
						processInterim(*it, segMagMarker);
						(*it).decay -= m_decayInterval;
						break;
					default:
						break;
					}
					++it;
				}
			}
			setMarker(vertex_highlite::READING, projMarker);
			marr.markers.push_back(projMarker);
		}
	}
	
	void RVizProjection::processInterim(const InterimInfo &info, Marker &m) {
		switch (info.type) {
		case vertex_highlite::FACE:
			// one marker for each face; stuff and return
			m_egosphere->stuffMarker(info.ego_index, m, m_egoRadius,
			                         egosphere::marker_type::FACE);
			setMarker(vertex_highlite::FACE, m);
			m.lifetime = ros::Duration(m_decay);
			m.color.a = info.decay.toSec() / m_decay.toSec();
			m.id = info.ego_index;
			break;
		case vertex_highlite::READING:
		case vertex_highlite::INTERPOLATE:
			// one marker for all projections; append point/color
			m.points.push_back(info.point);
			m.colors.push_back(getColor(info.type, info.decay));
			break;
		case vertex_highlite::SEG_SURF:
		case vertex_highlite::SEG_MAG:
			// one marker for all segments; append point/color
			m.points.push_back(info.point);
			m.points.push_back(info.endpoint);
			//m.colors.push_back(getColor(info.type, info.decay));
			break;
		default:
			setMarker(info.type, m);
			break;
		}
		//ROS_INFO("RVizProjection: set marker %d alpha to %f", m.id, m.color.a);
	}
	
	void RVizProjection::setMarker(vertex_highlite::InterimType type, Marker &m) {
		switch (type) {
		case vertex_highlite::READING:
		case vertex_highlite::INTERPOLATE: {
			m.ns   = "rvizProjections";
			m.id = 0;
			m.type = Marker::SPHERE_LIST;
			double sphrad = 0.04 * m_egoRadius;
			m.scale.x = sphrad; m.scale.y = sphrad; m.scale.z = sphrad;
			m.lifetime = ros::Duration(m_decay);
			break;
		}
		case vertex_highlite::FACE: {
			m.ns   = "rvizFaces";
			break;
		}
		case vertex_highlite::SEG_SURF:
		case vertex_highlite::SEG_MAG: {
			if (type == vertex_highlite::SEG_MAG) {
				m.ns = "rvizMagSegs";
				m.color.r = 0.0; m.color.g = 1.0; m.color.b = 0.0; m.color.a = 0.7;
			} else {
				m.ns = "rvizSurfaceSegs";
				m.color.r = 1.0; m.color.g = 1.0; m.color.b = 1.0; m.color.a = 1.0;
			}
			m.id = 0;
			m.type = Marker::LINE_LIST;
			m.pose.orientation.w = 1.0;
			double sphrad = 0.002;
			m.scale.x = sphrad; m.scale.y = sphrad; m.scale.z = sphrad;
			m.lifetime = ros::Duration(m_decay*2.0);
			break;
		}
		case vertex_highlite::VERTICES:
		default:
			m.lifetime = ros::Duration(0);
			m.color.r = 0.8; m.color.g = 0.2; m.color.b = 1.0; m.color.a = 0.7;
			break;
		}
		m.header.frame_id = m_ground;
		m.action = Marker::MODIFY;
		if (m_allowRotation) {
			m.pose = m_centerPose;
		} else {
			m.pose.position = m_centerPose.position;
		}
	}
	
	std_msgs::ColorRGBA RVizProjection::getColor(
	          vertex_highlite::InterimType type, const ros::Duration &tleft) {
		std_msgs::ColorRGBA color;
		color.a = tleft.toSec() / m_decay.toSec();
		switch (type) {
		case vertex_highlite::READING:
			color.r = 1.0;
			color.g = 0.0;
			color.b = 0.0;
			break;
		case vertex_highlite::INTERPOLATE:
			color.r = 1.0;
			color.g = 0.0;
			color.b = 0.7;
			break;
		case vertex_highlite::SEG_SURF:
			color.r = 0.8;
			color.g = 0.0;
			color.b = 0.8;
			break;
		case vertex_highlite::SEG_MAG:
			color.r = 0.0;
			color.g = 0.8;
			color.b = 0.8;
			break;
		default:
			color.r = 0.0;
			color.g = 0.0;
			color.b = (float)(204/255);
			break;
		}
		return color;
	}
	
};

int main(int argc, char** argv) {
	ros::init(argc, argv, "rviz_projection");
	ros::NodeHandle nh("~");

	egosphere_demo::RVizProjection rp(nh);
	ros::spin();
	return 0;
}

