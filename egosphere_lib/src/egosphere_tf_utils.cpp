/**
 * Put copyright notice here
 */
#include <egosphere/egosphere_tf_utils.hpp>
#include <egosphere/egosphere_ros_utils.hpp>
#include <tf/transform_datatypes.h>
#include <sstream>

namespace egosphere {
	
	std::string TfUtils::tfPointStr(const tf::Point &tfPoint) {
		std::stringstream oss;
		oss << "[" << tfPoint.x() << ", " << tfPoint.y() << ", " << tfPoint.z() << "]";
		return oss.str();
	}
	
	std::string TfUtils::tfPoseStr(const tf::Pose &tfPose) {
		std::stringstream oss;
		oss << "[" << tfPose.getOrigin().x() << ", " << tfPose.getOrigin().y() << ", " << tfPose.getOrigin().z() << "]";
		geometry_msgs::Quaternion q;
		tf::quaternionTFToMsg(tfPose.getRotation(), q);
		oss << "[" << q.x << ", " << q.y << ", " << q.z << ", " << q.w << "]";
		return oss.str();
	}
	
	geometry_msgs::Point TfUtils::getPointMsgViaTf(
	                                 std::shared_ptr<tf::TransformListener> tf,
	                                 const std::string &target,
	                                 const std::string &source) {
		tf::StampedTransform transform;
		tf->lookupTransform(target, source, ros::Time(0), transform);
		geometry_msgs::Point point;
		tf::pointTFToMsg(transform.getOrigin(), point);
		return point;
	}
	
	geometry_msgs::PointStamped TfUtils::getPointStampedMsgViaTf(
	                                 std::shared_ptr<tf::TransformListener> tf,
	                                 const std::string &target,
	                                 const std::string &source) {
		// from ROS docs, applied transform puts source data into target
		// - like Transformer::transformPoint, without Stamped<Point> creation
		// listener->transformPoint:
		//    target-frame, Stamped<tf::Point-in>, Stamped<tf::Point-out>
		// listener->transformPoint:
		//    target-frame, ros::Time, Stamped<tf::Point-in>, fixed-frame, Stamped<tf::Point-out>
		tf::StampedTransform transform;
		tf->lookupTransform(target, source, ros::Time(0), transform);
		geometry_msgs::PointStamped point;
		tf::pointTFToMsg(transform.getOrigin(), point.point);
		point.header.stamp = transform.stamp_;
		return point;
	}
	
	geometry_msgs::Pose TfUtils::getPoseMsgViaTf(
	                                 std::shared_ptr<tf::TransformListener> tf,
	                                 const std::string &target,
	                                 const std::string &source) {
		// from ROS docs, applied transform puts source data into target
		// - like Transformer::transformPose, without Stamped<Pose> creation
		tf::StampedTransform transform;
		tf->lookupTransform(target, source, ros::Time(0), transform);
		geometry_msgs::Pose pose;
		tf::poseTFToMsg(transform, pose);
		return pose;
	}
	
	geometry_msgs::Pose TfUtils::getPoseMsgRadius(double radius,
	                                              const Point& extPoint) {
		// get egosphere biangle for point
		Biangle relBi;
		Utils::calcBiangle(extPoint, relBi);
		relBi.mag = radius;
		// get egosphere::Point on surface
		Point relPoint;
		Utils::calcPoint(relBi, relPoint, radius);
		// create/return surface point as ROS pose
		geometry_msgs::Pose pose;
		RosUtils::pointToPointMsg(relPoint, pose.position);
		RosUtils::biangleToQuaternionMsg(&relBi, pose.orientation);
		return pose;
	}
	
	geometry_msgs::Pose TfUtils::getPoseMsgRadius(double radius,
	                                 const geometry_msgs::Pose &extPose) {
		// convert ROS position to egosphere::Point
		egosphere::Point extPoint;
		egosphere::RosUtils::pointMsgToPoint(extPose.position, extPoint);
		return getPoseMsgRadius(radius, extPoint);
	}
	
	//geometry_msgs::Pose TfUtils::getPoseFace(
	//                                 const FeatureExternal *ego,
	//                                 const geometry_msgs::Pose &extPose) const {
	//	// working out how to stuff FACE points...
	//	egosphere::Point extPoint;
	//	egosphere::RosUtils::pointMsgToPoint(repPose.position, extPoint);
	//	egosphere::Biangle relBi;
	//	egosphere::Utils::calcBiangle(extPoint, relBi);
	//	egosphere::EgoSearch search = m_featExternal->getSearch();
	//	unsigned int ixExt = search.nearest(relBi);
	//	std::vector<egosphere::EgoVertex*> verts = m_featExternal->getVerts();
	//	std::vector<geometry_msgs::Point> fPts;
	//	egosphere::RosUtils::fillFacePointMsgVec(verts[ixExt], fPts);
	//	const_cast<ext::ExternalInfo&>(it->second).updateMarkers(repPose, biPose, facePose, fPts);
	//}
	
	geometry_msgs::Pose TfUtils::offsetPoseInOtherFrame(
	                      std::shared_ptr<tf::TransformListener> tf,
			                const std::string &srcFrame,
			                const geometry_msgs::Pose &srcPose,
			                const std::string &offFrame,
								 const geometry_msgs::Pose &offPose) {
		geometry_msgs::PoseStamped newPose;
		offsetPoseInOtherFrame(tf, srcFrame, srcPose, offFrame, offPose, newPose);
		return newPose.pose;
	}
	
	void TfUtils::offsetPoseInOtherFrame(
	                      std::shared_ptr<tf::TransformListener> tf,
			                const std::string &srcFrame,
			                const geometry_msgs::Pose &srcPose,
			                const std::string &offFrame,
								 const geometry_msgs::Pose &offPose,
		                   geometry_msgs::PoseStamped &newPose) {
		// setup TF data objects:
		// - put offset in destination frame
		// - get identity in source frame
		tf::Pose dpose, spose;
		tf::poseMsgToTF(srcPose, spose);
		tf::poseMsgToTF(offPose, dpose);
		tf::Stamped<tf::Pose> tfNewPose;
		offsetPoseInOtherFrame(tf, srcFrame, spose, offFrame, dpose, tfNewPose);
		newPose.header.stamp = tfNewPose.stamp_;
		newPose.header.frame_id = tfNewPose.frame_id_;
		tf::poseTFToMsg(tfNewPose, newPose.pose);
	}
	
	void TfUtils::offsetPoseInOtherFrame(
	                      std::shared_ptr<tf::TransformListener> tf,
			                const std::string &srcFrame,
			                const tf::Pose &srcPose,
			                const std::string &offFrame,
								 const tf::Pose &offPose,
		                   tf::Stamped<tf::Pose> &newPose) {
		tf::Stamped<tf::Pose> tfOffDpose(offPose, ros::Time(0), offFrame);
		tf::Pose ipose = tf::Pose::getIdentity();
		tf::Stamped<tf::Pose> tfSrcIpose(ipose, ros::Time(0), srcFrame);
		tf::Stamped<tf::Pose> tfOffIpose;
		// do the transforms:
		// - identity from source to offset frame
		// - apply offset to identity in offset frame
		// - offset-identity back to source frame
		try {
			tf->transformPose(offFrame, tfSrcIpose, tfOffIpose);
			tfOffIpose.setRotation(tfOffDpose.getRotation() *
			                       tfOffIpose.getRotation());
			tfOffIpose.setOrigin(tfOffDpose.getOrigin() +
			                     tfOffIpose.getOrigin());
			tf->transformPose(srcFrame, tfOffIpose, tfSrcIpose);
		} catch (tf::TransformException &ex) {
			ROS_ERROR("TfUtils: transform pose\n\t%s", ex.what());
			newPose.stamp_ = ros::Time(0);
			newPose.frame_id_ = srcFrame;
			newPose.setData(srcPose);
			return;
		}
		// extract TF data applied to source (reusing 'dpose')
		// - put source in TF format
		// - apply offset-identity to source pose
		// - convert TF data to message format
		//tf::poseMsgToTF(srcPose, dpose);
		newPose.stamp_ = tfSrcIpose.stamp_;
		newPose.frame_id_ = tfSrcIpose.frame_id_;
		newPose.setRotation(tfSrcIpose.getRotation() *
		                    srcPose.getRotation());
		newPose.setOrigin(tfSrcIpose.getOrigin() + srcPose.getOrigin());
		//tf::poseStampedTFToMsg(tfSrcIpose, newPose);
		return;
	}
		
};

