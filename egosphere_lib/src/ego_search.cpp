/*
 * Put copyright notice here
 */
#include <egosphere/ego_search.h>
#include <iostream>
#include <iomanip>
#include <sstream>

namespace egosphere {
	
	EgoSearch::EgoSearch() { }
	
	EgoSearch::EgoSearch(double minTh, double maxTh, double spanTh,
	                     double minPh, double maxPh, double spanPh,
	                     std::vector< std::vector< std::map<Biangle,int> > > vs) :
			minTheta(minTh), maxTheta(maxTh),
			minPhi(minPh), maxPhi(maxPh),
			bucketSizeTheta(spanTh), bucketSizePhi(spanPh),
			verts(vs) {
		// nothing else to do...
	}
	
	EgoSearch::~EgoSearch() { }
	
	int EgoSearch::nearest(Biangle const &b) const {
		int thix, phix;
		std::map<Biangle,int>::const_iterator it;
		int ev = -1;
		double mind, tmpmin;
		mind = tmpmin = M_PI; // use maxVertDist?
		
		if (!b.invalid()) {
			thix = (int)((b.theta - minTheta) / bucketSizeTheta);
			phix = (int)((b.phi - minPhi) / bucketSizePhi);
			for (it=verts[thix][phix].begin(); it!=verts[thix][phix].end(); it++) {
				//std::cout << "search::nearest: check [" << (*it).first.str() << "]" << std::endl;
				// check Biangle equality to optimize the search (for times
				// when the Biangle is obtained directly from a vertex)
				if ((*it).first == b) {
					ev = (*it).second;
					break;
				} else {
					tmpmin = hypot((*it).first, b);
					if (tmpmin < mind) {
						mind = tmpmin;
						ev = (*it).second;
					}
				}
			}
		}
		return ev;
	}
	
	int EgoSearch::nearest(Point const &p) const {
		Biangle b;
		int thix, phix;
		std::map<Biangle,int>::const_iterator it;
		int ev = -1;
		double mind, tmpmin;
		mind = tmpmin = M_PI; // use maxVertDist?
		
		Utils::calcBiangle(p, b);
		if (!b.invalid()) {
			thix = (int)((b.theta - minTheta) / bucketSizeTheta);
			phix = (int)((b.phi - minPhi) / bucketSizePhi);
			for (it=verts[thix][phix].begin(); it!=verts[thix][phix].end(); it++) {
				Point vp;
				Utils::calcPoint((*it).first, vp);
				tmpmin = distance_sq(vp, p);
				if (tmpmin < mind) {
					mind = tmpmin;
					ev = (*it).second;
				}
			}
		}
		return ev;
	}
	
	std::string EgoSearch::str() const {
		std::ostringstream oss;
		std::map<Biangle,int>::const_iterator it;
		int vcnt;
		for (unsigned int i=0; i<verts.size(); i++) {
			oss << "Theta index " << i << ":" << std::endl;
			for (unsigned int j=0; j<verts[i].size(); j++) {
				oss << "  Phi index " << j << ":" << std::endl;
				vcnt = 1;
				for (it=verts[i][j].begin(); it!=verts[i][j].end(); it++) {
					oss << "    " << vcnt++ << " of " << verts[i][j].size() << ": ";
					oss << (*it).second << ": (" << (*it).first.str() << ")" << std::endl;
				}
				oss << "  End phi index " << j << std::endl;
			}
			oss << "End theta index " << i << std::endl;
		}
		return oss.str();
	}

};

