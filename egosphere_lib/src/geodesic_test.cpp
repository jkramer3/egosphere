/*
 * Put copyright notice here
 */

#include <egosphere/geodesic.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <algorithm>
#include <string.h>
#include <unistd.h>

int DEF_FREQ = 2;

void showHelp(std::string prg) {
	std::cout << prg << ": [-f freq (default " << DEF_FREQ << ")] ";
	std::cout << "[-o basefilename]" << std::endl;
}

/** Convert from spherical to cartesian back to spherical. */
void showConversion(double ft, double fp) {
	egosphere::Biangle *b1 = new egosphere::Biangle(ft, fp);
	std::cout << "Converting (" << b1->str(15) << ")" << std::endl;
	egosphere::Point *p = egosphere::Utils::calcPoint(b1);
	std::cout << "  Cartesian: (" << p->str(9) << ")" << std::endl;
	egosphere::Biangle *b2 = egosphere::Utils::calcBiangle(p);
	std::cout << "  Spherical: (" << b2->str(15) << ")" << std::endl;
}

/** Show results of a single point location. */
void showLocator(egosphere::Geosphere& geo, double ft, double fp) {
	std::cout << std::endl << "Locating vertex near (" << egosphere::toDeg(ft);
	std::cout << ", " << egosphere::toDeg(fp) << ")" << std::endl;
	geo.printNearest(ft, fp);
}

/** Construct the geodesic structure and display its information. */
int main(int argc, char **argv) {
	int freq;
	std::string finm("ego_gl_verts");
	bool write_files = false;
	bool show_conv   = false;
	bool show_verts  = false;
	bool show_voros  = false;
	bool show_search = false;
	bool show_nears  = false;
	bool test_nears  = true;
	int test_num = 10000;
	
	freq = DEF_FREQ;
	for (int i=1; i<argc; i++) {
		if (strcmp(argv[i], "-h") == 0) {
			showHelp(argv[0]);
			exit(0);
		} else if (i+1 != argc) {
			if (strcmp(argv[i], "-f") == 0) {
				freq = atoi(argv[++i]);
			} else if (strcmp(argv[i], "-o") == 0){
				write_files = true;
				finm = argv[++i];
			} else {
				std::cout << "Missing arguments" << std::endl;
				exit(1);
			}
		}
	}
	
	if (show_conv) {
		std::cout << "Testing coordinate conversion:" << std::endl;
		showConversion(egosphere::toRad(-30.0), egosphere::toRad(35.0));
		showConversion(0.0, M_PI/4);
		showConversion(egosphere::toRad(180.0), egosphere::toRad(-26.57));
		showConversion(egosphere::toRad(0.0), egosphere::toRad(58.28));
		showConversion(egosphere::toRad(-30.0), egosphere::toRad(89.0));
		showConversion(egosphere::toRad(90.5), egosphere::toRad(-80.0));
	}
	
	std::cout << "Constructing geosphere (freq=" << freq << ")" << std::endl;
	egosphere::Geosphere geo(freq);
	
	std::cout << std::endl << geo.metainfo_str() << std::endl;
	std::cout << geo.search_sum_str() << std::endl;
	
	if (write_files) {
		std::cout << "Writing files with basename " << finm << std::endl;
		geo.writeFiles(finm);
	}
	
	if (show_verts) {
		std::cout << "Geosphere vertices (* indicates border straddle):" << std::endl;
		std::cout << geo.vert_str() << std::endl;
		std::cout << "(Should have printed " << (10 * pow(geo.frequency(), 2) + 2) << " vertices)" << std::endl;
	}
	
	if (show_voros) {
		std::cout << "Geosphere voronoi vertices:" << std::endl;
		std::cout << geo.voro_str() << std::endl;
	}
	
	if (show_search) {
		std::cout << "Geosphere search structure:" << std::endl;
		std::cout << geo.search_str(9) << std::endl;
		std::cout << "(Should have printed " << (20 * pow(geo.frequency(), 2)) << " voronois)" << std::endl;
	}
	
	if (show_nears) {
		/* these are searches that were picked up as wrong during the
		 * randomized testing...set as known incorrects to be fixed
		 */
		/*
		showLocator(geo, egosphere::toRad(-30.0), egosphere::toRad(35.0));
		showLocator(geo, 0.0, M_PI/4);
		showLocator(geo, egosphere::toRad(180.0), egosphere::toRad(-26.57));
		showLocator(geo, egosphere::toRad(0.0), egosphere::toRad(58.28));
		showLocator(geo, egosphere::toRad(-30.0), egosphere::toRad(89.0));
		showLocator(geo, egosphere::toRad(90.5), egosphere::toRad(-80.0));
		showLocator(geo, egosphere::toRad(30), egosphere::toRad(-85.0));
		showLocator(geo, egosphere::toRad(-89), egosphere::toRad(-75.0));
		showLocator(geo, egosphere::toRad(-54), egosphere::toRad(-89.0));
		showLocator(geo, egosphere::toRad(180.0), egosphere::toRad(-58.2));
		showLocator(geo, egosphere::toRad(180.0), egosphere::toRad(-26.6));
		showLocator(geo, egosphere::toRad(180.0), egosphere::toRad(0.0));
		showLocator(geo, egosphere::toRad(180.0), egosphere::toRad(-89.0));
		*/
		/*
		showLocator(geo, egosphere::toRad(-38.405519999999996), egosphere::toRad(72.473939999999999));
		showLocator(geo, egosphere::toRad(150.203519999999997), egosphere::toRad(73.554299999999984));
		showLocator(geo, egosphere::toRad(-125.941680000000005), egosphere::toRad(-72.861120000000000));
		showLocator(geo, egosphere::toRad(174.308760000000035), egosphere::toRad(-73.034280000000010));
		showLocator(geo, egosphere::toRad(-86.634000000000000), egosphere::toRad(40.980419999999995));
		showLocator(geo, egosphere::toRad(-102.812759999999997), egosphere::toRad(-41.967900000000000));
		showLocator(geo, egosphere::toRad(46.687680000000007), egosphere::toRad(-41.504400000000004));
		showLocator(geo, egosphere::toRad(-106.241759999999985), egosphere::toRad(-40.446720000000006));
		showLocator(geo, egosphere::toRad(-116.360640000000004), egosphere::toRad(-41.609700000000004));
		showLocator(geo, egosphere::toRad(152.227080000000001), egosphere::toRad(40.682520000000004));
		showLocator(geo, egosphere::toRad(-24.373799999999999), egosphere::toRad(-40.994639999999997));
		showLocator(geo, egosphere::toRad(-54.317160000000001), egosphere::toRad(-21.407940000000004));
		showLocator(geo, egosphere::toRad(-17.439119999999992), egosphere::toRad(-21.288779999999999));
		showLocator(geo, egosphere::toRad(143.833320000000015), egosphere::toRad(7.732619999999994));
		showLocator(geo, egosphere::toRad(163.114199999999983), egosphere::toRad(21.474180000000004));
		showLocator(geo, egosphere::toRad(126.145440000000022), egosphere::toRad(-21.605939999999993));
		showLocator(geo, egosphere::toRad(-89.453519999999997), egosphere::toRad(-23.322959999999998));
		showLocator(geo, egosphere::toRad(-91.224359999999990), egosphere::toRad(20.230380000000004));
		showLocator(geo, egosphere::toRad(-18.889920000000011), egosphere::toRad(22.621140000000011));
		showLocator(geo, egosphere::toRad(-178.782839999999993), egosphere::toRad(-86.777280000000005));
		showLocator(geo, egosphere::toRad(-177.681240000000031), egosphere::toRad(-86.322960000000009));
		showLocator(geo, egosphere::toRad(-179.431200000000018), egosphere::toRad(-86.251500000000007));
		showLocator(geo, egosphere::toRad(-179.187840000000023), egosphere::toRad(-83.944799999999987));
		showLocator(geo, egosphere::toRad(-177.986520000000013), egosphere::toRad(-84.366180000000000));
		showLocator(geo, egosphere::toRad(173.679479999999955), egosphere::toRad(83.594340000000017));
		showLocator(geo, egosphere::toRad(172.544039999999995), egosphere::toRad(-84.644100000000009));
		showLocator(geo, egosphere::toRad(-177.167159999999996), egosphere::toRad(-85.317480000000003));
		showLocator(geo, egosphere::toRad(176.914079999999984), egosphere::toRad(81.013499999999993));
		showLocator(geo, egosphere::toRad(168.620399999999989), egosphere::toRad(87.420059999999978));
		showLocator(geo, egosphere::toRad(179.860680000000002), egosphere::toRad(60.652259999999998));
		showLocator(geo, egosphere::toRad(179.992439999999988), egosphere::toRad(-12.224159999999991));
		showLocator(geo, egosphere::toRad(179.952120000000008), egosphere::toRad(75.389759999999981));
		showLocator(geo, egosphere::toRad(179.861040000000003), egosphere::toRad(-20.255400000000005));
		showLocator(geo, egosphere::toRad(179.907120000000020), egosphere::toRad(65.380319999999998));
		showLocator(geo, egosphere::toRad(179.892359999999968), egosphere::toRad(65.359080000000006));
		showLocator(geo, egosphere::toRad(179.894880000000029), egosphere::toRad(64.989360000000019));
		showLocator(geo, egosphere::toRad(179.874719999999996), egosphere::toRad(-16.354620000000001));
		showLocator(geo, egosphere::toRad(179.907479999999993), egosphere::toRad(-16.359840000000002));
		showLocator(geo, egosphere::toRad(179.956439999999958), egosphere::toRad(60.113700000000001));
		showLocator(geo, egosphere::toRad(179.957879999999989), egosphere::toRad(70.489080000000001));
		showLocator(geo, egosphere::toRad(179.946360000000027), egosphere::toRad(-19.781100000000002));
		showLocator(geo, egosphere::toRad(179.861040000000003), egosphere::toRad(70.372979999999998));
		showLocator(geo, egosphere::toRad(179.879760000000033), egosphere::toRad(74.621520000000018));
		//showLocator(geo, egosphere::toRad(), egosphere::toRad());
		 */
		/*
		showLocator(geo, egosphere::toRad(-180.000000000000000), egosphere::toRad(58.071959999999990));
		showLocator(geo, egosphere::toRad(0.000000000000000), egosphere::toRad(-0.867060000000000));
		showLocator(geo, egosphere::toRad(-180.000000000000000), egosphere::toRad(56.822400000000002));
		showLocator(geo, egosphere::toRad(-72.000000000000000), egosphere::toRad(-5.152499999999997));
		showLocator(geo, egosphere::toRad(108.000000000000000), egosphere::toRad(60.885359999999999));
		showLocator(geo, egosphere::toRad(-36.000000000000000), egosphere::toRad(59.268779999999992));
		showLocator(geo, egosphere::toRad(117.179999999999978), egosphere::toRad(82.443239999999989));
		showLocator(geo, egosphere::toRad(119.138760000000005), egosphere::toRad(85.094280000000012));
		showLocator(geo, egosphere::toRad(153.619200000000006), egosphere::toRad(-83.029139999999998));
		showLocator(geo, egosphere::toRad(153.493560000000002), egosphere::toRad(-80.587440000000015));
		//showLocator(geo, egosphere::toRad(), egosphere::toRad());
		*/
		// maybe wrong on f=2
		showLocator(geo, egosphere::toRad(-72.000000000000000), egosphere::toRad(-61.716239999999999));
		// maybe wrong on f=14
		showLocator(geo, egosphere::toRad(0.000000000000000), egosphere::toRad(-86.985900000000015));
		
		std::cout << std::endl;
	}
	
	if (test_nears) {
		geo.test_search(test_num);
	}
	
	return 0;
}
