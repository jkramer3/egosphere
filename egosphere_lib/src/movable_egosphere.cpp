/*
 * Put copyright notice here
 */

#include <egosphere/geodesic.h>
#include <egosphere/egosphere_utils.h>
#include <egosphere/impression.hpp>
#include <egosphere/movable_egosphere.hpp>

namespace egosphere_demo {
	
	MovableEgosphere::MovableEgosphere(int f) {
		// create egosphere (vertices and search)
		egosphere::Geosphere geo(f);
		m_freq = geo.frequency();
		egosphere::GeosphereMetainfo mi = geo.getMetainfo();
		m_minAngle = mi.minVertVoroD;
		m_maxAngle = mi.maxVertVoroD;
		m_egosearch = geo.getEgoStructs(m_vertices);
		// create the vector for tracking which vertices have impressions
		m_vertImpIdsVec.reserve(m_vertices.size());
		for (unsigned int i=0; i<m_vertices.size(); ++i) {
			IdSet ids;
			m_vertImpIdsVec.push_back(ids);
		}
	}
	
	MovableEgosphere::~MovableEgosphere() {
		std::vector<egosphere::EgoVertex*>::iterator vit;
		for (vit=m_vertices.begin(); vit<m_vertices.end(); vit++) {
			delete *vit;
		}
	}
	
	int MovableEgosphere::getFrequency() const {
		return m_freq;
	}
	
	double MovableEgosphere::getMinAngle(bool inDegrees) const {
		return (inDegrees ? egosphere::toDeg(m_minAngle) : m_minAngle);
	}
	
	double MovableEgosphere::getMaxAngle(bool inDegrees) const {
		return (inDegrees ? egosphere::toDeg(m_maxAngle) : m_maxAngle);
	}
	
	unsigned int MovableEgosphere::getNearest(const egosphere::Biangle& b) const {
		return m_egosearch.nearest(b);
	}
	
	unsigned int MovableEgosphere::getNearest(egosphere::Biangle const *b) const {
		return getNearest(*b);
	}
	
	IdVec MovableEgosphere::getImpressionIds() const {
		IdVec ids;
		std::map<std::string, Impression>::const_iterator it;
		for (it=m_impMap.begin(); it!=m_impMap.end(); ++it) {
			ids.push_back(it->first);
		}
		return ids;
	}
	
	IdVec MovableEgosphere::getImpressionIds(unsigned int vid) const {
		IdVec ids;
		if (m_vertices.size() >= vid) {
			// vertex out of bounds, return empty vector
		} else if (m_vertImpIdsVec[vid].empty()) {
			// no ids for vertex, return empty vector
		} else {
			IdSet::const_iterator it;
			for (it=m_vertImpIdsVec[vid].begin(); it!=m_vertImpIdsVec[vid].end(); ++it) {
				ids.push_back(*it);
			}
		}
		return ids;
	}
	
	unsigned int MovableEgosphere::getVertexId(const std::string& iid) const {
		std::map<std::string, Impression>::const_iterator it = m_impMap.find(iid);
		if (it == m_impMap.end()) {
			throw std::invalid_argument("ID ["+ iid +"] not found");
		}
		return getNearest(it->second.m_biangle);
	}
	
	std::vector<egosphere::Point> MovableEgosphere::getNeighborPoints(
	                                             const std::string& iid) const {
		std::vector<egosphere::Point> npVec;
		m_vertices[getVertexId(iid)]->neighborPoints(npVec);
		return npVec;
	}
	
	void MovableEgosphere::addImpression(const std::string& iid,
	                                  const egosphere::StampedPoint& impLocal) {
		std::map<std::string, Impression>::const_iterator it = m_impMap.find(iid);
		if (it == m_impMap.end()) {
			m_impMap[iid] = Impression(impLocal);
		} else {
			m_impMap[iid].setFromStampedPoint(impLocal);
		}
		unsigned int vertId = getNearest(m_impMap[iid].m_biangle);
		m_vertImpIdsVec[vertId].insert(iid);
	}
	
	void MovableEgosphere::updateImpression(const std::string& iid,
	                                  const egosphere::StampedPoint& impLocal) {
		std::map<std::string, Impression>::iterator it = m_impMap.find(iid);
		if (it != m_impMap.end()) {
			unsigned int oldVert = getNearest(it->second.m_biangle);
			it->second.setFromStampedPoint(impLocal);
			unsigned int newVert = getNearest(it->second.m_biangle);
			if (newVert != oldVert) {
				m_vertImpIdsVec[oldVert].erase(iid);
				m_vertImpIdsVec[newVert].insert(iid);
			}
		}
	}
	
	Impression MovableEgosphere::delImpression(const std::string& iid) {
		// TODO: delete and return Impression from data structures
		return Impression(egosphere::StampedPoint());
	}
	
	egosphere::Point MovableEgosphere::getImpressionPosition(
			                                      const std::string& iid) const {
		std::map<std::string, Impression>::const_iterator it = m_impMap.find(iid);
		if (it != m_impMap.end()) {
			egosphere::Point p;
			egosphere::Utils::calcPoint(it->second.m_biangle, p);
			return p;
		}
		throw std::invalid_argument("ID ["+ iid +"] not found");
	}
		
};

