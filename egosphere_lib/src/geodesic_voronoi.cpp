/*
 * Put copyright notice here
 */
#include <egosphere/geodesic_voronoi.h>

namespace egosphere {
	
	GeoVoronoiVertex::GeoVoronoiVertex(GeoVertex *v1, GeoVertex *v2, GeoVertex *v3) :
			wrap(false)
	{
		Biangle const *b1=v1->getBiangle();
		Biangle const *b2=v2->getBiangle();
		Biangle const *b3=v3->getBiangle();
		Biangle *m1_2, *m1_3, *m2_3;
		double d1_2, d1_3, d2_3;
		bool dbg = false;
		m1_2 = Utils::midpoint(b1, b2);
		if (dbg) {
			std::cout << "Midpoint m1_2 (" << b1->str() << ") & (" << b2->str() << ") -> ";
			std::cout << "(" << m1_2->str() << ")" << std::endl;
		}
		m1_3 = Utils::midpoint(b1, b3);
		if (dbg) {
			std::cout << "Midpoint m1_3 (" << b1->str() << ") & (" << b3->str() << ") -> ";
			std::cout << "(" << m1_3->str() << ")" << std::endl;
		}
		m2_3 = Utils::midpoint(b2, b3);
		if (dbg) {
			std::cout << "Midpoint m2_3 (" << b2->str() << ") & (" << b3->str() << ") -> ";
			std::cout << "(" << m2_3->str() << ")" << std::endl;
		}
		d1_2 = Utils::direction(b2, m1_3, b1);
		d1_3 = Utils::direction(b3, m1_2, b1);
		d2_3 = Utils::direction(b3, m1_2, b2);
		// always want ev1 CCW ev2-m13, CW ev3-m12; ev2 CCW ev3-m12
		if (d1_2 < 0.0) {
			if (d1_3 < 0.0) {
				ev1 = v1; m23 = m2_3;
				if (d2_3 < 0.0) {
					ev2 = v2;   ev3 = v3;
					m13 = m1_3; m12 = m1_2;
				} else {
					ev2 = v3;   ev3 = v2;
					m13 = m1_2; m12 = m1_3;
				}
			} else {
				ev1 = v2;   ev2 = v3;   ev3 = v1;
				m23 = m1_3; m13 = m1_2; m12 = m2_3;
			}
		} else {
			if (d1_3 < 0.0) {
				ev1 = v2;   ev2 = v1;   ev3 = v3;
				m23 = m1_3; m13 = m2_3; m12 = m1_2;
			} else {
				ev1 = v3; m23 = m1_2;
				if (d2_3 < 0.0) {
					ev2 = v1;   ev3 = v2;
					m13 = m2_3; m12 = m1_3;
				} else {
					ev2 = v2;   ev3 = v1;
					m13 = m1_3; m12 = m2_3;
				}
			}
		}
		b1 = ev1->getBiangle(); b2 = ev2->getBiangle(); b3 = ev3->getBiangle();
		m1_2 = Utils::intersection(b1, m23, b2, m13);
		if (dbg) {
			std::cout << "Intersection m1_2:" << std::endl;
			std::cout << "    (" << b1->str() << ") & (" << m23->str() << ")" << std::endl;
			std::cout << "    (" << b2->str() << ") & (" << m13->str() << ")" << std::endl;
			std::cout << "    (" << m1_2->str() << ")" << std::endl;
		}
		m1_3 = Utils::intersection(b1, m23, b3, m12);
		if (dbg) {
			std::cout << "Intersection m1_3:" << std::endl;
			std::cout << "    (" << b1->str() << ") & (" << m23->str() << ")" << std::endl;
			std::cout << "    (" << b3->str() << ") & (" << m12->str() << ")" << std::endl;
			std::cout << "    (" << m1_3->str() << ")" << std::endl;
		}
		m2_3 = Utils::intersection(b2, m13, b3, m12);
		if (dbg) {
			std::cout << "Intersection m2_3:" << std::endl;
			std::cout << "    (" << b2->str() << ") & (" << m13->str() << ")" << std::endl;
			std::cout << "    (" << b3->str() << ") & (" << m12->str() << ")" << std::endl;
			std::cout << "    (" << m2_3->str() << ")" << std::endl;
		}
		bool xd12, xd13, xd23;
		xd12 = fabs(b1->theta - b2->theta) > M_PI;
		xd13 = fabs(b1->theta - b3->theta) > M_PI;
		xd23 = fabs(b2->theta - b3->theta) > M_PI;
		if (xd12 || xd13 || xd23) {
			// not all points on one side of discontinuity, so wrap
			if (dbg) std::cout << "  And that's a wrap" << std::endl;
			wrap = true;
		}
		// an issue if thetas split over PI/-PI discontinuity; take the
		// best 2 of 3 majority's sign and change the odd one's sign to match
		if (wrap) {
			double sc = sign(m1_2->theta) + sign(m1_3->theta) + sign(m2_3->theta);
			if ((sc > 0.0 && m1_2->theta < 0.0) ||
			    (sc < 0.0 && m1_2->theta > 0.0)) {
				m1_2->theta *= -1;
			}
			if ((sc > 0.0 && m1_3->theta < 0.0) ||
			    (sc < 0.0 && m1_3->theta > 0.0)) {
				m1_3->theta *= -1;
			}
			if ((sc > 0.0 && m2_3->theta < 0.0) ||
			    (sc < 0.0 && m2_3->theta > 0.0)) {
				m2_3->theta *= -1;
			}
			double avg = (m1_2->theta + m1_3->theta + m2_3->theta)/3.0;
			biangle = new Biangle(avg, (m1_2->phi + m1_3->phi + m2_3->phi)/3.0, false);
		} else {
			biangle = new Biangle((m1_2->theta + m1_3->theta + m2_3->theta)/3.0,
			                      (m1_2->phi + m1_3->phi + m2_3->phi)/3.0, false);
		}
		// finally, set the vertices [min|max]Voro[Theta|Phi] values
		ev1->setVoroAngs(biangle, wrap);
		ev2->setVoroAngs(biangle, wrap);
		ev3->setVoroAngs(biangle, wrap);
		delete m1_2; delete m1_3; delete m2_3;
	}
	
	GeoVoronoiVertex::~GeoVoronoiVertex() {
		if (biangle) delete biangle;
		if (m12) delete m12;
		if (m13) delete m13;
		if (m23) delete m23;
	}
	
	bool GeoVoronoiVertex::operator<(const GeoVoronoiVertex &other) {
		return *biangle < *(other.biangle);
	}
	
	std::string GeoVoronoiVertex::str() const {
		std::ostringstream oss;
		oss << std::fixed << std::setprecision(2);
		oss << toDeg(biangle->theta) << ", " << toDeg(biangle->phi);
		return oss.str();
	}
	
	std::string GeoVoronoiVertex::vert_str() const {
		std::ostringstream oss;
		oss << std::fixed << std::setprecision(2);
		oss << "ev1: d=" << hypot(*biangle, *(ev1->getBiangle())) << ", ";
		oss << "(" << ev1->ang_str() << ")\t";
		oss << "ev2: d=" << hypot(*biangle, *(ev2->getBiangle())) << ", ";
		oss << "(" << ev2->ang_str() << ")\t";
		oss << "ev3: d=" << hypot(*biangle, *(ev3->getBiangle())) << ", ";
		oss << "(" << ev3->ang_str() << ")";
		return oss.str();
	}
	
	double GeoVoronoiVertex::minVertexDistance() const {
		double d=M_PI; // unit sphere; calc'd min always < PI
		d = std::min(d, hypot(*biangle, *(ev1->getBiangle())));
		d = std::min(d, hypot(*biangle, *(ev2->getBiangle())));
		d = std::min(d, hypot(*biangle, *(ev3->getBiangle())));
		return d;
	}
	
	double GeoVoronoiVertex::maxVertexDistance() const {
		double d=0.0;
		d = std::max(d, hypot(*biangle, *(ev1->getBiangle())));
		d = std::max(d, hypot(*biangle, *(ev2->getBiangle())));
		d = std::max(d, hypot(*biangle, *(ev3->getBiangle())));
		return d;
	}
	
	GeoVertex* GeoVoronoiVertex::nearestGeoVertex(Biangle const *b) {
		double d1_23, d2_13, d3_12;
		d1_23 = Utils::direction(ev1->getBiangle(), m23, b);
		d2_13 = Utils::direction(ev2->getBiangle(), m13, b);
		if (d1_23 >= 0.0 && d2_13 < 0.0) {
			// in ev3's region; CW d1_23, CCW d2_13
			return ev3;
		}
		d3_12 = Utils::direction(ev3->getBiangle(), m12, b);
		//if (d1_23 < 0.0 && d3_12 >= 0.0) {
		if (d3_12 >= 0.0) {
			// in ev2's region; CCW d1_23, CW d3_12
			return ev2;
		}
		// must be in ev1's region; CW d2_13, CCW d3_12
		return ev1;
	}
	
};
