/*
 * Put copyright notice here
 */

#include <egosphere/geodesic.h>
#include <egosphere/egosphere_utils.h>
#include <egosphere/feature_external.hpp>

namespace egosphere_demo {
	
	FeatureExternal::FeatureExternal(int f) {
		egosphere::Geosphere geo(f);
		m_freq = geo.frequency();
		egosphere::GeosphereMetainfo mi = geo.getMetainfo();
		m_minAngle = mi.minVertD;
		m_maxAngle = mi.maxVertD;
		m_egosearch = geo.getEgoStructs(m_vertices);
	}
	
	FeatureExternal::~FeatureExternal() {
		std::vector<egosphere::EgoVertex*>::iterator vit;
		for (vit=m_vertices.begin(); vit<m_vertices.end(); vit++) {
			delete *vit;
		}
	}
	
	std::vector<egosphere::EgoVertex*> FeatureExternal::getVerts() const {
		return m_vertices;
	}
	
	int FeatureExternal::getFrequency() const {
		return m_freq;
	}
	
	double FeatureExternal::getMinAngle(bool inDegrees) const {
		return (inDegrees ? egosphere::toDeg(m_minAngle) : m_minAngle);
	}
	
	double FeatureExternal::getMaxAngle(bool inDegrees) const {
		return (inDegrees ? egosphere::toDeg(m_maxAngle) : m_maxAngle);
	}
	
	egosphere::EgoSearch FeatureExternal::getSearch() const {
		return m_egosearch;
	}

};

