/**
 * Put copyright notice here
 */
#include <egosphere/feature_external_node.hpp>
#include <egosphere/egosphere_ros_utils.hpp>
#include <tf/transform_datatypes.h>
#include <time.h>   /* time, support rand */
#include <list>
#include <sstream>

namespace egosphere_demo {
	
	namespace ext {
		ExternalInfo::ExternalInfo(const std::string &egoFrame,
		                           const std::string &extFrame)
				: status(UNKNOWN)
		{
			// pose will (eventually) be relative to egosphere center
			geometry_msgs::Pose extPose;
			extPose.orientation.w = 1.0;
			addExtMarker(egoFrame, extFrame, extPose);
			addEgoMarkers(egoFrame, extFrame);
		}
		
		ExternalInfo::~ExternalInfo() {
		}
		
		void ExternalInfo::updateMarkers(const geometry_msgs::Pose &repPose,
		                                 const geometry_msgs::Pose &biPose,
		                                 const geometry_msgs::Pose &facePose,
		                                 const std::vector<geometry_msgs::Point> &facePoints) {
			if (status == READY) {
				status = UPDATE;
			} else if (status == UPDATE) {
				//ROS_INFO_STREAM_THROTTLE(1, "updateMarkers: updating poses");
				//ROS_INFO_STREAM_THROTTLE(1, "updateMarkers: repPose " << repPose << "biPose " << biPose << "facePose " << facePose);
				//ROS_INFO_STREAM("updateMarkers: repPose " << repPose << "biPose " << biPose << "facePose " << facePose);
				externals.markers[REP].pose = repPose;
				externals.markers[BI].pose = biPose;
				externals.markers[FACE].pose = facePose;
				externals.markers[FACE].points = facePoints;
				
				// direction arrow needs special handling...
				egosphere::Point egoPoint;
				egosphere::RosUtils::pointMsgToPoint(repPose.position, egoPoint);
				externals.markers[ARROW].scale.x = egosphere::distance(egoPoint);
				externals.markers[ARROW].pose.orientation = biPose.orientation;
				//externals.markers[ARROW].points[1] = repPose.position;
				//ROS_INFO_STREAM("updateMarkers: marker array:" << std::endl << externals);
				//applyMarkerUpdate();
			} else {
				ROS_ERROR("Attempt to update %s in non-UPDATE state!", getFrame().c_str());
			}
		}
		
		const InfoStat ExternalInfo::getStatus() const {
			return status;
		}
		
		const MarkerArray& ExternalInfo::getMarkerArr() const {
			return externals;
		}
		
		const std::string& ExternalInfo::getFrame() const {
			return externals.markers[REP].header.frame_id;
		}
		
		const geometry_msgs::Pose& ExternalInfo::getPose() const {
			return externals.markers[REP].pose;
		}
		
		// ***** private methods
		void ExternalInfo::addExtMarker(const std::string &egoFrame,
		                                const std::string &extFrame,
		                                const geometry_msgs::Pose &extPose) {
			Marker rep;
			//egosphere::RosUtils::initMarker(egosphere::highlite::DIRECTION,
			egosphere::RosUtils::initMarker(egosphere::highlite::IMPRESSION,
			              egoFrame, extPose, rep);
			rep.id = REP;
			rep.ns = extFrame;
			//rep.scale = egosphere::RosUtils::getScale(egosphere::highlite::DIRECTION, 0.3);
			rep.color.r=1.0; rep.color.g=1.0; rep.color.b=1.0; rep.color.a=1.0;
			//rep.ns = extFrame +"/impression";
			externals.markers.clear();
			externals.markers.push_back(rep);
			status = InfoStat::ADD;
			//ROS_INFO_STREAM("addExtMarker REP:" << std::endl << rep);
		}
		
		void ExternalInfo::addEgoMarkers(const std::string &egoFrame,
		                                 const std::string &extFrame) {
			if (status != InfoStat::ADD) {
				ROS_ERROR("ExternalInfo status not ADD!");
				if (externals.markers.size() == 1) {
					ROS_INFO("Already have REP marker; assume good");
					addExtMarker(getFrame(), extFrame, getPose());
				} else {
					ROS_ERROR("Have %lu ExternalInfo markers!", externals.markers.size());
					return;
				}
			}
			// poses will (eventually) be relative to external frame position
			Marker bi;
			geometry_msgs::Pose egoPose;
			egoPose.orientation.w = 1.0;
			egosphere::RosUtils::initMarker(egosphere::highlite::READING,
			              egoFrame, egoPose, bi);
			bi.id = BI;
			bi.ns = extFrame;
			//bi.ns = extFrame +"/biangle";
			//bi.scale = egosphere::RosUtils::getScale(egosphere::highlite::READING, 0.1);
			externals.markers.push_back(bi);
			//ROS_INFO_STREAM("addEgoMarkers BI:" << std::endl << bi);
			
			Marker arrow;
			egosphere::RosUtils::initMarker(egosphere::highlite::DIRECTION,
			              egoFrame, egoPose, arrow);
			arrow.id = ARROW;
			arrow.ns = extFrame;
			arrow.pose.position.x = 0.0;
			arrow.pose.position.y = 0.0;
			arrow.pose.position.z = 0.0;
			// control arrowhead length; buries tip in rep object!
			//arrow.points.push_back(arrow.pose.position);
			//arrow.points.push_back(getPose().position);
			//arrow.scale.x = 0.01;
			//arrow.scale.y = 0.02;
			//arrow.scale.z = 0.03;
			externals.markers.push_back(arrow);
			//ROS_INFO_STREAM("addEgoMarkers ARROW:" << std::endl << arrow);
			
			Marker face;
			egosphere::RosUtils::initMarker(egosphere::highlite::FACE,
			              egoFrame, egoPose, face);
			face.id = FACE;
			face.ns = extFrame;
			//face.ns = extFrame +"/face";
			externals.markers.push_back(face);
			//ROS_INFO_STREAM("addEgoMarkers FACE:" << std::endl << face);
			status = InfoStat::READY;
		}
	}
	
	FeatureExternalNode::FeatureExternalNode(ros::NodeHandle& nh)
			: m_featExternal(NULL)
			, m_timerPubRate(ros::Duration(0.01))
			, m_timerTfRate(ros::Duration(0.01))
			, m_egoVertMarkersPublish(true)
			, m_externalsPublish(true)
	{
		ROS_INFO("FeatureExternalNode: in constructor...");
		// *** load the parameters
		int rate, freq;
		nh.param("frequency", freq, 8);
		nh.param("radius", m_radius, 1.0);
		nh.param("pub_rate", rate, 10);
		m_timerPubRate = ros::Duration(rate / 1000.0);
		nh.param("tf_rate", rate, 10);
		m_timerTfRate = ros::Duration(rate / 1000.0);
		nh.param("center_frame", m_center, std::string("ego_center"));
		nh.param("global_frame", m_ground, std::string("ego_global"));
		// TODO: frame list from launch file not working!
		std::string frames;
		nh.param("item_list", frames, std::string("NOTSET"));
		//ROS_INFO("item_list as string: %s", frames.c_str());
		std::vector<std::string> itemFrames;
		nh.getParam("item_list", itemFrames);
		for (unsigned int i=0; i<itemFrames.size(); ++i) {
			addExternal(itemFrames[i]);
		}
		nh.param("show_vertices", m_egoVertMarkersPublish, true);
		nh.param("show_externals", m_externalsPublish, true);
		
		// *** create egosphere and related
		m_name = ros::this_node::getName();
		m_featExternal = new FeatureExternal(freq);
		egosphere::RosUtils::fillMarkerEgoVerts(m_center, m_radius,
		                                        m_featExternal->getVerts(),
		                                        m_featExternal->getSearch(),
															 m_egoVertMarkers);
		//ROS_INFO_STREAM("FeatureExternalNode EgoVerts:" << std::endl << m_egoVertMarkers);
		m_egoVertMarkers_pub = nh.advertise<MarkerArray>(m_name +"/markers", 1);
		
		// *** create "externals" and related
		if (!m_tfliPtr) {
			m_tfliPtr.reset(new tf::TransformListener());
		}
		// note: using same topic as ego verts!
		m_externals_pub = nh.advertise<MarkerArray>(m_name +"/markers", 1);
		
		// *** create TF i/o and related
		if (!m_tfbrPtr) {
			m_tfbrPtr.reset(new tf::TransformBroadcaster());
		}
		m_externalsAddFrame_sub = nh.subscribe(m_name +"/external_add_frame", 1, &FeatureExternalNode::addExternalFrameCb, this);
		m_externalsAdd_sub = nh.subscribe(m_name +"/external_add", 1, &FeatureExternalNode::addExternalCb, this);
		m_externalsPointSet_sub = nh.subscribe(m_name +"/external_location", 1, &FeatureExternalNode::setExternalTfPointCb, this);
		m_externalsPoseSet_sub = nh.subscribe(m_name +"/external_pose", 1, &FeatureExternalNode::setExternalTfPoseCb, this);
		m_externalsRemove_sub = nh.subscribe(m_name +"/external_remove", 1, &FeatureExternalNode::removeExternalTfCb, this);
		
		// *** exiting steps
		m_timerPub = nh.createTimer(m_timerPubRate, &FeatureExternalNode::pubTimer, this);
		m_timerTf = nh.createTimer(m_timerTfRate, &FeatureExternalNode::tfTimer, this);
		try {
			m_tfliPtr->waitForTransform(m_center, m_ground, ros::Time::now(), ros::Duration(3.0));
		} catch (tf::TransformException &ex) {
					ROS_ERROR("Frame lookup [%s], [%s]: %s", m_center.c_str(), m_ground.c_str(), ex.what());
		}
		showConfig();
		ROS_INFO("FeatureExternalNode: exiting constructor");
	}
	
	FeatureExternalNode::~FeatureExternalNode() {
		if (m_featExternal) delete m_featExternal;
	}
	
	void FeatureExternalNode::showConfig() const {
		ROS_INFO("-----------------------------------------------");
		ROS_INFO("  Egosphere configuration:");
		ROS_INFO("          node name: %s", m_name.c_str());
		ROS_INFO("          frequency:    %d", m_featExternal->getFrequency());
		ROS_INFO("    min angle (deg): %6.3f", m_featExternal->getMinAngle(true));
		ROS_INFO("    max angle (deg): %6.3f", m_featExternal->getMaxAngle(true));
		ROS_INFO("       global frame: %s", m_ground.c_str());
		ROS_INFO("       center frame: %s", m_center.c_str());
		ROS_INFO("  sphere radius (m): %5.2f", m_radius);
		ROS_INFO("  display rate (Hz): %5.2f", (m_timerPubRate.toSec() * 1000.0));
		ROS_INFO("       TF rate (Hz): %5.2f", (m_timerTfRate.toSec() * 1000.0));
		ROS_INFO("-----------------------------------------------");
	}
	
	void FeatureExternalNode::pubTimer(const ros::TimerEvent &te) const {
		// note: timer-event triggered, so no mutex coordination among
		//   pubs, tf lookups, and subscriber data entry needed!
		//ROS_INFO("pubTimer: publishing...");
		if (m_egoVertMarkersPublish) {
			pubEgoVerts();
		}
		pubExternals();
		//ROS_INFO("pubTimer: exiting...");
	}
	
	void FeatureExternalNode::tfTimer(const ros::TimerEvent &te) const {
		// note: timer-event triggered, so no mutex coordination among
		//   pubs, tf lookups, and subscriber data entry needed!
		// note: broadcast puts registered frame/poses into TF system
		//   updateExternals looks up frame/poses and updates ExternalInfos
		//   these are purposely separate, as if TF didn't depend on local
		//   registration/data by subscriber(s)
		//ROS_INFO("tfTimer: broadcasting...");
		broadcast();
		//ROS_INFO("tfTimer: updating externals...");
		updateExternals();
		//ROS_INFO("tfTimer: exiting...");
	}
	
	void FeatureExternalNode::pubEgoVerts() const {
		if (m_egoVertMarkersPublish) {
			m_egoVertMarkers_pub.publish(m_egoVertMarkers);
		}
	}
	
	void FeatureExternalNode::addExternalFrameCb(const std_msgs::String::ConstPtr &msg) {
		addExternal(msg->data);
	}
	
	bool FeatureExternalNode::addExternal(const std::string &extFrame) {
		ROS_INFO("addExternal: adding [%s]", extFrame.c_str());
		ext::ExternalInfo info(m_center, extFrame);
		m_externals.insert(std::pair<std::string,ext::ExternalInfo>(extFrame,info));
		ROS_INFO("addExternal: added [%s]", extFrame.c_str());
	}
	
	bool FeatureExternalNode::removeExternal(const std::string &extFrame) {
		// TODO: remove an external item to 'm_externals' map
	}
	
	void FeatureExternalNode::updateExternals() const {
		std::map<std::string, ext::ExternalInfo>::const_iterator it;
		for (it=m_externals.begin(); it!=m_externals.end(); ++it) {
			// check external's status (add, remove, update)
			std::string extFrame(it->first);
			switch (it->second.getStatus()) {
			case ext::READY:
			case ext::UPDATE:
				try {
					geometry_msgs::Pose repPose = getPoseViaTf(m_tfliPtr, m_center, extFrame);
					geometry_msgs::Pose biPose = getPoseRadius(m_radius, repPose);
					geometry_msgs::Pose facePose = getPoseFace(m_featExternal, repPose);
					// working out how to stuff FACE points...
					egosphere::Point extPoint;
					egosphere::RosUtils::pointMsgToPoint(repPose.position, extPoint);
					egosphere::Biangle relBi;
					egosphere::Utils::calcBiangle(extPoint, relBi);
					egosphere::EgoSearch search = m_featExternal->getSearch();
					unsigned int ixExt = search.nearest(relBi);
					std::vector<egosphere::EgoVertex*> verts = m_featExternal->getVerts();
					std::vector<geometry_msgs::Point> fPts;
					egosphere::RosUtils::fillFacePointMsgVec(verts[ixExt], fPts);
					const_cast<ext::ExternalInfo&>(it->second).updateMarkers(repPose, biPose, facePose, fPts);
				} catch (tf::TransformException &ex) {
					continue;
				}
				break;
			case ext::REMOVE:
				// remove: delete from map
				ROS_INFO("updateExternals: %s status REMOVE", extFrame.c_str());
				break;
			default:
				//ROS_INFO("updateExternals: %s unexpected status", extFrame.c_str());
				break;
			}
		}
	}
	
	void FeatureExternalNode::pubExternals() const {
		if (m_externalsPublish) {
			std::map<std::string, ext::ExternalInfo>::const_iterator it;
			for (it=m_externals.begin(); it!=m_externals.end(); ++it) {
				m_externals_pub.publish(it->second.getMarkerArr());
			}
		}
	}
	
	void FeatureExternalNode::addExternalCb(const std_msgs::String::ConstPtr &msg) {
		// TODO: key off m_extTfs? if so, DO NOT addExternal!
		addExternal(msg->data);
		addExternalTf(msg->data);
	}
	
	void FeatureExternalNode::addExternalTf(const std::string &extFrame) {
		ROS_INFO("addExternalTf: adding [%s]", extFrame.c_str());
		tf::Transform transform;
		transform.setIdentity();
		m_extTfs[extFrame] = transform;
		ROS_INFO("addExternalTf: added [%s]", extFrame.c_str());
	}
	
	void FeatureExternalNode::removeExternalTfCb(const std_msgs::String::ConstPtr &msg) {
		// TODO: remove an external item to 'm_externals' map
	}
	
	void FeatureExternalNode::setExternalTfPointCb(
	                        const geometry_msgs::PointStamped::ConstPtr &msg) {
		if (m_extTfs.count(msg->header.frame_id) > 0) {
			ROS_INFO_STREAM("setExternalTfPointCb got: frame [" << msg->header.frame_id << "], point:" << std::endl << msg->point);
			// convert point to egosphere->external; assume position specifies
			// provided frame in global coordinates
			//geometry_msgs::Point extPoint = getPointViaTf(m_tfliPtr, m_center, m_ground);
			// set m_extTfs point, allow broadcast/updateExternals to do the rest
			//updateExternalTf(msg->header.frame_id, extPoint);
			updateExternalTf(msg->header.frame_id, msg->point);
			ROS_INFO("setExternalTfPointCb end");
		}
	}
	
	void FeatureExternalNode::setExternalTfPoseCb(
	                        const geometry_msgs::PoseStamped::ConstPtr &msg) {
		if (m_extTfs.count(msg->header.frame_id) > 0) {
			ROS_INFO_STREAM("setExternalTfPoseCb got: frame [" << msg->header.frame_id << "], pose:" << std::endl << msg->pose);
			// convert pose to egosphere->external; assume pose specifies
			// provided frame in global coordinates/orientation
			//geometry_msgs::Pose extPose = getPoseViaTf(m_tfliPtr, m_center, m_ground);
			// set m_extTfs pose, allow broadcast/updateExternals to do the rest
			//updateExternalTf(msg->header.frame_id, extPose);
			updateExternalTf(msg->header.frame_id, msg->pose);
			ROS_INFO("setExternalTfPoseCb end");
		}
	}
	
	void FeatureExternalNode::broadcast() const {
		//ROS_INFO_THROTTLE(1.0, "broadcast: starting...");
		std::map<std::string, tf::Transform>::const_iterator it;
		for (it=m_extTfs.begin(); it!=m_extTfs.end(); ++it) {
			geometry_msgs::Pose geoPose;
			tf::poseTFToMsg(it->second, geoPose);
			//ROS_INFO_STREAM("broadcast: frame [" << it->first << "], pose:" << std::endl << geoPose);
			m_tfbrPtr->sendTransform(tf::StampedTransform(it->second, ros::Time::now(), m_ground, it->first));
		}
		//ROS_INFO_THROTTLE(1.0, "broadcast: exiting...");
	}
	
	void FeatureExternalNode::updateExternalTf(const std::string &extFrame,
	                                  const geometry_msgs::Point &point) {
		// update tranform map (for broadcasting)
		std::map<std::string, tf::Transform>::const_iterator it;
		it = m_extTfs.find(extFrame);
		if (it != m_extTfs.end()) {
			ROS_INFO_STREAM("updateExternalTf: frame [" << extFrame << "], point in:" << std::endl << point);
			tf::Point tfPoint;
			tf::pointMsgToTF(point, tfPoint);
			const_cast<tf::Transform&>(it->second).setOrigin(tfPoint);
			geometry_msgs::Pose geoPose;
			tf::poseTFToMsg(it->second, geoPose);
			ROS_INFO_STREAM("updateExternalTf: frame [" << extFrame << "], pose out:" << std::endl << geoPose);
		}
		// TODO: don't need to update externals 'cuz broadcast?
	}
	
	void FeatureExternalNode::updateExternalTf(const std::string &extFrame,
	                                  const geometry_msgs::Pose &pose) {
		// update tranform map (for broadcasting)
		std::map<std::string, tf::Transform>::const_iterator it;
		it = m_extTfs.find(extFrame);
		if (it != m_extTfs.end()) {
			ROS_INFO_STREAM("updateExternalTf: frame [" << extFrame << "], pose in:" << std::endl << pose);
			tf::Pose tfPose;
			tf::poseMsgToTF(pose, tfPose);
			m_extTfs[extFrame] = tfPose;
			geometry_msgs::Pose geoPose;
			tf::poseTFToMsg(it->second, geoPose);
			ROS_INFO_STREAM("updateExternalTf: frame [" << extFrame << "], pose out:" << std::endl << geoPose);
		}
		// TODO: don't need to update externals 'cuz broadcast?
	}
	
	geometry_msgs::Point FeatureExternalNode::getPointViaTf(
	                                 std::shared_ptr<tf::TransformListener> tf,
	                                 const std::string &target,
	                                 const std::string &source) const {
		// from ROS docs, applied transform puts source data into target
		// - like Transformer::transformPoint, without Stamped<Point> creation
		// listener->transformPoint:
		//    target-frame, Stamped<tf::Point-in>, Stamped<tf::Point-out>
		// listener->transformPoint:
		//    target-frame, ros::Time, Stamped<tf::Point-in>, fixed-frame, Stamped<tf::Point-out>
		tf::StampedTransform transform;
		tf->lookupTransform(target, source, ros::Time(0), transform);
		geometry_msgs::Point point;
		tf::pointTFToMsg(transform.getOrigin(), point);
		return point;
	}
	
	geometry_msgs::Pose FeatureExternalNode::getPoseViaTf(
	                                 std::shared_ptr<tf::TransformListener> tf,
	                                 const std::string &target,
	                                 const std::string &source) const {
		// from ROS docs, applied transform puts source data into target
		// - like Transformer::transformPose, without Stamped<Pose> creation
		tf::StampedTransform transform;
		tf->lookupTransform(target, source, ros::Time(0), transform);
		geometry_msgs::Pose pose;
		tf::poseTFToMsg(transform, pose);
		//ROS_INFO_STREAM("getPoseViaTf: source [" << source << "], target [" << target << "], pose:" << std::endl << pose);
		return pose;
	}
	
	geometry_msgs::Pose FeatureExternalNode::getPoseRadius(double radius,
	                                 const geometry_msgs::Pose &extPose) const {
		//ROS_INFO_STREAM("getPoseRadius: extPose:" << std::endl << extPose);
		// convert ROS position to egosphere::Point
		egosphere::Point extPoint;
		egosphere::RosUtils::pointMsgToPoint(extPose.position, extPoint);
		//ROS_INFO_STREAM("getPoseRadius: extPoint: [" << extPoint.str() << "]");
		// get egosphere biangle for point
		egosphere::Biangle relBi;
		egosphere::Utils::calcBiangle(extPoint, relBi);
		relBi.mag = radius;
		//ROS_INFO_STREAM("getPoseRadius: relBi: [" << relBi.str() << "]");
		// get egosphere::Point on surface
		egosphere::Point relPoint;
		egosphere::Utils::calcPoint(relBi, relPoint, radius);
		//ROS_INFO_STREAM("getPoseRadius: relPoint: [" << relPoint.str() << "]");
		// create/return surface point as ROS pose
		geometry_msgs::Pose pose;
		egosphere::RosUtils::pointToPointMsg(relPoint, pose.position);
		egosphere::RosUtils::biangleToQuaternionMsg(&relBi, pose.orientation);
		//ROS_INFO_STREAM("getPoseRadius: center [" << m_center << "], biangle: [" << relBi.str() << "], pose:" << std::endl << pose);
		return pose;
	}
	
	geometry_msgs::Pose FeatureExternalNode::getPoseFace(
	                                 const FeatureExternal *ego,
	                                 const geometry_msgs::Pose &extPose) const {
		//unsigned int relFace = ;
		geometry_msgs::Pose pose;
		pose.orientation.w = 1.0;
		return pose;
	}
	
	std::string FeatureExternalNode::tfPointStr(const tf::Point &tfPoint) const {
		std::stringstream oss;
		oss << "[" << tfPoint.x() << ", " << tfPoint.y() << ", " << tfPoint.z() << "]";
		return oss.str();
	}
	
};

int main(int argc, char **argv) {
	//std::string name("egosphere_movable");
	ros::init(argc, argv, "feature_external_node");
	ros::NodeHandle nh("~");
	egosphere_demo::FeatureExternalNode egoNode(nh);
	ros::spin();
	return(0);
}

