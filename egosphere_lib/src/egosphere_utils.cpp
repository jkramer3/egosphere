/*
 * Put copyright notice here
 */

#include <egosphere/egosphere_utils.h>
#include <egosphere/ego_path.hpp>
//#include <tf/transform_datatypes.h>

namespace egosphere {
	
	double Utils::distance(double angD, double radius) {
		Biangle b(angD, angD);
		return distance(b, radius);
	}
	
	double Utils::distance(const Biangle& b, double radius) {
		Biangle bz(0.0, 0.0);
		return distance(b, bz, radius);
	}
	
	double Utils::distance(const Biangle& b1, Biangle& b2, double radius) {
		Point p1, p2;
		calcPoint(b1, p1, radius, false);
		calcPoint(b2, p2, radius, false);
		return sqrt(pow(p1.x-p2.x, 2) + pow(p1.y-p2.y, 2) + pow(p1.z-p2.z, 2));
	}
		
	void Utils::calcPoint(const Biangle& src, Point& dst, double radius, bool wMag) {
		if (anglesWithin(src.phi, M_PI/2.0, 2.0e-8)) {
			dst.x = 0.0;
			dst.y = 0.0;
			dst.z = radius;
		} else if (anglesWithin(src.phi, -M_PI/2.0, 2.0e-8)) {
			dst.x = 0.0;
			dst.y = 0.0;
			dst.z = -radius;
		} else {
			dst.x = src.costh * src.cosph * radius;
			dst.y = src.sinth * src.cosph * radius;
			dst.z = src.sinph * radius;
			if (wMag) {
				dst.x *= src.mag;
				dst.y *= src.mag;
				dst.z *= src.mag;
			}
		}
	}
	
	Point* Utils::calcPoint(Biangle const *b, double radius) {
		Point* p_ptr = new Point();
		calcPoint(*b, *p_ptr, radius);
		return p_ptr;
	}
	
	void Utils::calcBiangle(const Point& src, Biangle& dst) {
		//double r = sqrt((src.x*src.x) + (src.y*src.y) + (src.z*src.z));
		dst.mag   = sqrt((src.x*src.x) + (src.y*src.y) + (src.z*src.z));
		dst.theta = norm_ang(atan2(src.y, src.x));
		dst.phi   = norm_ang(asin(src.z / dst.mag));
		dst.costh = cos(dst.theta);
		dst.sinth = sin(dst.theta);
		dst.cosph = cos(dst.phi);
		dst.sinph = sin(dst.phi);
	}
	
	Biangle* Utils::calcBiangle(Point *p) {
		Biangle *b_ptr = new Biangle();
		calcBiangle(*p, *b_ptr);
		return b_ptr;
	}
	
	bool Utils::within(Point const *p1, Point const *p2, double d) {
		return fabs(p2->x-p1->x) <= d &&
		       fabs(p2->y-p1->y) <= d &&
		       fabs(p2->z-p1->z) <= d;
	}
	
	bool Utils::within(Biangle const *b1, Biangle const *b2, double rad) {
		return anglesWithin(b1->theta, b2->theta, rad) &&
		       anglesWithin(b1->phi, b2->phi, rad);
	}
	
	bool Utils::aligned(Biangle const *b1, Biangle const *b2,
	                    double thoffset, double thdiff,
	                    double phoffset, double phdiff) {
		bool th_al = anglesAligned(b1->theta, b2->theta, thoffset, thdiff);
		bool ph_al = anglesAligned(b1->phi, b2->phi, phoffset, phdiff);
		return th_al && ph_al;
	}
	
	Biangle* Utils::midpoint(Biangle const *b1, Biangle const *b2) {
		return fracpoint(b1, b2, 0.5);
	}
	
	Biangle* Utils::fracpoint(Biangle const *b1, Biangle const *b2, double frac) {
		Point p;
		double d, f, m;
		d = hypot(*b1, *b2);
		f = sin(frac*d) / sin(d);
		p.x = (f * b1->cosph * b1->costh) + (f * b2->cosph * b2->costh);
		p.y = (f * b1->cosph * b1->sinth) + (f * b2->cosph * b2->sinth);
		p.z = (f * b1->sinph)             + (f * b2->sinph);
		m = (b1->mag + b2->mag) * frac;
		return new Biangle(atan2(p.y, p.x), atan2(p.z, hypot(p.x, p.y)), m);
	}
	
	double Utils::bearing(Biangle const *b1, Biangle const *b2, bool type) {
		double d;
		if (type) {
			double x, y, dth, EPS=2.0e-8;
			if (b1->cosph < EPS) {
				return b1->phi > 0.0 ? M_PI : -M_PI;
			}
			dth = angleBetween(b2->theta, b1->theta);
			y = sin(dth) * b2->cosph;
			x = (b1->cosph * b2->sinph) - (b1->sinph * b2->cosph * cos(dth));
			d = atan2(y, x);
		} else {
			// 'final' bearing is adjusted initial bearing from end to start!
			d = bearing(b2, b1, true);
			d = norm_ang(d + M_PI);
		}
		return d;
	}
	
	Biangle* Utils::destpoint(Biangle const *b, double ad, double bear) {
		// from: http://williams.best.vwh.net/avform.htm
		//   only when ad < M_PI/2, which should be OK
		double th, ph, EPS=2.0e-8;
		double sinad = sin(ad);
		ph = asin(b->sinph * cos(ad) + b->cosph * sinad * cos(bear));
		if (fabs(ph) > ((M_PI/2)-EPS)) {
			th = b->theta; // endpoint a pole
		} else {
			th = norm_ang(b->theta - asin(sin(bear) * sinad / cos(ph)));
		}
		return new Biangle(th, ph, b->mag);
	}
	
	double Utils::xtrack(Biangle const *ep1, Biangle const *ep2, Biangle const *b) {
		// NOTE: assumes radius of sphere is 1
		double d13, b13, b12, dxt, EPS=2.0e-8;
		d13 = hypot(*ep1, *b);     // angular distance from start to third
		b13 = bearing(ep1, b);   // init bearing from start to third
		b12 = bearing(ep1, ep2); // init bearing from start to end
		dxt = asin( sin(d13) * sin(b13 - b12) );
		if (dxt != dxt) {
			std::cerr << "Utils::xtrack: ERROR -- dxt is NaN!" << std::endl;
		}
		dxt = (fabs(dxt) < EPS ? 0.0 : dxt);
		// this is if we want to differentiate between -0.0 and 0.0
		// leaving it out because I don't think we ever care
		//if (dxt >= 0.0 && dxt < EPS) dxt = 0.0;
		//else if (dxt < 0.0 && dxt > -EPS) dxt = -0.0;
		return dxt;
	}
	
	double Utils::course(Biangle const *b1, Biangle const *b2) {
		double d, EPS=2.0e-8;
		if (b1->cosph < EPS) {
			return b1->phi > 0.0 ? M_PI : -M_PI;
		}
		d = hypot(*b1, *b2);
		if (sin(b2->theta - b1->theta) < 0) {
			double ltzero1 = (b2->sinph - b1->sinph * cos(d)) / (sin(d) * b1->cosph);
			ltzero1 = (ltzero1 > 1.0 ? 1.0 : ltzero1);
			ltzero1 = (ltzero1 < -1.0 ? -1.0 : ltzero1);
			double ltzero2 = acos(ltzero1);
			if (ltzero2 != ltzero2) {
				std::cerr << "Utils::course: ERROR -- ltzero is NaN!" << std::endl;
				std::cerr << "  b1=(" << b1->str() << "), b2=(" << b2->str() << ")" << std::endl;
			}
			return ltzero2;
		}
		double gtzero1 = (b2->sinph - b1->sinph * cos(d)) / (sin(d) * b1->cosph);
		gtzero1 = (gtzero1 > 1.0 ? 1.0 : gtzero1);
		gtzero1 = (gtzero1 < -1.0 ? -1.0 : gtzero1);
		double gtzero2 = 2*M_PI - acos(gtzero1);
		if (gtzero2 != gtzero2) {
			std::cerr << "Utils::course: ERROR -- gtzero is NaN!" << std::endl;
			std::cerr << "  b1=(" << b1->str() << "), b2=(" << b2->str() << ")" << std::endl;
		}
		return gtzero2;
	}

	double Utils::direction(Biangle const *ep1, Biangle const *ep2, Biangle const *b) {
		double EPS=2.0e-8;
		double dir = asin(sin(hypot(*ep1, *b)) * sin(course(ep1, b) - course(ep1, ep2)));
		if (dir != dir) {
			std::cerr << "Utils::direction: ERROR -- dir is NaN!" << std::endl;
		}
		dir = (fabs(dir) < EPS ? 0.0 : dir);
		// this is if we want to differentiate between -0.0 and 0.0
		// leaving it out because I don't think we ever care
		//if (dir >= 0.0 && dir < EPS) dir = 0.0;
		//else if (dir < 0.0 && dir > -EPS) dir = -0.0;
		return dir;
	}
	
	Biangle* Utils::intersection(Biangle const *b1, Biangle const *b2,
	                      Biangle const *b3, Biangle const *b4) {
		Point *p1, *p2, *p3, *p4;
		Point pa, pb, p13, p43, p21;
		double mua, mub, d1343, d4321, d1321, d4343, d2121;
		double numer,denom;
		double EPS = 2e-16;
		
		p1=calcPoint(b1); p2=calcPoint(b2); p3=calcPoint(b3); p4=calcPoint(b4);
		p13.x = p1->x - p3->x;
		p13.y = p1->y - p3->y;
		p13.z = p1->z - p3->z;
		p43.x = p4->x - p3->x;
		p43.y = p4->y - p3->y;
		p43.z = p4->z - p3->z;
		if (fabs(p43.x) < EPS && fabs(p43.y) < EPS && fabs(p43.z) < EPS) {
			std::cout << "intersection(): Point 4->3 has < " << EPS << "! ";
			std::cout << "(" << b4->str() << ") (" << b3->str() << ") -> ";
			std::cout << "(" << p43.str() << ")" << std::endl;
			return 0;
		}
		p21.x = p2->x - p1->x;
		p21.y = p2->y - p1->y;
		p21.z = p2->z - p1->z;
		if (fabs(p21.x) < EPS && fabs(p21.y) < EPS && fabs(p21.z) < EPS) {
			std::cout << "intersection(): Point 2->1 has < " << EPS << "! ";
			std::cout << "(" << b2->str() << ") (" << b1->str() << ") -> ";
			std::cout << "(" << p21.str() << ")" << std::endl;
			return 0;
		}
		d1343 = p13.x * p43.x + p13.y * p43.y + p13.z * p43.z;
		d4321 = p43.x * p21.x + p43.y * p21.y + p43.z * p21.z;
		d1321 = p13.x * p21.x + p13.y * p21.y + p13.z * p21.z;
		d4343 = p43.x * p43.x + p43.y * p43.y + p43.z * p43.z;
		d2121 = p21.x * p21.x + p21.y * p21.y + p21.z * p21.z;
		
		denom = d2121 * d4343 - d4321 * d4321;
		if (fabs(denom) < EPS) {
			std::cout << "intersection(): Denominator < " << EPS << "!" << std::endl;
			delete p1; delete p2; delete p3; delete p4;
			return 0;
		}
		numer = d1343 * d4321 - d1321 * d4343;
		
		mua = numer / denom;
		mub = (d1343 + d4321 * (mua)) / d4343;
		
		pa.x = p1->x + mua * p21.x;
		pa.y = p1->y + mua * p21.y;
		pa.z = p1->z + mua * p21.z;
		//std::cout << "      Point on p1->p2: (" << pa.str() << ")" << std::endl;
		pb.x = p3->x + mub * p43.x;
		pb.y = p3->y + mub * p43.y;
		pb.z = p3->z + mub * p43.z;
		//std::cout << "      Point on p3->p4: (" << pb.str() << ")" << std::endl;
		pa.x = (pa.x + pb.x) / 2.0;
		pa.y = (pa.y + pb.y) / 2.0;
		pa.z = (pa.z + pb.z) / 2.0;
		//std::cout << "             Averaged: (" << pa.str() << ")" << std::endl;
		delete p1; delete p2; delete p3; delete p4;
		return calcBiangle(&pa);
	}
	
	EgoPath Utils::getPath(Biangle const *b1, Biangle const *b2, double dist) {
		Point p;
		double d, A, B, steps, pct;
		double mstep, mcur;
		EgoPath path;
		
		d = hypot(*b1, *b2);
		if (d < dist) {
			path.pushBack(*b2);
		} else {
			steps = ceil(d / dist);
			pct = 1.0 / steps;
			mstep = (b2->mag - b1->mag) / steps;
			mcur = b1->mag + mstep;
			for (double next = pct; next<=1.0; next+=pct) {
				A = sin((1.0-next)*d) / sin(d);
				B = sin(next*d) / sin(d);
				p.x = (A * b1->cosph * b1->costh) + (B * b2->cosph * b2->costh);
				p.y = (A * b1->cosph * b1->sinth) + (B * b2->cosph * b2->sinth);
				p.z = (A * b1->sinph)             + (B * b2->sinph);
				Biangle b(atan2(p.y, p.x), atan2(p.z, hypot(p.x, p.y)), mcur);
				mcur += mstep;
				path.pushBack(b);
			}
		}
		return path;
	}
	
	EgoPath Utils::getPath(StampedBiangle const *b1,
	                       StampedBiangle const *b2, double dist) {
		double d;
		EgoPath path;
		
		d = hypot(*b1, *b2);
		if (d < dist) {
			path.pushBack(*b2);
		} else {
			Point p;
			double A, B, steps, pct, diff;
			double mstep, mcur;
			Time tdiff, tstep, tcur;
			
			steps = ceil(d / dist);
			pct = 1.0 / steps;
			// time difference and step amount
			diff = b2->t.toDouble() - b1->t.toDouble();
			tdiff = Time(diff);
			tcur = b2->t - b1->t;
			tstep = Time(diff / steps);
			tcur = b1->t;
			tcur += tstep;
			// mag difference and step amount
			diff = b2->mag - b1->mag;
			mstep = diff / steps;
			mcur = b1->mag + mstep;
			for (double next = pct; next<=1.0; next+=pct) {
				A = sin((1.0-next)*d) / sin(d);
				B = sin(next*d) / sin(d);
				p.x = (A * b1->cosph * b1->costh) + (B * b2->cosph * b2->costh);
				p.y = (A * b1->cosph * b1->sinth) + (B * b2->cosph * b2->sinth);
				p.z = (A * b1->sinph)             + (B * b2->sinph);
				StampedBiangle b(atan2(p.y, p.x), atan2(p.z, hypot(p.x, p.y)), mcur);
				b.t = tcur;
				tcur += tstep;
				mcur += mstep;
				path.pushBack(b);
			}
		}
		return path;
	}
	
	/*
	void Utils::pointToPointMsg(Point const *p, geometry_msgs::Point &msg) {
		msg.x = p->x; msg.y = p->y; msg.z = p->z;
	}
	
	void Utils::biangleToPointMsg(Biangle const *b, geometry_msgs::Point &msg) {
		Point* p = Utils::calcPoint(b);
		pointToPointMsg(p, msg); 
		delete p;
	}
	
	void Utils::biangleToQuaternionMsg(Biangle const *b, geometry_msgs::Quaternion &msg) {
		// TODO: need to confirm this returns the correct result!
		//msg = tf::createQuaternionMsgFromRollPitchYaw(0.0, (M_PI/2)-b->phi, b->theta);
		msg = tf::createQuaternionMsgFromRollPitchYaw(0.0,
		            (b->phi > 0 ? (M_PI/2) - b->phi : (M_PI/2) + b->phi),
		            b->theta);
	}
	
	void Utils::biangleToPoseMsg(Biangle const *b, geometry_msgs::Pose &msg) {
		biangleToPointMsg(b, msg.position);
		biangleToQuaternionMsg(b, msg.orientation);
	}
	*/
	
};
