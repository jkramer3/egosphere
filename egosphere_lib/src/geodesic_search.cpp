/*
 * Put copyright notice here
 */

#include <egosphere/geodesic_search.h>
#include <algorithm>

namespace egosphere {
	
	// ***************************************************************
	// *** Constants to make code a bit more readable
	// ***************************************************************
	static bool PARAM_MIN   = true;
	static bool PARAM_MAX   = false;
	static bool PARAM_PHI   = true;
	static bool PARAM_THETA = false;
	
	// ***************************************************************
	// *** Template functions -- meant ONLY to handle subclasses of
	// *** AngleInterval, and so purposely NOT in header file 
	// ***************************************************************
	/** Calculate and create a number of intervals, storing them in \c vec. */
	template<typename T>
	void makeIntervals(const double& min, const double& max, const double& span,
	                   const double& range, std::vector<T*>& vec) {
		int numBuckets;
		double bucket_lo, bucket_hi;
		
		numBuckets = (range / span) + 1;
		bucket_lo = min;
		bucket_hi = bucket_lo + span;
		for (int i=0; i<numBuckets; i++) {
			T *ti = new T(bucket_lo, bucket_hi);
			vec.push_back(ti);
			bucket_lo = bucket_hi; bucket_hi += span;
		}
	}
	
	/** Binary searches vector \c vec for an element that \a contains value
	 * \c val. Note that the elements of \c vec are \a pointers and that the
	 * type must have the function \c in(double) defined. */
	template<typename T>
	T* find(const double& val, const std::vector<T*>& vec) {
		int ix = find_ix(val, vec);
		if (ix < 0) return 0;
		return vec[ix];
	}
	
	/** Binary searches vector \c vec for the \a index of an element that
	 * \a contains value \c val. Note that the elements of \c vec are
	 * \a pointers and that the type must have the function \c in(double)
	 * defined. */
	template<typename T>
	int find_ix(const double& val, const std::vector<T*>& vec) {
		unsigned int lb=0, ub=vec.size(), ix=ub/2;
		int dir = 1;
		while (ub-lb > 0) {
			dir = vec[ix]->in(unsignZero(val));
			if (dir < 0) {
				ub = ix;
				ix = lb + ((ub-lb) / 2);
			} else if (dir > 0) {
				lb = ix;
				ix = ub - ((ub-lb) / 2);
			} else {
				break;
			}
		}
		if (dir != 0) {
			return -1;
		}
		return ix;
	}
	
	// ******************************************
	// *** Angle interval base class
	// ******************************************
	AngleInterval::AngleInterval() : lo(toRad(-181.0)), hi(toRad(181)) {
		//delta = hi - lo;
	}
	
	AngleInterval::AngleInterval(double l, double h) {
		lo=unsignZero(l);
		hi=unsignZero(h);
		//delta = hi - lo;
	}
	
	int AngleInterval::in(double val) const {
		return (val < lo ? -1 : (val > hi ? 1 : 0));
	}

	std::string AngleInterval::str() const {
		return str(2);
	}
	
	std::string AngleInterval::str(int dec) const {
		std::ostringstream oss;
		if (dec >= 0) oss << std::fixed << std::setprecision(dec);
		oss << toDeg(lo) << ", " << toDeg(hi);
		return oss.str();
	}
	
	// ******************************************
	// *** Phi (altitude) angle interval class
	// ******************************************
	PhiInter::PhiInter() : AngleInterval(toRad(-91.0), toRad(91.0)) { }
	
	PhiInter::PhiInter(double l, double h) : AngleInterval(l, h) { }

	PhiInter::PhiInter(double l, double h, GeoVertex *v) :
			AngleInterval(l, h) {
		verts.push_back(v);
	}
	
	PhiInter::~PhiInter() { }
	
	int PhiInter::numVertex() const {
		return verts.size();
	}
	
	void PhiInter::addVertex(GeoVertex const *ev) {
		std::vector<const GeoVertex*>::const_iterator it;
		bool found = false;
		for (it=verts.begin(); it<verts.end(); it++) {
			if ((*it) == ev) found = true;
		}
		if (!found) {
			verts.push_back(ev);
		}
	}
	
	GeoVertex const * PhiInter::nearest(Biangle& biang) const {
		std::vector<const GeoVertex*>::const_iterator it;
		GeoVertex const *ev = 0;
		double mind, tmpmin;
		mind = tmpmin = M_PI;
		for (it=verts.begin(); it<verts.end(); it++) {
			tmpmin = hypot(biang, *((*it)->getBiangle()));
			if (tmpmin < mind) {
				mind = tmpmin;
				ev = (*it);
			}
		}
		return ev;
	}
	
	GeoVertex const * PhiInter::nearest(Point& p) const {
		std::vector<const GeoVertex*>::const_iterator it;
		GeoVertex const *ev = 0;
		double mind, tmpmin;
		mind = tmpmin = M_PI;
		for (it=verts.begin(); it<verts.end(); it++) {
			tmpmin = distance_sq(p, *((*it)->getPoint()));
			if (tmpmin < mind) {
				mind = tmpmin;
				ev = (*it);
			}
		}
		return ev;
	}
	
	void PhiInter::map2EgoSearch(std::map<const GeoVertex*, int>& gv2ev,
	                             std::map<Biangle,int>& vec) {
		std::vector<const GeoVertex*>::const_iterator it;
		vec.clear();
		if (verts.size() > 0) {
			for (it=verts.begin(); it<verts.end(); it++) {
				Biangle const *b = (*it)->getBiangle();
				Biangle gv_b(b->theta, b->phi, false);
				std::pair<Biangle,int> ele(gv_b, gv2ev[(*it)]);
				vec.insert(ele);
			}
		} else {
			std::cout << "    NO VERTICES IN VECTOR!" << std::endl;
		}
	}
	
	std::string PhiInter::str() const {
		return str(2);
	}
	
	std::string PhiInter::str(int dec) const {
		std::ostringstream oss;
		std::vector<const GeoVertex*>::const_iterator it;
		oss << "(" << AngleInterval::str(dec) << ") \t->" << std::endl;
		if (verts.size() > 0) {
			for (it=verts.begin(); it<verts.end(); it++) {
				oss << "\t\t" << (*it) << ":(" << (*it)->ang_str(dec) << ")" << std::endl;
			}
		} else {
			oss << "\t\t<No GeoVertex>" << std::endl;
		}
		return oss.str();
	}
	
	struct comparePhiInterval {
		bool operator ()(PhiInter *lhs, PhiInter *rhs) {
			return lhs->hi <= rhs->lo;
		}
	};
	
	// ******************************************
	// *** Theta (azimuth) angle interval class
	// ******************************************
	ThetaInter::ThetaInter() : AngleInterval(), sorted(false) { }
	
	ThetaInter::ThetaInter(double l, double h) :	AngleInterval(l, h), sorted(false) {
	}
	
	ThetaInter::~ThetaInter() {
		std::vector<PhiInter*>::iterator it;
		for (it=phiInterVec.begin(); it<phiInterVec.end(); it++) {
			delete (*it);
		}
	}
	
	void ThetaInter::makePhiIntervals(double& minPhi, double& maxPhi, double& span) {
		makeIntervals(minPhi, maxPhi, span, M_PI, phiInterVec);
		sortPhi();
	}
	
	unsigned int ThetaInter::phiSize() const {
		return phiInterVec.size();
	}
	
	void ThetaInter::phiVertexStats(int (&arr)[3]) {
		std::vector<PhiInter*>::const_iterator it;
		int vcount=0, vsum=0, minVPP=9999, maxVPP=0;
		for (it=phiInterVec.begin(); it<phiInterVec.end(); it++) {
			vcount = (*it)->numVertex();
			vsum  += vcount;
			minVPP = std::min(minVPP, vcount);
			maxVPP = std::max(maxVPP, vcount);
		}
		arr[0] = minVPP; arr[1] = maxVPP; arr[2] = vsum;
	}
	
	void ThetaInter::sortPhi() {
		if (phiInterVec.size() > 1) {
			std::sort(phiInterVec.begin(), phiInterVec.end(), comparePhiInterval());
		}
		sorted = true;
	}
	
	int ThetaInter::numVertex() const {
		int count = 0;
		std::vector<PhiInter*>::const_iterator it;
		for (it=phiInterVec.begin(); it<phiInterVec.end(); it++) {
			count += (*it)->numVertex();
		}
		return count;
	}
	
	void ThetaInter::addVertex(double& minPhi, double& maxPhi, const GeoVertex* vert) {
		int phix = find_ix<PhiInter>(minPhi, phiInterVec);
		// phix should NEVER return 0!
		if (phix >= 0) {
			while (phix < (int)phiInterVec.size() && phiInterVec[phix]->lo < maxPhi) {
				phiInterVec[phix++]->addVertex(vert);
			}
		} else {
			std::cout << "Putting ev " << vert << " (" << vert->ang_str() << "): ";
			std::cout << " PhiInter for " << minPhi << " missing!" << std::endl;
		}
	}
	
	GeoVertex const * ThetaInter::findVertex(Biangle& b) const {
		if (!sorted) return 0;
		double angth, angph;
		
		angth = norm_ang(b.theta);
		angph = norm_ang(b.phi);
		if (in(angth) != 0) return 0;
		PhiInter *pi = find<PhiInter>(angph, phiInterVec);
		if (pi == 0) {
			std::cout << "PHI INTERVAL FOR (" << b.str() << ") NOT FOUND" << std::endl;
			return 0;
		}
		return pi->nearest(b);
	}
	
	GeoVertex const * ThetaInter::findVertex(Point& p, Biangle& b) const {
		if (!sorted) return 0;
		double angth, angph;
		
		angth = norm_ang(b.theta);
		angph = norm_ang(b.phi);
		if (in(angth) != 0) return 0;
		PhiInter *pi = find<PhiInter>(angph, phiInterVec);
		if (pi == 0) {
			std::cout << "PHI INTERVAL FOR (" << b.str() << ") NOT FOUND" << std::endl;
			return 0;
		}
		return pi->nearest(p);
	}
	
	void ThetaInter::map2EgoSearch(
	                    std::map<const GeoVertex*, int>& gv2ev,
	                    std::vector< std::map<Biangle,int> >& vec2) {
		std::vector<PhiInter*>::const_iterator it;
		vec2.clear();
		vec2.reserve(phiInterVec.size());
		for (it=phiInterVec.begin(); it<phiInterVec.end(); it++) {
			std::map<Biangle,int> vec;
			(*it)->map2EgoSearch(gv2ev, vec);
			vec2.push_back(vec);
		}
	}
	
	std::string ThetaInter::str() const {
		return str(2);
	}
	
	std::string ThetaInter::str(int dec) const {
		std::ostringstream oss;
		std::vector<PhiInter*>::const_iterator it;
		oss << "Theta interval: (" << AngleInterval::str(dec) << ") ";
		oss << "with phi intervals:" << std::endl;
		if (phiInterVec.size() > 0) {
			for (it=phiInterVec.begin(); it<phiInterVec.end(); it++) {
				oss << "\t" << (*it)->str(dec);
			}
		} else {
			oss << "\t<No PhiInter> ";
		}
		return oss.str();
	}
	
	// ******************************************
	// *** Geosphere point-location structure
	// ******************************************
	GeoSearch::GeoSearch(GeosphereMetainfo& mi, std::vector<GeoVertex*>& verts) {
		//std::cout << "Constructing GeoSearch object" << std::endl;
		double dist, buf;
		dist = mi.maxVertVoroD;
		//dist = mi.minVertVoroD;
		buf = mi.minVertVoroD / 2.0;
		initSearchVecs(mi, interThetaVec);
		fillSearchVecs(verts, dist, buf, interThetaVec);
	}
	
	GeoSearch::~GeoSearch() {
		std::vector<ThetaInter*>::iterator it;
		for (it=interThetaVec.begin(); it<interThetaVec.end(); it++) {
			delete (*it);
		}
	}
	
	GeoVertex const * GeoSearch::nearest(Biangle& b) const {
		ThetaInter *thinter;
		thinter = find<ThetaInter>(norm_ang(b.theta), interThetaVec);
		if (thinter == 0) {
			std::cout << "THETA INTERVAL FOR (" << b.str() << ") NOT FOUND" << std::endl;
			return 0;
		}
		return thinter->findVertex(b);
	}
	
	GeoVertex const * GeoSearch::nearest(Point& p) const {
		Biangle *b = Utils::calcBiangle(&p);
		ThetaInter *thinter;
		thinter = find<ThetaInter>(norm_ang(b->theta), interThetaVec);
		if (thinter == 0) {
			std::cout << "THETA INTERVAL FOR (" << b->str() << ") NOT FOUND" << std::endl;
			return 0;
		}
		return thinter->findVertex(p, *b);
	}
	
	void GeoSearch::map2EgoSearch(std::map<const GeoVertex*, int>& gv2ev,
	                              std::vector< std::vector< std::map<Biangle,int> > >& vec3) {
		std::vector<ThetaInter*>::const_iterator it;
		vec3.clear();
		vec3.reserve(interThetaVec.size());
		for (it=interThetaVec.begin(); it<interThetaVec.end(); it++) {
			std::vector< std::map<Biangle,int> > vec2;
			(*it)->map2EgoSearch(gv2ev, vec2);
			vec3.push_back(vec2);
		}
	}
	
	std::string GeoSearch::str() const {
		return str(2);
	}
	
	std::string GeoSearch::str(int dec) const {
		return sum_str(true, dec);
	}
	
	std::string GeoSearch::sum_str() const {
		return sum_str(false, 2);
	}
	
	std::string GeoSearch::sum_str(bool incInter, int dec) const {
		std::ostringstream oss;
		std::vector<ThetaInter*>::const_iterator it;
		int stats[3];
		if (interThetaVec.size() > 0) {
			int minVPP=9999, maxVPP=0;
			float avgVPP=0.0;
			int phicount=0, phisum=0, vsum=0, minPPT=9999, maxPPT=0;
			for (it=interThetaVec.begin(); it<interThetaVec.end(); it++) {
				phicount = (*it)->phiSize();
				(*it)->phiVertexStats(stats);
				minVPP   = std::min(minVPP, stats[0]);
				maxVPP   = std::max(maxVPP, stats[1]);
				avgVPP   += (float)(stats[2] / phicount);
				phisum   += phicount;
				minPPT   = std::min(minPPT, phicount);
				maxPPT   = std::max(maxPPT, phicount);
				vsum     += (*it)->numVertex();
				if (incInter) oss << (*it)->str(dec);
			}
			oss << "Search: total of " << vsum << " vertex refs stored in ";
			oss << interThetaVec.size() << " theta buckets and ";
			oss << phisum << " phi buckets" << std::endl;
			oss << "  Phi buckets per theta: min=" << minPPT << ", max=";
			oss << maxPPT << ", avg=" << (phisum / interThetaVec.size()) << std::endl;
			oss << "  Vertices per phi bucket: min=" << minVPP << ", max=";
			oss << maxVPP << ", avg=" << (avgVPP / interThetaVec.size()) << std::endl;
		} else {
			oss << "<No ThetaInter> ";
		}
		return oss.str();
	}
	
	void GeoSearch::initSearchVecs(GeosphereMetainfo& mi,
	                               std::vector<ThetaInter*>& thvec) {
		//std::cout << "  In initSearchVecs()" << std::endl;
		std::vector<ThetaInter*>::const_iterator it;
		
		makeIntervals(mi.minTheta, mi.maxTheta, mi.maxVertD, (2*M_PI), thvec);
		for (it=thvec.begin(); it<thvec.end(); it++) {
			(*it)->makePhiIntervals(mi.minPhi, mi.maxPhi, mi.maxVertD);
		}
		//std::cout << "  Exiting initSearchVecs()" << std::endl;
	}
	
	void GeoSearch::fillSearchVecs(std::vector<GeoVertex*>& verts,
	                               double& dist, double& buf,
	                               std::vector<ThetaInter*>& thvec) {
		std::vector<GeoVertex*>::iterator it;
		Biangle b;
		int thix;
		double bounds[4];
		for (it=verts.begin(); it<verts.end(); it++) {
			b = *((*it)->getBiangle());
			getBounds(*it, dist, buf, bounds);
			if (!(*it)->getVoroWrap()) {
				thix = find_ix<ThetaInter>(bounds[0], thvec);
				if (thix >= 0) {
					while (thix < (int)thvec.size() && thvec[thix]->lo < bounds[1]) {
						thvec[thix++]->addVertex(bounds[2], bounds[3], *it);
					}
				} else {
					std::cout << "      GeoVertex (" << (*it)->ang_str() << "): ";
					std::cout << "theta interval not found!" << std::endl;
				}
			} else {
				thix = find_ix<ThetaInter>(-M_PI, thvec);
				if (thix >= 0) {
					while (thix < (int)thvec.size() && thvec[thix]->lo < bounds[1]) {
						thvec[thix++]->addVertex(bounds[2], bounds[3], *it);
					}
				} else {
					std::cout << "      GeoVertex (" << (*it)->ang_str() << "): ";
					std::cout << "theta interval not found!" << std::endl;
				}
				thix = find_ix<ThetaInter>(bounds[0], thvec);
				if (thix >= 0) {
					while (thix < (int)thvec.size() && thvec[thix]->lo < M_PI) {
						thvec[thix++]->addVertex(bounds[2], bounds[3], *it);
					}
				} else {
					std::cout << "      GeoVertex (" << (*it)->ang_str() << "): ";
					std::cout << "theta interval not found!" << std::endl;
				}
			}
		}
	}
	
	void GeoSearch::getBounds(GeoVertex *ev, double& d, double& buf, double (&arr)[4]) {
		Biangle const *b = ev->getBiangle();
		if ((b->phi + d) >= (M_PI/2.0)) {
			// north pole
			arr[0] = -M_PI;
			arr[1] = M_PI;
			arr[2] = (M_PI/2.0) - d;
			arr[3] = (M_PI/2.0);
		} else if ((b->phi - d) <= (-M_PI/2.0)) {
			// south pole
			arr[0] = -M_PI;
			arr[1] = M_PI;
			arr[2] = (-M_PI/2.0);
			arr[3] = (-M_PI/2.0) + d;
		} else {
			if (ev->getVoroWrap()) {
				arr[0] = ev->getVoroAng(PARAM_MAX, PARAM_THETA) - buf;
				arr[1] = ev->getVoroAng(PARAM_MIN, PARAM_THETA) + buf;
			} else {
				arr[0] = ev->getVoroAng(PARAM_MIN, PARAM_THETA) - buf;
				arr[1] = ev->getVoroAng(PARAM_MAX, PARAM_THETA) + buf;
			}
			arr[2] = ev->getVoroAng(PARAM_MIN, PARAM_PHI) - buf;
			arr[3] = ev->getVoroAng(PARAM_MAX, PARAM_PHI) + buf;
		}
	}
		
};

