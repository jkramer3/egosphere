/*
 * Put copyright notice here
 */
#include <egosphere/rviz_vertex.hpp>
#include <egosphere/egosphere_ros_utils.hpp>
#include <iostream>
#include <iomanip>
#include <sstream>

namespace egosphere {
	
	RVizVertex::RVizVertex() : EgoVertex() { }
	
	RVizVertex::RVizVertex(Biangle& b) : EgoVertex(b) { }
	
	RVizVertex::RVizVertex(Point& p) : EgoVertex(p) { }
	
	RVizVertex::~RVizVertex() { }
	
	void RVizVertex::stuffMarker(visualization_msgs::Marker &m, double radius,
	                             marker_type::MarkerType t) {
		switch (t) {
		case marker_type::SPHERE:
			stuffSphere(m, radius);
			break;
		case marker_type::FACE:
		default:
			stuffFace(m, radius);
			break;
		}
		m.ns = "RVizVertex";
		m.action = visualization_msgs::Marker::MODIFY;
		m.pose.orientation.w = 1.0;
		m.color.r = 0;
		m.color.g = 1;
		m.color.b = 1;
		m.color.a = 1;
	}
	
	void RVizVertex::stuffFace(visualization_msgs::Marker &m, double radius) {
		m.type = visualization_msgs::Marker::TRIANGLE_LIST;
		m.points.clear();
		Point p1, p2, me;
		geometry_msgs::Point rp1, rp2, rme;
		getPoint(me, radius);
		RosUtils::pointToPointMsg(me, rme);
		neighbors[0]->getPoint(p1, radius);
		RosUtils::pointToPointMsg(p1, rp1);
		// note that order of points matters to light reflection
		for (unsigned int i=1; i<neighbors.size(); ++i) {
			neighbors[i]->getPoint(p2, radius);
			RosUtils::pointToPointMsg(p2, rp2);
			m.points.push_back(rme);
			m.points.push_back(rp1);
			m.points.push_back(rp2);
			rp1 = rp2;
		}
		RosUtils::pointToPointMsg(p1, rp2);
		m.points.push_back(rme);
		m.points.push_back(rp1);
		m.points.push_back(rp2);
		RosUtils::biangleToPointMsg(&biangle, m.pose.position, radius);
		m.scale.x = 1.0;
		m.scale.y = 1.0;
		m.scale.z = 1.0;
	}
	
	void RVizVertex::stuffSphere(visualization_msgs::Marker &m, double radius) {
		m.type = visualization_msgs::Marker::SPHERE_LIST;
		Point me;
		geometry_msgs::Point rme;
		getPoint(me, radius);
		RosUtils::pointToPointMsg(me-me, rme);
		m.points.push_back(rme);
		RosUtils::biangleToPointMsg(&biangle, m.pose.position, radius);
		m.scale.x = 0.04 * radius;
		m.scale.y = 0.04 * radius;
		m.scale.z = 0.04 * radius;
	}
	
	std::string RVizVertex::str() const {
		return EgoVertex::str();
	}
	
};

