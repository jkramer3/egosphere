/*
 * Put copyright notice here
 */
#include <egosphere/egosphere_path.hpp>
//#include <egosphere/egosphere_utils.h>
//#include <egosphere/egosphere_ros_utils.hpp>
#include <egosphere/egosphere.hpp>
//#include <egosphere/ego_path.hpp>
#include <algorithm>

namespace egosphere {
	
	EgospherePath::EgospherePath() { }
	
	/*
	EgospherePath::EgospherePath(const Egosphere &egosphere,
	                             const std::list<StampedBiangle> &steps) {
		std::list<StampedBiangle>::const_iterator it;
		for (it=steps.begin(); it!=steps.end(); it++) {
			m_path.push_back(egosphere.nearest(*it));
		}
	}
	
	EgospherePath::EgospherePath(const Egosphere &egosphere,
	                             const StampedBiangleMsgArray &msg) {
		for (int i=0; i<msg.biangles.size(); i++) {
			StampedBiangle sb;
			RosUtils::stampedBiangleMsgToStampedBiangle(msg.biangles[i], sb);
			m_path.push_back(egosphere.nearest(sb));
		}
	}
	
	EgospherePath::EgospherePath(const Egosphere &egosphere,
	                             const EgoPath &ep) {
		std::vector<StampedBiangle> sb_v;
		ep.toVector(sb_v);
		for (int i=0; i<sb_v.size(); i++) {
			m_path.push_back(egosphere.nearest(sb_v[i]));
		}
	}
	*/
	
	std::list<unsigned int>::iterator EgospherePath::begin() {
		return m_path.begin();
	}
	
	std::list<unsigned int>::const_iterator EgospherePath::begin() const {
		return m_path.begin();
	}
	
	std::list<unsigned int>::iterator EgospherePath::end() {
		return m_path.end();
	}
	
	std::list<unsigned int>::const_iterator EgospherePath::end() const {
		return m_path.end();
	}
	
	bool EgospherePath::empty() const {
		return m_path.empty();
	}
	
	unsigned int EgospherePath::size() const {
		return m_path.size();
	}
	
	unsigned int EgospherePath::peekFront() const {
		return m_path.front();
	}
	
	unsigned int EgospherePath::peekBack() const {
		return m_path.back();
	}
	
	void EgospherePath::clear() {
		m_path.clear();
	}
	
	unsigned int EgospherePath::popFront() {
		unsigned int vid(m_path.front());
		m_path.pop_front();
		return vid;
	}
	
	int EgospherePath::popFront(int n) {
		// TODO: perhaps return a list of removed values?
		if (n <= 0) return 0;
		int pops = std::min(n, (int)m_path.size());
		std::list<unsigned int>::iterator it = m_path.begin();
		std::advance(it, pops);
		m_path.erase(m_path.begin(), it);
		return pops;
	}
	
	unsigned int EgospherePath::popBack() {
		unsigned int vid(m_path.back());
		m_path.pop_back();
		return vid;
	}
	
	int EgospherePath::popBack(int n) {
		// TODO: perhaps return a list of removed values?
		if (n <= 0) return 0;
		int pops = std::min(n, (int)m_path.size());
		std::list<unsigned int>::iterator it = m_path.end();
		std::advance(it, -pops);
		m_path.erase(it, m_path.end());
		return pops;
	}
	
	void EgospherePath::pushFront(unsigned int vid) {
		this->m_path.insert(m_path.begin(), vid);
	}
	
	void EgospherePath::pushFront(const EgospherePath &ep) {
		std::vector<unsigned int> ep_v;
		ep.toVector(ep_v);
		for (int i=ep_v.size()-1; i>=0; i--) {
			m_path.push_front(ep_v[i]);
		}
	}
	
	/*
	void EgospherePath::pushFront(const Egosphere &e, const StampedBiangle &b) {
		m_path.push_front(e.nearest(b));
	}
	
	void EgospherePath::pushFront(const Egosphere &e, const std::list<StampedBiangle> &l) {
		this->m_path.insert(m_path.begin(), l.begin(), l.end());
	}
	
	void EgospherePath::pushFront(const Egosphere &e, const EgoPath &ep) {
		std::vector<StampedBiangle> sb_v;
		ep.toVector(sb_v);
		for (int i=sb_v.size()-1; i>=0; i--) {
			m_path.push_front(e.nearest(sb_v[i]));
		}
	}
	*/
	
	void EgospherePath::pushBack(unsigned int vid) {
		this->m_path.insert(m_path.end(), vid);
	}
	
	void EgospherePath::pushBack(const EgospherePath &ep) {
		std::vector<unsigned int> ep_v;
		ep.toVector(ep_v);
		for (unsigned int i=0; i<ep_v.size(); i++) {
			this->m_path.insert(m_path.end(), ep_v[i]);
		}
	}
	
	/*
	void EgospherePath::pushBack(const Egosphere &e, const StampedBiangle &b) {
		this->m_path.insert(m_path.end(), e.nearest(b));
	}
	
	void EgospherePath::pushBack(const Egosphere &e, const std::list<StampedBiangle> &l) {
		this->m_path.insert(m_path.end(), l.begin(), l.end());
	}
	
	void EgospherePath::pushBack(const Egosphere &e, const EgoPath &ep) {
		std::vector<StampedBiangle> sb_v;
		ep.toVector(sb_v);
		for (int i=0; i<sb_v.size(); i++) {
			m_path.push_back(e.nearest(sb_v[i]));
		}
	}
	*/
	
	/*
	Time EgospherePath::timespan() {
		Time t(m_path.back().t);
		t -= m_path.front().t;
		return t;
	}
	*/
	
	void EgospherePath::toVector(std::vector<unsigned int> &v) const {
		v.clear();
		v.reserve(m_path.size());
		std::list<unsigned int>::const_iterator it;
		for (it=m_path.begin(); it!=m_path.end(); it++) {
			v.push_back(*it);
		}
	}
	
	/*
	void EgospherePath::toVector(const Egosphere &e, std::vector<StampedBiangle> &v) const {
		v.clear();
		v.reserve(m_path.size());
		std::list<unsigned int>::const_iterator it;
		for (it=m_path.begin(); it!=m_path.end(); it++) {
			StampedBiangle sb;
			if (e.getBiangleOf(*it, sb)) {
				v.push_back(sb);
			}
		}
	}
	
	void EgospherePath::toEgoPath(const Egosphere &e, EgoPath &ep) const {
		ep.clear();
		std::list<unsigned int>::const_iterator it;
		for (it=m_path.begin(); it!=m_path.end(); it++) {
			StampedBiangle sb;
			if (e.getBiangleOf(*it, sb)) {
				ep.pushBack(sb);
			}
		}
	}
	*/
	
};

