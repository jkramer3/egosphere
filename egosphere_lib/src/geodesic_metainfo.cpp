/*
 * Put copyright notice here
 */

#include <egosphere/egosphere_utils.h>
#include <egosphere/geodesic_metainfo.h>
#include <iostream>
#include <iomanip>
#include <sstream>

namespace egosphere {

	// **********************************************************
	// **** GeosphereMetainfo public functions
	// **********************************************************
	GeosphereMetainfo::GeosphereMetainfo() :
			  freq(0)
			, numVerts(0)
			, numVoros(0)
			, minTheta(-M_PI)
			, maxTheta(M_PI)
			, minPhi(-M_PI/2.0)
			, maxPhi(M_PI/2.0)
			, minVertD(M_PI) // unit sphere; calc'd min always < PI
			, maxVertD(0.0)
			, minVoroD(M_PI) // unit sphere; calc'd min always < PI
			, maxVoroD(0.0)
			, minVertVoroD(M_PI) // unit sphere; calc'd min always < PI
			, maxVertVoroD(0.0)
	{ }

	std::string GeosphereMetainfo::str() const {
		std::ostringstream oss;
		oss << std::setprecision(6);
		oss << "Geosphere frequency=" << freq << "; ";
		oss << numVerts << " vertices" << std::endl;
		oss << "Theta (min, max):\t(" << minTheta << ", " << maxTheta << ")\t";
		oss << "(" << toDeg(minTheta) << ", " << toDeg(maxTheta) << ") degs" << std::endl;
		oss << "Phi   (min, max):\t(" << minPhi << ", " << maxPhi << ")\t";
		oss << "(" << toDeg(minPhi) << ", " << toDeg(maxPhi) << ") degs" << std::endl;
		oss << "Minimum (angular) distance between vertices: ";
		oss << minVertD << " (" << toDeg(minVertD) << " degs)" << std::endl;
		oss << "Maximum (angular) distance between vertices: ";
		oss << maxVertD << " (" << toDeg(maxVertD) << " degs)" << std::endl;
		oss << "Geosphere has " << numVoros << " voronoi points" << std::endl;
		//oss << "Minimum (angular) distance between voronoi vertices: ";
		//oss << "(not calculated)" << std::endl;
		//oss << "Maximum (angular) distance between voronoi vertices: ";
		//oss << "(not calculated)" << std::endl;
		oss << "Minimum (angular) distance from voronoi to vertex: ";
		oss << minVertVoroD << " (" << toDeg(minVertVoroD) << " degs)" << std::endl;
		oss << "Minimum (euclidean) distance between vertex separation:" << std::endl;
		oss << "      @1mm, 0.001m from center: " << Utils::distance(minVertVoroD, 0.001) << std::endl;
		oss << "      @5mm, 0.005m from center: " << Utils::distance(minVertVoroD, 0.005) << std::endl;
		oss << "       @1cm, 0.01m from center: " << Utils::distance(minVertVoroD, 0.01) << std::endl;
		oss << "       @5cm, 0.05m from center: " << Utils::distance(minVertVoroD, 0.05) << std::endl;
		oss << "       @10cm, 0.1m from center: " << Utils::distance(minVertVoroD, 0.1) << std::endl;
		oss << "       @50cm, 0.5m from center: " << Utils::distance(minVertVoroD, 0.5) << std::endl;
		oss << "             @1.0m from center: " << Utils::distance(minVertVoroD, 1.0) << std::endl;
		oss << "             @2.0m from center: " << Utils::distance(minVertVoroD, 2.0) << std::endl;
		oss << "             @5.0m from center: " << Utils::distance(minVertVoroD, 5.0) << std::endl;
		oss << "Maximum (angular) distance from voronoi to vertex: ";
		oss << maxVertVoroD << " (" << toDeg(maxVertVoroD) << " degs)" << std::endl;
		oss << "Maximum (euclidean) distance between vertex separation:" << std::endl;
		oss << "      @1mm, 0.001m from center: " << Utils::distance(maxVertVoroD, 0.001) << std::endl;
		oss << "      @5mm, 0.005m from center: " << Utils::distance(maxVertVoroD, 0.005) << std::endl;
		oss << "       @1cm, 0.01m from center: " << Utils::distance(maxVertVoroD, 0.01) << std::endl;
		oss << "       @5cm, 0.05m from center: " << Utils::distance(maxVertVoroD, 0.05) << std::endl;
		oss << "       @10cm, 0.1m from center: " << Utils::distance(maxVertVoroD, 0.1) << std::endl;
		oss << "       @50cm, 0.5m from center: " << Utils::distance(maxVertVoroD, 0.5) << std::endl;
		oss << "             @1.0m from center: " << Utils::distance(maxVertVoroD, 1.0) << std::endl;
		oss << "             @2.0m from center: " << Utils::distance(maxVertVoroD, 2.0) << std::endl;
		oss << "             @5.0m from center: " << Utils::distance(maxVertVoroD, 5.0) << std::endl;
		return oss.str();
	}
	
};
