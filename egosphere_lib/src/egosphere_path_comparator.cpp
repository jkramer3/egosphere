/*
 * Put copyright notice here
 */
#include <egosphere/egosphere_utils.h>
#include <egosphere/egosphere.hpp>
#include <egosphere/egosphere_path.hpp>

namespace egosphere {
	
	namespace path_compare_types {
		enum Type { NONE, SIMPLE }
	}
	
	static double EgospherePathComparator::compare(const Egosphere &es,
	                      const EgospherePath &p1, const EgospherePath &p2) {
		if (m_maxAngle != other.m_maxAngle) {
			throw std::invalid_argument("Paths must have the same maxAngle");
		}
		double prob;
		EgoVertex *m_1 = NULL, *m_2 = NULL;
		EgoVertex *o_1 = NULL, *o_2 = NULL;
		std::list<EgoVertex*>::const_iterator my_it, ot_it;
		m_it = m_path.begin(); o_it = other.m_path.begin();
		m_1 = (*m_it); o_1 = (*o_it);
		prob = compareBegin(m_1, o_1);
		for (; m_it!=m_path.end() && o_it!=other.m_path.end(); m_it++, o_it++) {
			m_2 = (*m_it); o_2 = (*o_it);
			prob *= compareStep(m_1, m_2, o_1, o_2);
		}
		return prob;
	}
	
	double EgospherePath::compareBegin(EgoVertex* me, EgoVertex* oth) {
		double dist, prob = 0.0;
		Biangle m_b, o_b;
		me->getBiangle(m_b); oth->getBiangle(o_b);
		dist = hypot(m_b, o_b);
		// making up 'prob' numbers at this point...
		if (dist < m_maxAngle) {
			prob = 1.0;
		} else if (dist < (2*m_maxAngle)) {
			prob = 0.99;
		} else if (dist < (3*m_maxAngle)) {
			prob = 0.95;
		} else if (dist < (4*m_maxAngle)) {
			prob = 0.8;
		} else if (dist < (5*m_maxAngle)) {
			prob = 0.5;
		}
		return prob;
	}
	
	double EgospherePath::compareStep(EgoVertex* m_1, EgoVertex* m_2,
	                                  EgoVertex* o_1, EgoVertex* o_2) {
		// P(A and B) = P(A) * P(B|A)?
		// TODO: account for depth shift
		double m_bear, o_bear;
		m_bear = m_1->neighborBearing(m_2);
		// get closest bearing to m_bear from other vertex?
		o_bear = o_1->neighborBearing(o_2);
		diff = fabs(angleBetween(m_bear, o_bear));
		//if (diff < 
	}
	*/
	
};

