/*
 * Put copyright notice here
 */
#include <egosphere/ego_vertex.h>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <sstream>

namespace egosphere {
	
	EgoVertex::EgoVertex() { }
	
	EgoVertex::EgoVertex(double t, double p) {
		biangle.theta = t;
		biangle.phi = p;
		Utils::calcPoint(biangle, point);
	}
	
	EgoVertex::EgoVertex(Biangle& b) {
		biangle = b;
		Utils::calcPoint(biangle, point);
	}
	
	EgoVertex::EgoVertex(Point& p) {
		point = p;
		Utils::calcBiangle(p, biangle);
	}
	
	EgoVertex::EgoVertex(const EgoVertex& other) :
			biangle(other.biangle),
			point(other.point)
	{
		// biangle & point inited to other; deep copy neighbors
		neighbors.assign(other.neighbors.begin(), other.neighbors.end());
		neighbor_bearing.assign(other.neighbor_bearing.begin(),
		                        other.neighbor_bearing.end());
	}
	
	EgoVertex::~EgoVertex() { }
	
	// note: friend method, not EgoVertex::swap
	void swap(EgoVertex& first, EgoVertex& second) {
		using std::swap;
		swap(first.biangle, second.biangle);
		swap(first.point, second.point);
		swap(first.neighbors, second.neighbors);
		swap(first.neighbor_bearing, second.neighbor_bearing);
	}
	
	EgoVertex& EgoVertex::operator=(EgoVertex other) {
		// by-value param allows immediate swap
		swap(*this, other);
		return *this;
	}
	
	unsigned int EgoVertex::getId() const {
		return vix;
	}
	
	Biangle* EgoVertex::getBiangle() const {
		return new Biangle(biangle);
	}
	
	void EgoVertex::getBiangle(Biangle& b) const {
		b = biangle;
	}
	
	Point* EgoVertex::getPoint(double radius) const {
		if (radius <= 0.0) return NULL;
		return (radius == 1.0 ? new Point(point)
		                      : Utils::calcPoint(&biangle, radius));
	}
	
	void EgoVertex::getPoint(Point& p, double radius) const {
		if (radius <= 0.0) {
			p.x = NAN; p.y = NAN; p.z = NAN;
		} else if (radius == 1.0) {
			p = point;
		} else {
			Utils::calcPoint(biangle, p, radius);
		}
	}
	
	int EgoVertex::numNeighbors() const {
		return (int)neighbors.size();
	}
	
	bool EgoVertex::isNeighbor(unsigned int vid) const {
		bool retval = false;
		for (unsigned int i=0; i<neighbors.size(); i++) {
			if (vid == neighbors.at(i)->getId()) {
				retval = true;
				break;
			}
		}
		return retval;
	}
	
	int EgoVertex::isNeighbor(EgoVertex* ev) const {
		int retval = -1;
		for (unsigned int i=0; i<neighbors.size(); i++) {
			if (ev == neighbors.at(i)) {
				retval = i;
				break;
			}
		}
		return retval;
	}
	
	EgoVertex* EgoVertex::getNeighborByIndex(int nix) const {
		if (nix < 0 || nix >= (int)neighbors.size()) {
			return NULL;
		}
		return neighbors[nix];
	}
	
	EgoVertex* EgoVertex::getNeighborById(unsigned int nid) const {
		EgoVertex* retval = NULL;
		for (unsigned int i=0; i<neighbors.size(); i++) {
			if ((int)neighbors.at(i)->getId() == nid) {
				retval = neighbors.at(i);
				break;
			}
		}
		return retval;
	}
	
	int EgoVertex::getNeighborNumber(unsigned int nid) const {
		int retval = -1;
		for (unsigned int i=0; i<neighbors.size(); i++) {
			if (neighbors.at(i)->getId() == nid) {
				retval = i;
				break;
			}
		}
		return retval;
	}
	
	unsigned int EgoVertex::getNeighborId(int nix) const {
		if (nix < 0 || nix >= (int)neighbors.size()) {
			throw std::out_of_range("invalid neighbor number");
		}
		return neighbors.at(nix)->getId();
	}
	
	void EgoVertex::neighborPoints(std::vector<Point> &nvec) const {
		nvec.clear();
		for (unsigned int i=0; i<neighbors.size(); i++) {
			Point p;
			neighbors.at(i)->getPoint(p);
			nvec.push_back(p);
		}
	}
	
	double EgoVertex::neighborBearing(EgoVertex* ev) const {
		double d = NAN;
		int nix;
		if ((nix = isNeighbor(ev)) >= 0) {
			if (nix < (int)neighbor_bearing.size()) {
				d = neighbor_bearing[nix];
			}
		}
		return d;
	}
	
	double EgoVertex::neighborBearing(unsigned int nid) const {
		int nix = getNeighborNumber(nid);
		return neighborBearing(nix);
	}
	
	double EgoVertex::neighborBearing(int nix) const {
		double d = NAN;
		if (nix >=0 && (unsigned int)nix < neighbors.size()) {
			d = neighbor_bearing[nix];
		}
		return d;
	}
	
	double EgoVertex::distance(Biangle& other) const {
		return hypot(biangle, other);
	}
	
	std::string EgoVertex::str() const { return str(2); }
	
	std::string EgoVertex::str(int dec) const {
		std::ostringstream oss;
		oss << "(" << ang_str(dec) << "), (" << point_str(dec) << ")";
		return oss.str();
	}
	
	std::string EgoVertex::ang_str() const { return ang_str(2); }
	
	std::string EgoVertex::ang_str(int dec) const {
		std::ostringstream oss;
		oss << std::fixed << std::setprecision(dec);
		oss << toDeg(biangle.theta) << ", " << toDeg(biangle.phi);
		return oss.str();
	}
	
	std::string EgoVertex::point_str() const { return point_str(2); }
	
	std::string EgoVertex::point_str(int dec) const {
		std::ostringstream oss;
		oss << std::fixed << std::setprecision(dec);
		oss << point.x << ", " << point.y << ", " << point.z;
		return oss.str();
	}
	
	std::string EgoVertex::neigh_str() const { return neigh_str(2); }
	
	std::string EgoVertex::neigh_str(int dec) const {
		std::ostringstream oss;
		//std::set<EgoVertex*>::const_iterator it;
		oss << "Vertex: " << str(dec) << std::endl;
		for (unsigned int i=0; i<neighbors.size(); i++) {
			oss << "  Neighbor " << i << ": ";
			oss << neighbors[i]->str(dec);
			oss << ", bearing: " << toDeg(neighbor_bearing[i]) << std::endl;
		}
		return oss.str();
	}
	
	void EgoVertex::setIndex(unsigned int ix) {
		vix = ix;
	}
	
	void EgoVertex::addNeighbor(EgoVertex* ev) {
		neighbors.push_back(ev);
		Biangle ev_b;
		ev->getBiangle(ev_b);
		double bearing = Utils::bearing(&biangle, &ev_b);
		neighbor_bearing.push_back(bearing);
	}
	
};
