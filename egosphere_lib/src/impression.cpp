/*
 * Put copyright notice here
 */
#include <egosphere/impression.hpp>
#include <iostream>
#include <iomanip>
#include <sstream>

namespace egosphere_demo {
	
	Impression::Impression() {
		m_biangle.invalidate();
	}
	
	Impression::Impression(const egosphere::StampedPoint& impLocal) {
		setFromStampedPoint(impLocal);
	}
	
	Impression::~Impression() { }
	
	void Impression::setFromStampedPoint(const egosphere::StampedPoint& impLocal) {
		egosphere::Utils::calcBiangle(impLocal, m_biangle);
		m_stamp = impLocal.t;
	}
	
	//std::string Impression::str() const {
	//	std::ostringstream oss;
	//	return oss.str();
	//}
	
};

