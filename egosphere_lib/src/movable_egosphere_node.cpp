/**
 * Put copyright notice here
 */
#include <egosphere/movable_egosphere_node.hpp>
#include <egosphere/egosphere_ros_utils.hpp>
#include <egosphere/egosphere_tf_utils.hpp>
#include <tf/transform_datatypes.h>
#include <sstream>

namespace egosphere_demo {
	
	MovableEgosphereNode::MovableEgosphereNode(ros::NodeHandle& nh)
			: m_egosphere(NULL)
			, m_timerPubRate(ros::Duration(0.01))
			, m_timerTfRate(ros::Duration(0.01))
	{
		ROS_INFO("MovableEgosphereNode: in constructor...");
		// *** load the parameters
		int rate, freq;
		nh.param("frequency", freq, 8);
		nh.param("radius", m_radius, 1.0);
		nh.param("pub_rate", rate, 10);
		m_timerPubRate = ros::Duration(rate / 1000.0);
		nh.param("tf_rate", rate, 10);
		m_timerTfRate = ros::Duration(rate / 1000.0);
		nh.param("center_frame", m_center, std::string("ego_center"));
		nh.param("global_frame", m_ground, std::string("ego_global"));
		
		// *** create egosphere and related
		m_centerPose.orientation.w = 1.0;
		m_name = ros::this_node::getName();
		m_egosphere = new MovableEgosphere(freq);
		egosphere::RosUtils::fillMarkerEgoVerts(m_center, m_radius,
		                                        m_egosphere->m_vertices,
		                                        m_egosphere->m_egosearch,
															 m_egoVerts);
		
		// *** create TF and pubs/subs
		if (!m_tfliPtr) {
			m_tfliPtr.reset(new tf::TransformListener());
		}
		if (!m_tfbrPtr) {
			m_tfbrPtr.reset(new tf::TransformBroadcaster());
		}
		m_egoVerts_pub = nh.advertise<MarkerArray>(m_name +"/markers", 1);
		m_center_sub = nh.subscribe(m_name +"/center_pose", 1, &MovableEgosphereNode::centerPoseCb, this);
		m_impAdd_sub = nh.subscribe(m_name +"/imp_add", 1, &MovableEgosphereNode::impAddCb, this);
		m_impRemove_sub = nh.subscribe(m_name +"/imp_remove", 1, &MovableEgosphereNode::impRemoveCb, this);
		m_imp_pub = nh.advertise<MarkerArray>(m_name +"/impressions", 1);
		
		// *** start execution (timers, prime TF, etc.)
		m_timerPub = nh.createTimer(m_timerPubRate, &MovableEgosphereNode::pubTimer, this);
		m_timerTf = nh.createTimer(m_timerTfRate, &MovableEgosphereNode::tfTimer, this);
		try {
			m_tfliPtr->waitForTransform(m_center, m_ground, ros::Time::now(), ros::Duration(3.0));
		} catch (tf::TransformException &ex) {
			ROS_ERROR("Frame lookup [%s], [%s]: %s", m_center.c_str(), m_ground.c_str(), ex.what());
		}
		showConfig();
		ROS_INFO("MovableEgosphereNode: exiting constructor");
	}
	
	MovableEgosphereNode::~MovableEgosphereNode() {
		if (m_egosphere) delete m_egosphere;
	}
	
	void MovableEgosphereNode::showConfig() const {
		ROS_INFO("-----------------------------------------------");
		ROS_INFO("  Egosphere configuration:");
		ROS_INFO("          node name: %s", m_name.c_str());
		ROS_INFO("          frequency:    %d", m_egosphere->getFrequency());
		ROS_INFO("    min angle (deg): %6.3f", m_egosphere->getMinAngle(true));
		ROS_INFO("  min rays distance:");
		ROS_INFO("              @0.5m: %7.4f", egosphere::Utils::distance(m_egosphere->getMinAngle(), 0.5));
		ROS_INFO("              @1.0m: %7.4f", egosphere::Utils::distance(m_egosphere->getMinAngle(), 1.0));
		ROS_INFO("              @2.0m: %7.4f", egosphere::Utils::distance(m_egosphere->getMinAngle(), 2.0));
		ROS_INFO("              @5.0m: %7.4f", egosphere::Utils::distance(m_egosphere->getMinAngle(), 5.0));
		ROS_INFO("    max angle (deg): %6.3f", m_egosphere->getMaxAngle(true));
		ROS_INFO("  max rays distance:");
		ROS_INFO("              @0.5m: %7.4f", egosphere::Utils::distance(m_egosphere->getMaxAngle(), 0.5));
		ROS_INFO("              @1.0m: %7.4f", egosphere::Utils::distance(m_egosphere->getMaxAngle(), 1.0));
		ROS_INFO("              @2.0m: %7.4f", egosphere::Utils::distance(m_egosphere->getMaxAngle(), 2.0));
		ROS_INFO("              @5.0m: %7.4f", egosphere::Utils::distance(m_egosphere->getMaxAngle(), 5.0));
		ROS_INFO("       global frame: %s", m_ground.c_str());
		ROS_INFO("       center frame: %s", m_center.c_str());
		ROS_INFO("  sphere radius (m): %5.2f", m_radius);
		ROS_INFO("  display rate (Hz): %5.2f", (m_timerPubRate.toSec() * 1000.0));
		ROS_INFO("       TF rate (Hz): %5.2f", (m_timerTfRate.toSec() * 1000.0));
		ROS_INFO("-----------------------------------------------");
	}
	
	void MovableEgosphereNode::pubTimer(const ros::TimerEvent &te) const {
		// note: message queue-event triggered, so no mutex coordination among
		//   pubs, tf lookups, and subscriber data entry needed!
		pubEgoVerts();
		pubImpressions();
	}
	
	void MovableEgosphereNode::tfTimer(const ros::TimerEvent &te) const {
		// note: message queue-event triggered, so no mutex coordination among
		//   pubs, tf lookups, and subscriber data entry needed!
		broadcast();
		updateImpressions();
	}
	
	void MovableEgosphereNode::pubEgoVerts() const {
		// triggered by pub timer, center pose callback
		m_egoVerts_pub.publish(m_egoVerts);
	}
	
	void MovableEgosphereNode::centerPoseCb(const geometry_msgs::Pose::ConstPtr &msg) {
		// note: message queue-event triggered, so no mutex coordination among
		//   pubs, tf lookups, and subscriber data entry needed!
		m_centerPose = *msg;
		broadcast();
		updateImpressions();
		pubImpressions();
		pubEgoVerts();
	}
	
	void MovableEgosphereNode::broadcast() const {
		// triggered by TF timer, center pose callback
		tf::Transform transform;
		tf::poseMsgToTF(m_centerPose, transform);
		m_tfbrPtr->sendTransform(tf::StampedTransform(transform, ros::Time::now(), m_ground, m_center));
	}
	
	void MovableEgosphereNode::updateImpressions() const {
		// triggered by TF timer, center pose callback
		std::vector<std::string> iids = m_egosphere->getImpressionIds();
		for (int i=0; i<iids.size(); ++i) {
			try {
				m_egosphere->updateImpression(iids[i], impressionPosition(iids[i]));
			} catch (tf::TransformException &ex) {
				ROS_ERROR("MovableEgosphereNode::updateImpressions: [%s] %s", iids[i].c_str(), ex.what());
			}
		}
	}
	
	egosphere::StampedPoint MovableEgosphereNode::impressionPosition(
	                                            const std::string& iid) const {
		geometry_msgs::PointStamped relP =
		               egosphere::TfUtils::getPointStampedMsgViaTf(m_tfliPtr,
		                                                       m_center, iid);
		egosphere::StampedPoint sp;
		egosphere::RosUtils::pointStampedMsgToStampedPoint(relP, sp);
		return sp;
	}
	
	void MovableEgosphereNode::pubImpressions() const {
		// triggered by pub timer, center pose callback
		egosphere::Point egoP;
		std::vector<std::string> iids = m_egosphere->getImpressionIds();
		for (int i=0; i<iids.size(); ++i) {
			updateImpressionMarker(iids[i]);
			m_imp_pub.publish(getImpressionMarker(iids[i]));
		}
	}
	
	void MovableEgosphereNode::impAddCb(const std_msgs::String::ConstPtr &msg) {
		try {
			egosphere::StampedPoint impPoint = impressionPosition(msg->data);
			m_egosphere->addImpression(msg->data, impPoint);
			m_impMarkers[msg->data] = createImpressionMarker(msg->data);
			updateImpressionMarker(msg->data);
		} catch (tf::TransformException &ex) {
			ROS_ERROR("MovableEgosphereNode::impAddCb: [%s] %s", msg->data.c_str(), ex.what());
		}
	}
	
	void MovableEgosphereNode::impRemoveCb(const std_msgs::String::ConstPtr &msg) {
		// remove impression from m_egosphere
	}
	
	MarkerArray MovableEgosphereNode::createImpressionMarker(
	                                            const std::string& iid) const {
		MarkerArray marr;
		geometry_msgs::Pose aPose;
		Marker mBi;
		egosphere::RosUtils::initMarker(egosphere::highlite::READING,
		                                m_center, aPose, mBi);
		mBi.id = marr_ix::BIANGLE;
		mBi.ns = iid;
		marr.markers.push_back(mBi);
		
		Marker mDir;
		egosphere::RosUtils::initMarker(egosphere::highlite::DIRECTION,
		                                m_center, aPose, mDir);
		mDir.id = marr_ix::DIRECTION;
		mDir.ns = iid;
		marr.markers.push_back(mDir);
		//Marker mDirS;
		//egosphere::RosUtils::initMarker(egosphere::highlite::DIR_SHAFT,
		//                                m_center, aPose, mDirS);
		//mDirS.id = marr_ix::DIR_SHAFT;
		//mDirS.ns = iid;
		//marr.markers.push_back(mDirS);
		//
		//Marker mDirA;
		//egosphere::RosUtils::initMarker(egosphere::highlite::DIRECTION,
		//                                m_center, aPose, mDirA);
		//mDirA.id = marr_ix::DIR_ARROW;
		//mDirA.ns = iid;
		//marr.markers.push_back(mDirA);
		
		Marker mFace;
		egosphere::RosUtils::initMarker(egosphere::highlite::FACE,
		                                m_center, aPose, mFace);
		mFace.id = marr_ix::FACE_FULL;
		mFace.ns = iid;
		marr.markers.push_back(mFace);
		return marr;
	}
	
	void MovableEgosphereNode::updateImpressionMarker(const std::string& iid) const {
		std::map<std::string,MarkerArray>::const_iterator it = m_impMarkers.find(iid);
		if (it != m_impMarkers.end()) {
			egosphere::Point impPoint = m_egosphere->getImpressionPosition(iid);
			MarkerArray& marr = const_cast<MarkerArray&>(it->second);
			// BIANGLE: surface sphere
			marr.markers[marr_ix::BIANGLE].pose =
		                egosphere::TfUtils::getPoseMsgRadius(m_radius, impPoint);
			double dirDist = egosphere::distance(impPoint);
			// DIR_SHAFT: arrow shaft (cylinder)
			// TODO: orientation is for arrow! needs rotation!
			// TODO: position needs offset of half (dirDist * 0.95)!
			//tf::Transform tfCyl(tf::Transform::getIdentity());
			//tfCyl.getOrigin().setX((dirDist * 0.95) / 2.0);
			//tf::Transform tfTmp(tf::Transform::getIdentity());
			//tfTmp.setRotation(tf::createQuaternionFromRPY(0.0, -M_PI/2.0, -M_PI/2.0));
			//tfTmp.setRotation(tf::createQuaternionFromRPY(0.0, -M_PI/2.0, 0.0));
			//tfTmp.setRotation(tf::createQuaternionFromRPY(0.0, -M_PI, 0.0));
			//tf::Quaternion tq;
			//tf::quaternionMsgToTF(marr.markers[marr_ix::BIANGLE].pose.orientation, tq);
			//tfTmp.setRotation(tq);
			//tfCyl *= tfTmp;
			//tf::poseTFToMsg(tfCyl, marr.markers[marr_ix::DIR_SHAFT].pose);
			//tf::quaternionTFToMsg(tfCyl, marr.markers[marr_ix::DIR_SHAFT].pose.orientation);
			//marr.markers[marr_ix::DIR_SHAFT].pose.orientation = marr.markers[marr_ix::BIANGLE].pose.orientation;
			//marr.markers[marr_ix::DIR_SHAFT].scale.z = dirDist * 0.95;
			// DIR_ARROW: arrow tip (arrow)
			// TODO: position needs offset of 0.05* dirDist!
			//marr.markers[marr_ix::DIR_ARROW].pose.orientation = marr.markers[marr_ix::BIANGLE].pose.orientation;
			//marr.markers[marr_ix::DIR_ARROW].scale.x = dirDist * 0.05;
			//marr.markers[marr_ix::DIR_ARROW].scale.x = dirDist;
			// DIRECTION: arrow
			marr.markers[marr_ix::DIRECTION].pose.orientation = marr.markers[marr_ix::BIANGLE].pose.orientation;
			marr.markers[marr_ix::DIRECTION].scale.x = dirDist;
			// FACE_FULL: neighbor highlite
			std::vector<egosphere::Point> npVec = m_egosphere->getNeighborPoints(iid);
			egosphere::Point ep;
			egosphere::RosUtils::pointMsgToPoint(marr.markers[marr_ix::BIANGLE].pose.position, ep);
			egosphere::RosUtils::fillFaceMarkerPoints(ep, npVec, marr.markers[marr_ix::FACE_FULL]);
		}
	}
	
	const MarkerArray& MovableEgosphereNode::getImpressionMarker(const std::string& iid) const {
		std::map<std::string, MarkerArray>::const_iterator it = m_impMarkers.find(iid);
		if (it != m_impMarkers.end()) {
			return it->second;
		}
		throw std::invalid_argument("ID ["+ iid +"] not found");
	}
	
};

int main(int argc, char **argv) {
	ros::init(argc, argv, "movable_egosphere_node");
	ros::NodeHandle nh("~");
	egosphere_demo::MovableEgosphereNode egoNode(nh);
	ros::spin();
	return(0);
}

