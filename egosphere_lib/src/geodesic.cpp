/*
 * Put copyright notice here
 */

#include <egosphere/geodesic.h>
#include <vector>
#include <map>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <sys/time.h>

// NOTE: the frequency = #subdivisions +1
// e.g., f=1 is a basic icosahedron
#define FREQUENCY 2

// NOTE: lots of code in '#ifdef DEBUG' to be removed...
//#define DEBUG

namespace egosphere {
	
	// **********************************************************
	// **** Geosphere public functions
	// **********************************************************
	Geosphere::Geosphere() : geoSearch(0) {
		generateIcosahedron();
		subdivide(FREQUENCY-1);
		finishConstruction();
	}
	
	Geosphere::Geosphere(int f) : geoSearch(0) {
		generateIcosahedron();
		subdivide(f-1);
		finishConstruction();
	}
	
	Geosphere::~Geosphere() {
		std::vector<GeoVertex*>::iterator it;
		for (it=vertices.begin(); it<vertices.end(); it++) {
			delete *it;
		}
		std::vector<GeoVoronoiVertex*>::iterator vit;
		for (vit=voronoi.begin(); vit<voronoi.end(); vit++) {
			delete *vit;
		}
		if (geoSearch) delete geoSearch;
	}
	
	int Geosphere::frequency() const {
		return freq;
	}
	
	GeosphereMetainfo Geosphere::getMetainfo() const {
		GeosphereMetainfo mi;
		mi.freq = freq;
		mi.numVerts = vertices.size();
		// get the vertex info
		std::vector<GeoVertex*>::const_iterator itvert;
		for (itvert=vertices.begin(); itvert<vertices.end(); itvert++) {
			mi.minTheta = std::min(mi.minTheta, (*itvert)->getBiangle()->theta);
			mi.maxTheta = std::max(mi.maxTheta, (*itvert)->getBiangle()->theta);
			mi.minPhi   = std::min(mi.minPhi,   (*itvert)->getBiangle()->phi);
			mi.maxPhi   = std::max(mi.maxPhi,   (*itvert)->getBiangle()->phi);
			mi.minVertD = std::min(mi.minVertD, (*itvert)->minNeighborDistance());
			mi.maxVertD = std::max(mi.maxVertD, (*itvert)->maxNeighborDistance());
		}
		mi.numVoros = voronoi.size();
		// get the max voro dist
		std::vector<GeoVoronoiVertex*>::const_iterator itvoro;
		for (itvoro=voronoi.begin(); itvoro<voronoi.end(); itvoro++) {
			mi.minVertVoroD = std::min(mi.minVertVoroD, (*itvoro)->minVertexDistance());
			mi.maxVertVoroD = std::max(mi.maxVertVoroD, (*itvoro)->maxVertexDistance());
		}
		return mi;
	}
	
	void Geosphere::getVertices(std::vector<GeoVertex*>& verts) {
		verts.clear();
		verts.assign(vertices.begin(), vertices.end());
	}
	
	/* moved to header file because it needs to be a template
	EgoSearch Geosphere::getEgoStructs(std::vector<EgoVertex*>& verts) {
		std::vector<GeoVertex*>::iterator gvit;
		std::vector<GeoVertex*>::const_iterator gvneighit;
		std::map<const GeoVertex*, EgoVertex*> gv2ev;
		std::map<const GeoVertex*, EgoVertex*>::iterator mapit;
		std::vector<GeoVertex*> neighs;
		std::vector< std::vector< std::vector<EgoVertex*> > > vec3;
		
		// create the pared-down EgoVertices and mapping
		verts.clear();
		for (gvit=vertices.begin(); gvit<vertices.end(); gvit++) {
			Biangle gv_b( *((*gvit)->getBiangle()) );
			EgoVertex *ev = new EgoVertex(gv_b);
			verts.push_back(ev);
			gv2ev.insert( std::pair<GeoVertex*, EgoVertex*>(*gvit, ev) );
		}
		// fill in the neighbors
		for (gvit=vertices.begin(); gvit<vertices.end(); gvit++) {
			(*gvit)->getNeighbors(neighs);
			EgoVertex *ev = gv2ev[(*gvit)];
			for (gvneighit=neighs.begin(); gvneighit<neighs.end(); gvneighit++) {
				ev->addNeighbor(gv2ev[(*gvneighit)]);
			}
		}
		// map the GeoVertex search to EgoVertex
		geoSearch->map2EgoSearch(gv2ev, vec3);
		EgoSearch es(metainfo.minTheta, metainfo.maxTheta, metainfo.maxVertD,
		             metainfo.minPhi, metainfo.maxPhi, metainfo.maxVertD,
		             vec3);
		return es;
	}
	*/
		
	double Geosphere::minDistanceExhaustive(double theta, double phi) {
		GeoVertex* ev = minDistanceVertexExhaustive(theta, phi);
		if (ev == 0) return M_PI;
		return ev->distance(theta, phi);
	}

	GeoVertex* Geosphere::minDistanceVertexExhaustive(double theta, double phi) {
		double d, tmp;
		GeoVertex* ev = 0;
		d = tmp = 2*M_PI;
		std::vector<GeoVertex*>::const_iterator it;
		for (it=vertices.begin(); it<vertices.end(); it++) {
			tmp = (*it)->distance(theta, phi);
			d = std::min(d, tmp);
			if (d == tmp) {
				ev = *it;
			}
		}
		return ev;
	}
	
	bool Geosphere::printNearest(double theta, double phi) {
		Biangle b(theta, phi);
		double ed;
		bool correct=false;
		
		GeoVertex const *ev = geoSearch->nearest(b);
		GeoVertex *ex;
		if (ev == 0) {
			ex = minDistanceVertexExhaustive(theta, phi);
			ed = ex->distance(theta, phi);
			std::cout << "**** Not found (NEED TO FIX THIS)! Exhaustive: (";
			std::cout << ex->ang_str() << "), d=" << ed << std::endl;
		} else {
			double nd;
			nd = ev->distance(theta, phi);
			ex = minDistanceVertexExhaustive(theta, phi);
			ed = ex->distance(theta, phi);
			std::cout << "    Found: " << ev->str() << std::endl;
			std::cout << "    Distance: " << nd;
			if (ev == ex) {
				correct = true;
				std::cout << ", same as exhaustive check" << std::endl;
			} else {
				if (nd == ed) {
					std::cout << " == " << ed << " *** nearest != exhaustive; maybe wrong?" << std::endl;
					std::cout << "    Exhst: " << ex->str() << std::endl;
				} else {
					std::cout << " != " << ed << " **** NEED TO FIX THIS!" << std::endl;
					std::cout << "    Exhst: " << ex->str() << std::endl;
				}
			}
		}
		return correct;
	}
	
	std::string Geosphere::vert_str() const {
		std::vector<GeoVertex*>::const_iterator it; //, nit;
		std::ostringstream oss;
		int count = 1;
		
		for (it=vertices.begin(); it<vertices.end(); it++) {
			oss << count << ":" << *it << ":\t" << (*it)->str() << std::endl;
			count++;
		}
		return oss.str();
	}
	
	std::string Geosphere::voro_str() const {
		std::vector<GeoVoronoiVertex*>::const_iterator it;
		std::ostringstream oss;
		int count = 1;
		for (it=voronoi.begin(); it<voronoi.end(); it++) {
			oss << count << " (" << (*it)->str() << ")" << ((*it)->wrap ? "*" : " ");
			oss << ":\t" << (*it)->vert_str() << std::endl;
			count++;
		}
		return oss.str();
	}
	
	std::string Geosphere::metainfo_str() const {
		return metainfo.str();
	}
	
	std::string Geosphere::search_str() const {
		return search_str(2);
	}
	
	std::string Geosphere::search_str(int dec) const {
		return geoSearch->str(dec);
	}
	
	std::string Geosphere::search_sum_str() const {
		return geoSearch->sum_str();
	}
	
	void Geosphere::writeFiles(std::string finm) const {
		writeFile_Faces(finm);
	}
	
	void Geosphere::test_search(int n) {
		int tick = n < 99999 ? 1000 : (n < 999999 ? 10000 : 50000);
		std::ostringstream oss;
		Biangle *b;
		Point *p;
		GeoVertex const *ev, *ex;
		double tsrchtot=0.0, tsrch_carttot=0.0, texhtot=0.0;
		double dsrch, dexh;
		timeval tsrchst, tsrched, tsrch_cartst, tsrch_carted, texhst, texhed;
		double t1, t2, tsrchelapsed, tsrch_cartelapsed, texhelapsed;
		double tsrchmin=9999.0, tsrchmax=0.0, tsrchavg=0.0;
		double tsrch_cartmin=9999.0, tsrch_cartmax=0.0, tsrch_cartavg=0.0;
		double texhmin=9999.0, texhmax=0.0, texhavg=0.0;
		std::cout << "Performing " << n << " randomized searches...";
		srand(time(0));
		for (int i=0; i<n; i++) {
			if (i%tick == 0) {
				std::cout << i << "..." << std::endl;
			}
			b = new Biangle(
				(((double)(rand() % 1000000) / 1000000.0) * 2*M_PI) - M_PI,
				(((double)(rand() % 1000000) / 1000000.0) * M_PI) - (M_PI / 2.0));
			p = Utils::calcPoint(b);
			
			gettimeofday(&tsrchst, NULL);
			ev = geoSearch->nearest(*b);
			gettimeofday(&tsrched, NULL);
			t1 = tsrchst.tv_sec+(tsrchst.tv_usec/1000000.0);
			t2 = tsrched.tv_sec+(tsrched.tv_usec/1000000.0);
			tsrchelapsed = t2-t1;
			tsrchtot += tsrchelapsed;
			tsrchmin = std::min(tsrchmin, tsrchelapsed);
			tsrchmax = std::max(tsrchmax, tsrchelapsed);
			tsrchavg += tsrchelapsed;
			
			gettimeofday(&tsrch_cartst, NULL);
			ev = geoSearch->nearest(*p);
			gettimeofday(&tsrch_carted, NULL);
			t1 = tsrch_cartst.tv_sec+(tsrch_cartst.tv_usec/1000000.0);
			t2 = tsrch_carted.tv_sec+(tsrch_carted.tv_usec/1000000.0);
			tsrch_cartelapsed = t2-t1;
			tsrch_carttot += tsrch_cartelapsed;
			tsrch_cartmin = std::min(tsrch_cartmin, tsrch_cartelapsed);
			tsrch_cartmax = std::max(tsrch_cartmax, tsrch_cartelapsed);
			tsrch_cartavg += tsrch_cartelapsed;
			
			gettimeofday(&texhst, NULL);
			ex = minDistanceVertexExhaustive(b->theta, b->phi);
			gettimeofday(&texhed, NULL);
			t1 = texhst.tv_sec+(texhst.tv_usec/1000000.0);
			t2 = texhed.tv_sec+(texhed.tv_usec/1000000.0);
			texhelapsed = t2-t1;
			texhtot += texhelapsed;
			texhmin = std::min(texhmin, texhelapsed);
			texhmax = std::max(texhmax, texhelapsed);
			texhavg += texhelapsed;
			
			if (ev != 0) {
				if (ev != ex) {
					dsrch = ev->distance(b->theta, b->phi);
					dexh  = ex->distance(b->theta, b->phi);
					oss << "Searched for (" << b->str(15) << "): ";
					if (dsrch != dexh) {
						oss << "*** INCORRECT! ***" << std::endl;
					} else {
						oss << "same distance, different vertices" << std::endl;
					}
					oss << "\tFound (" << ev->ang_str(15) << "), d=" << toDeg(dsrch) << std::endl;
					oss << "\tExhau (" << ex->ang_str(15) << "), d=" << toDeg(dexh) << std::endl;
				}
			} else {
				oss << "Searched for (" << b->str(15) << "): MISSING!" << std::endl;
				dexh  = ex->distance(b->theta, b->phi);
				oss << "  Exhaustive is (" << ex->ang_str(15) << "), d=" << toDeg(dexh) << std::endl;
			}
			delete b;
			delete p;
		}
		std::cout << oss.str() << std::endl;
		std::cout << "Total of " << n << " locations; total search=" << tsrchtot;
		std::cout << ", total exhaust=" << texhtot << std::endl;
		std::cout << "Search times:  min / max / avg -> " << (tsrchmin*1000) << "\t/ ";
		std::cout << (tsrchmax*1000) << "\t/ " << (tsrchavg * 1000 / n) << std::endl;
		std::cout << "Cartesn times: min / max / avg -> " << (tsrch_cartmin*1000) << "\t/ ";
		std::cout << (tsrch_cartmax*1000) << "\t/ " << (tsrch_cartavg * 1000 / n) << std::endl;
		std::cout << "Exhaust times: min / max / avg -> " << (texhmin*1000) << "\t/ ";
		std::cout << (texhmax*1000) << "\t/ " << (texhavg*1000 / n) << std::endl;
	}
	
	// **********************************************************
	// **** private functions
	// **********************************************************
	void Geosphere::generateIcosahedron() {
#ifdef DEBUG
		std::cout << "Geosphere: generateIcosahedron()" << std::endl;
#endif
		double phi_r1 = atan(1.0 / 2.0);  // +/- altitude of vertex rings
		double theta_off = 2*M_PI / 10.0; // azimuth between non-pole vertices
		double theta_inc = 2*theta_off;   // azimuth between vertices at ring
		double theta;
		
		// create the base icosahedron vertices
		// push the apex, index 0
		vertices.push_back(new GeoVertex(0.0, M_PI/2.0));
		// push ring1, index 1-5
		theta = 0.0;
		for (int i=1; i<6; i++) {
			vertices.push_back(new GeoVertex(theta, phi_r1));
			theta += theta_inc;
		}
		// push ring2, index 6-10
		theta = theta_off;
		for (int i=6; i<11; i++) {
			vertices.push_back(new GeoVertex(theta, -phi_r1));
			theta += theta_inc;
		}
		// push the nadir, index 11
		vertices.push_back(new GeoVertex(0.0, -M_PI/2.0));
		for (int i=0; i<(int)vertices.size(); i++) {
			vertices[i]->setIndex(i);
		}
	}
	
	void Geosphere::subdivide(int numdiv) {
#ifdef DEBUG
		std::cout << "Geosphere::subdivide(" << numdiv << ")" << std::endl;
#endif
		// use the icosahedron vertices to create triangles.
		// make sure the vector will have appropriate capacity
		//   known formula: V = 10 * freq^2 + 2
		freq = numdiv+1;
		vertices.reserve(10 * pow(freq, 2) + 2);
		// not interested in keeping faces around, so they're set here in the
		// subdivision routine; temporary storage, goes out of scope upon exit
		std::vector<GeoFace*> faces;
		stuffBasicFaces(faces);
		
		// subdivide each of the icosahedron's triangular faces
		// we're not worrying about 'pushing' the vertices out to the
		// sphere's surface, so there is an easy method of face subdivision
		// that uses the following scheme:
		// - choose one face edge as the 'major' edge
		// - recognize that all vertices will be located either on the
		//   'major' edge OR on a line parallel to that 'major' edge (with
		//   endpoints on the 'minor' edges)
		// - endpoints of the parallels are spaced equally apart, there being
		//   exactly 'numdiv' of them
		// - each parallel will have exactly one less vertex (and thus one
		//   less segment) as the previous line
		// note that vertex order is *important*: 'major' goes from v1->v2,
		//   'minor1' from v1->v3, and 'minor2' from v2->v3
		
		// TODO: rather than vertices.push_back, might be nice to insert
		std::vector<GeoFace*>::iterator fit;
		for (fit=faces.begin(); fit<faces.end(); fit++) {
			std::vector<GeoVertex*> e1, e2, e3;
			GeoVertex *priorverts_eplo, *priorverts_ephi;
#ifdef DEBUG
			std::cout << "Starting face #" << fcount << std::endl;
#endif
			
			// get subdivision vertices of 'major', put in e1
			if (!(*fit)->m1) {
				subdiv_ico_edge(*fit, numdiv, 1, e1);
			} else {
				(*fit)->getSubVerts((*fit)->v1, (*fit)->v2, e1);
			}
#ifdef DEBUG
			std::cout << "  'Major'  verts: ";
			std::cout << "(" << (*fit)->v1->ang_str() << ") ";
			for (unsigned int i=0; i<e1.size(); i++) {
				std::cout << "(" << e1[i]->ang_str() << ") ";
			}
			std::cout << "(" << (*fit)->v2->ang_str() << ") ";
			std::cout << std::endl;
#endif
			
			// use the other edges to establish the edges parallel to the
			// 'major' edge; parallel line endpoints are subdivided 'minor'
			// note that minor1 must start at v1 and that minor edges must
			// both end at v3
			if (!(*fit)->m2) {
				subdiv_ico_edge(*fit, numdiv, 2, e2);
			} else {
				(*fit)->getSubVerts((*fit)->v1, (*fit)->v3, e2);
			}
#ifdef DEBUG
			std::cout << "  'Minor2' verts: ";
			std::cout << "(" << (*fit)->v1->ang_str() << ") ";
			for (unsigned int i=0; i<e2.size(); i++) {
				std::cout << "(" << e2[i]->ang_str() << ") ";
			}
			std::cout << "(" << (*fit)->v3->ang_str() << ") ";
			std::cout << std::endl;
#endif
			
			if (!(*fit)->m3) {
				subdiv_ico_edge(*fit, numdiv, 3, e3);
			} else {
				(*fit)->getSubVerts((*fit)->v2, (*fit)->v3, e3);
			}
#ifdef DEBUG
			std::cout << "  'Minor3' verts: ";
			std::cout << "(" << (*fit)->v2->ang_str() << ") ";
			for (unsigned int i=0; i<e3.size(); i++) {
				std::cout << "(" << e3[i]->ang_str() << ") ";
			}
			std::cout << "(" << (*fit)->v3->ang_str() << ") ";
			std::cout << std::endl;
#endif
			
			// remaining vertices to add aren't on existing edge, so
			// can be added with no checks for previous subdivision
			std::vector<GeoVertex*> newverts, priorverts=e1;
			priorverts_eplo = (*fit)->v1;
			priorverts_ephi = (*fit)->v2;
#ifdef DEBUG
			std::cout << "  Beginning subdiv; ";
			std::cout << "eplo=(" << priorverts_eplo->ang_str() << ") ";
			std::cout << "ephi=(" << priorverts_ephi->ang_str() << ") ";
			std::cout << std::endl;
#endif
			for (int i=0; i<numdiv; i++) {
			//for (int i=0; i<(numdiv-1); i++) {
				GeoVertex *prev;
				std::vector<GeoVertex*> aug_newverts;
				std::vector<GeoVertex*>::iterator nit;
				
				// subdivide parallel into 1 less segments than previous
				// and set neighbors (requires 'prev' be e2[i] initially)
#ifdef DEBUG
				std::cout << "  Subdividing parallel (" << e2[i]->ang_str() << ") ";
				std::cout << "(" << e3[i]->ang_str() << ") ";
				std::cout << (numdiv-1)-i << " times" << std::endl;
				std::cout << "                (" << e2[i]->point_str() << ") ";
				std::cout << "(" << e3[i]->point_str() << ") " << std::endl;
#endif
				subdiv_help(e2[i], e3[i], (numdiv-1)-i, newverts);
				prev = e2[i];
				for (nit=newverts.begin(); nit<newverts.end(); nit++) {
					vertices.push_back(*nit);
#ifdef DEBUG
					std::cout << "  Added vertex: ";
					std::cout << "(" << (*nit)->ang_str() << ")    ";
					std::cout << "(" << (*nit)->point_str() << ")";
					std::cout << std::endl;
#endif
					prev->addNeighbor(*nit, true);
					prev = *nit;
				}
				prev->addNeighbor(e3[i], true); // last neighbor 'prev' to e3[i]
				
				// set the neighbors between the new vertices and the previous
				// parallel (which, initially i==0, *is* the 'major')
				// to do so, walk the 'priorverts' and create 2 neighbors for
				// each; need the endpoints on the 'minors' also, but need to
				// preserve 'newverts' for when it becomes 'priorverts'...so put
				// its contents in a temp vector called 'aug_newverts' that is
				// augmented with the minor's endpoints
				aug_newverts.assign(newverts.begin(), newverts.end());
				aug_newverts.insert(aug_newverts.begin(), e2[i]);
				aug_newverts.push_back(e3[i]);
#ifdef DEBUG
					std::cout << "  Added: front ";
					std::cout << "(" << aug_newverts[0]->ang_str() << ") ";
					std::cout << "back (" << aug_newverts[aug_newverts.size()-1]->ang_str();
					std::cout << ")";
					std::cout << std::endl;
#endif
				
				// since we just added the endpoints, we know that 'aug_newverts'
				// now has one more vertex than 'priorverts', so the increment is
				// guaranteed to stay in bounds
				// have finished triangle faces here; calculate/set the voronoi
				// vertices
#ifdef DEBUG
				std::cout << "  Begin processing subdiv faces: ";
				std::cout << std::endl;
#endif
				int ix_newverts = 0;
				for (nit=priorverts.begin(); nit<priorverts.end(); nit++) {
					// voronoi for face 'below' nit & aug_newverts[ix_newverts]
					//   ('lower' vertex is priorverts_eplo)
#ifdef DEBUG
					std::cout << "    Voronoi (lower) from: ";
					std::cout << "(" << priorverts_eplo->ang_str() << ") ";
					std::cout << "(" << (*nit)->ang_str() << ") ";
					std::cout << "(" << aug_newverts[ix_newverts]->ang_str() << ") ";
					std::cout << std::endl;
#endif
					voronoi.push_back(new GeoVoronoiVertex(priorverts_eplo,
					                                       *nit,
					                                       aug_newverts[ix_newverts]));
#ifdef DEBUG
					std::cout << "    Added (lower) voronoi: ";
					std::cout << "(" << voronoi[voronoi.size()-1]->str() << ") ";
					std::cout << std::endl;
#endif
					(*nit)->addNeighbor(aug_newverts[ix_newverts], true);
					// voronoi for face 'above' nit & aug_newverts[ix_newverts]
					//   (note ix_newverts-1 is 'lower', ix_newverts is 'upper')
					//   order chosen to preserve CW assignment
					ix_newverts++;
#ifdef DEBUG
					std::cout << "    Voronoi (upper) from: ";
					std::cout << "(" << (*nit)->ang_str() << ") ";
					std::cout << "(" << aug_newverts[ix_newverts]->ang_str() << ") ";
					std::cout << "(" << aug_newverts[ix_newverts-1]->ang_str() << ") ";
					std::cout << std::endl;
#endif
					voronoi.push_back(new GeoVoronoiVertex(*nit,
					                                       aug_newverts[ix_newverts],
					                                       aug_newverts[ix_newverts-1]));
#ifdef DEBUG
					std::cout << "    Added (upper) voronoi: ";
					std::cout << "(" << voronoi[voronoi.size()-1]->str() << ") ";
					std::cout << std::endl;
#endif
					(*nit)->addNeighbor(aug_newverts[ix_newverts], true);
					priorverts_eplo = *nit;
				}
				// voronoi for final face 'above'
				//   priorverts_eplo & aug_newverts[ix_newverts]
				//   ('upper' vertex is priorverts_ephi)
#ifdef DEBUG
				std::cout << "    Voronoi (last)  from: ";
				std::cout << "(" << priorverts_eplo->ang_str() << ") ";
				std::cout << "(" << priorverts_ephi->ang_str() << ") ";
				std::cout << "(" << aug_newverts[ix_newverts]->ang_str() << ") ";
				std::cout << std::endl;
#endif
				voronoi.push_back(new GeoVoronoiVertex(priorverts_eplo,
				                                       priorverts_ephi,
				                                       aug_newverts[ix_newverts]));
#ifdef DEBUG
				std::cout << "    Added (last)  voronoi: ";
				std::cout << "(" << voronoi[voronoi.size()-1]->str() << ") ";
				std::cout << std::endl;
#endif
				// update priorverts_ep[lo|hi]; same as ends of prior aug_newverts
				priorverts_eplo = e2[i];
				priorverts_ephi = e3[i];
#ifdef DEBUG
				std::cout << "  Finished subdiv; set ";
				std::cout << "eplo=(" << priorverts_eplo->ang_str() << ") ";
				std::cout << "ephi=(" << priorverts_ephi->ang_str() << ") ";
				std::cout << std::endl;
#endif
				// swap elements of 'newverts' and 'priorverts'; the contents
				// of 'newverts' will be nuked at the top of the loop, effectively
				// loosing whatever is now in priorverts...but we don't care
				priorverts.swap(newverts);
			}
			// voronoi for final face ('minor' lines join at v3)
#ifdef DEBUG
			std::cout << "  Voronoi (final) from: ";
			std::cout << "(" << priorverts_eplo->ang_str() << ") ";
			std::cout << "(" << priorverts_ephi->ang_str() << ") ";
			std::cout << "(" << (*fit)->v3->ang_str() << ") ";
			std::cout << std::endl;
#endif
			voronoi.push_back(new GeoVoronoiVertex(priorverts_eplo,
			                                       priorverts_ephi,
			                                       (*fit)->v3));
#ifdef DEBUG
			std::cout << "  Added (final) voronoi: ";
			std::cout << "(" << voronoi[voronoi.size()-1]->str() << ") ";
			std::cout << std::endl;
			std::cout << "Finished face #" << fcount++ << std::endl;
#endif
		}
		
#ifdef DEBUG
		std::cout << "Finished subdivisions; deleting faces" << std::endl;
#endif
		for (int j=0; j<(int)faces.size(); j++) {
			delete faces[j];
		}
		faces.clear();
#ifdef DEBUG
		std::cout << "Exiting subdivision(" << numdiv << ")" << std::endl;
#endif
	}
	
	void Geosphere::subdiv_ico_edge(GeoFace *f, int numdiv,
	                                int side, std::vector<GeoVertex*>& ev) {
		GeoVertex *u, *v, *prev;
		std::vector<GeoVertex*>::iterator nit;
		
		if (side == 1) { u=f->v1; v=f->v2; }
		else if (side == 2) { u=f->v1; v=f->v3; }
		else if (side == 3) { u=f->v2; v=f->v3; }
		else { return; }
		// get new vertices for 'major' edge
		subdiv_help(u, v, numdiv, ev);
		f->setSubVerts(u, v, ev);
		// neighbor setting requires 'prev' be u initially
		prev = u;
		// add new vertices to total list
		for (nit=ev.begin(); nit<ev.end(); nit++) {
			vertices.push_back(*nit);
			prev->addNeighbor(*nit, true);
			prev = *nit;
		}
		// last neighbor setting from 'prev' to v
		prev->addNeighbor(v, true);
		f->setDivided(side, true);
	}
	
	void Geosphere::subdiv_help(GeoVertex *u,
	                            GeoVertex *v,
	                            int numdiv,
	                            std::vector<GeoVertex*>& addv) {
		addv.clear();
		if (numdiv < 1) return;
		Biangle const *bu, *bv;
		Point p;
		double frac, fracstep, A, B, tdist;
		// from: 'Intermediate points on a great circle'
		//    http://williams.best.vwh.net/avform.htm
		bu = u->getBiangle(); bv = v->getBiangle();
		tdist = hypot(*bu, *bv);
		frac = 0.0;
		fracstep = 1.0 / (numdiv+1);
		for (int i=0; i<numdiv; i++) {
			frac += fracstep;
			A = sin((1.0-frac)*tdist) / sin(tdist);
			B = sin(frac*tdist) / sin(tdist);
			p.x = (A * bu->cosph * bu->costh) + (B * bv->cosph * bv->costh);
			p.y = (A * bu->cosph * bu->sinth) + (B * bv->cosph * bv->sinth);
			p.z = (A * bu->sinph)             + (B * bv->sinph);
			addv.push_back(new GeoVertex(atan2(p.y, p.x), atan2(p.z, hypot(p.x, p.y))));
		}
	}
	
	void Geosphere::fillAdjacents(std::vector<GeoFace*>& faces) {
		// for this implementation, each face is compared to every other one
		// since it'll only be called during initial construction (and only
		// with the orginal 20 icosahedron faces), just go with it without
		// optimizing or a better algorithm...
		std::vector<GeoFace*>::iterator itcur, itcmp;
		GeoFace *cur, *cmp;
		
		for (itcur=faces.begin(); itcur<faces.end(); itcur++) {
			cur = *itcur;
			for (itcmp=itcur+1; itcmp<faces.end(); itcmp++) {
				cmp = *itcmp;
				// if faces are adjacent, side will be set to 1, 2, or 3
				int side = 0;
				if ((side = cur->adjacent(cmp)) > 0) {
					// adjacent; set cur's reference to cmp in the side (as
					//   calculated), then set cmp's reference to cur in its
					//   appropriate side (needs to be calculated)
					cur->setAdjacent(side, cmp);
					side = cmp->adjacent(cur);
					cmp->setAdjacent(side, cur);
				}
			}
		}
	}
	
	void Geosphere::finishConstruction() {
#ifdef DEBUG
		std::cout << "Finishing construction()" << std::endl;
#endif
		metainfo = getMetainfo();
		
#ifdef DEBUG
		std::cout << "Assigning vertex indices" << std::endl;
#endif
		for (unsigned int i=0; i<vertices.size(); i++) {
			vertices[i]->setIndex(i);
		}
		
#ifdef DEBUG
		std::cout << "Sorting voronoi" << std::endl;
#endif
		std::sort(voronoi.begin(), voronoi.end(), compareGeoVoronoiVertexPtrs());
		
#ifdef DEBUG
		std::cout << "Creating geoSearch structure" << std::endl;
#endif
		geoSearch = new GeoSearch(metainfo, vertices);
		
#ifdef DEBUG
		std::cout << "Finished construction; ";
		std::cout << "created " << vertices.size() << " vertices, ";
		std::cout << "and " << voronoi.size() << " voronois" << std::endl;
#endif
	}
	
	void Geosphere::writeFile_Faces(std::string finm) const {
		Point const *p;
		int pwrit = 0;
		std::string nm;
		std::map<GeoVertex*,unsigned int> ixmap;
		GeoVoronoiVertex *voro;
		
		// open 2 files, one for vert coords, one for face indices
		nm = finm +"_"+ static_cast<std::ostringstream*>( &(std::ostringstream() << freq) )->str() +".xyz";
		std::ofstream ofs_v(nm.c_str());
		if (!ofs_v) {
			std::cout << "Can't open [" << nm << "]!" << std::endl;
			return;
		}
		std::cout << "Opened file [" << nm << "] for writing" << std::endl;
		ofs_v << "# File: " << nm << std::endl;
		ofs_v << "# Cartesian geodesic vertex coordinates" << std::endl;
		ofs_v << "# " << std::endl;
		nm = finm +"_"+  static_cast<std::ostringstream*>( &(std::ostringstream() << freq) )->str() + ".xyzf";
		std::ofstream ofs_i(nm.c_str());
		if (!ofs_i) {
			std::cout << "Can't open [" << nm << "]!" << std::endl;
			return;
		}
		std::cout << "Opened file [" << nm << "] for writing" << std::endl;
		ofs_i << "# File: " << nm << std::endl;
		ofs_i << "# Geodesic face vertex indices" << std::endl;
		ofs_i << "# " << std::endl;
		
		// write the vertex coordinates
		for (unsigned int i=0; i<vertices.size(); i++) {
			ixmap[vertices[i]] = i;
			p = vertices[i]->getPoint();
			ofs_v << p->x << " " << p->y << " " << p->z << std::endl;
			pwrit++;
		}
		ofs_v.close();
		std::cout << "Wrote " << pwrit << " vertices to file" << std::endl;
		
		pwrit = 0;
		for (unsigned int i=0; i<voronoi.size(); i++) {
			voro = voronoi[i];
			ofs_i << ixmap[voro->ev1] << " " << ixmap[voro->ev2] << " " << ixmap[voro->ev3] << std::endl;
			pwrit++;
		}
		ofs_i.close();
		std::cout << "Wrote " << pwrit << " indices to file" << std::endl;
	}
	
	void Geosphere::stuffBasicFaces(std::vector<GeoFace*> &faces) {
		// these are the faces joined at apex
		faces.push_back(new GeoFace(vertices[0], vertices[1], vertices[2])); // 0
		faces.push_back(new GeoFace(vertices[0], vertices[2], vertices[3])); // 1
		faces.push_back(new GeoFace(vertices[0], vertices[3], vertices[4])); // 2
		faces.push_back(new GeoFace(vertices[0], vertices[4], vertices[5])); // 3
		faces.push_back(new GeoFace(vertices[0], vertices[5], vertices[1])); // 4
		// these are the faces around top-middle
		faces.push_back(new GeoFace(vertices[1], vertices[6], vertices[2])); // 5
		faces.push_back(new GeoFace(vertices[2], vertices[7], vertices[3])); // 6
		faces.push_back(new GeoFace(vertices[3], vertices[8], vertices[4])); // 7
		faces.push_back(new GeoFace(vertices[4], vertices[9], vertices[5])); // 8
		faces.push_back(new GeoFace(vertices[5], vertices[10], vertices[1])); // 9
		// these are the faces around bottom-middle
		faces.push_back(new GeoFace(vertices[6], vertices[2], vertices[7])); // 10
		faces.push_back(new GeoFace(vertices[7], vertices[3], vertices[8])); // 11
		faces.push_back(new GeoFace(vertices[8], vertices[4], vertices[9])); // 12
		faces.push_back(new GeoFace(vertices[9], vertices[5], vertices[10])); // 13
		faces.push_back(new GeoFace(vertices[10], vertices[1], vertices[6])); // 14
		// these are the faces joined at nadir
		faces.push_back(new GeoFace(vertices[6], vertices[11], vertices[7])); // 15
		faces.push_back(new GeoFace(vertices[7], vertices[11], vertices[8])); // 16
		faces.push_back(new GeoFace(vertices[8], vertices[11], vertices[9])); // 17
		faces.push_back(new GeoFace(vertices[9], vertices[11], vertices[10])); // 18
		faces.push_back(new GeoFace(vertices[10], vertices[11], vertices[6])); // 19
		fillAdjacents(faces);
	}
	
};

