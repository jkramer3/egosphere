/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef FEATURE_EXTERNAL_H_
#define FEATURE_EXTERNAL_H_

#include <egosphere/ego_search.h>
#include <egosphere/ego_vertex.h>
#include <vector>

namespace egosphere_demo {
	
class FeatureExternal {
	public:
		FeatureExternal(int f);
		~FeatureExternal();
		
		/** Return the egosphere frequency. */
		int getFrequency() const;
		
		/** Return the minimum anglular distance between any two vertices. */
		double getMinAngle(bool inDegrees=false) const;
		
		/** Return the maximum anglular distance between any two vertices. */
		double getMaxAngle(bool inDegrees=false) const;
		
		/** Return the closest vertex to the given \c Biangle. */
		//egosphere::EgoVertex* getNearest(egosphere::Biangle& b);
		
		/** Return the vector of egosphere vertices. */
		std::vector<egosphere::EgoVertex*> getVerts() const;
		
		/** Return the \c EgoSearch object. */
		egosphere::EgoSearch getSearch() const;
	
	private:
		int m_freq;
		double m_minAngle, m_maxAngle;
		
		/** Egosphere structure; a vector of vertices. */
		std::vector<egosphere::EgoVertex*> m_vertices;
		
		/** Structure that allows fast searching for the nearest vertex to an
		 * arbitrary point by spatial index (either spherical or 3d Cartesian
		 * coordinates). */
		egosphere::EgoSearch m_egosearch;
		
};

};
#endif

