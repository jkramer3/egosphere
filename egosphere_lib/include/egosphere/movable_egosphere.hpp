/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef MOVABLE_EGOSPHERE_H_
#define MOVABLE_EGOSPHERE_H_

#include <egosphere/ego_search.h>
#include <egosphere/ego_vertex.h>
#include <map>
#include <unordered_set>
#include <vector>

namespace egosphere_demo {

typedef std::unordered_set<std::string> IdSet;
typedef std::vector<std::string> IdVec;
class Impression; // forward declaration
class MovableEgosphereNode; // friend class

class MovableEgosphere {
	public:
		friend class MovableEgosphereNode;
		MovableEgosphere(int f);
		~MovableEgosphere();
		
		/** Return the egosphere frequency. */
		int getFrequency() const;
		
		/** Return the minimum anglular distance between any two vertices. */
		double getMinAngle(bool inDegrees=false) const;
		
		/** Return the maximum anglular distance between any two vertices. */
		double getMaxAngle(bool inDegrees=false) const;
		
		/** Return the ID of the vertex closest to the given \c Biangle. */
		unsigned int getNearest(const egosphere::Biangle& b) const;
		
		/** Return the ID of the vertex closest to the given \c Biangle. */
		unsigned int getNearest(egosphere::Biangle const *b) const;
		
		/** Return a vector of \c Impression IDs. */
		IdVec getImpressionIds() const;
		
		/** Return a vector of \c Impression IDs for the given vertex ID. */
		IdVec getImpressionIds(unsigned int vid) const;
		
		/** Return the vertex ID associated with the given \c Impression ID. */
		unsigned int getVertexId(const std::string& iid) const;
		
		std::vector<egosphere::Point> getNeighborPoints(
		                                          const std::string& iid) const;
		
	private:
		int m_freq;
		double m_minAngle, m_maxAngle;
		std::vector<egosphere::EgoVertex*> m_vertices;
		egosphere::EgoSearch m_egosearch;
		/** Map of impression IDs to Impression objects. */
		std::map<std::string, Impression> m_impMap;
		/** Vector of sets of Impression IDs, where index is equivalent to
		 * a vertex ID. */
		std::vector<IdSet> m_vertImpIdsVec;
		
		// TODO: should these be public? why not?
		void addImpression(const std::string& iid,
		                   const egosphere::StampedPoint& impLocal);
		void updateImpression(const std::string& iid,
		                      const egosphere::StampedPoint& impLocal);
		Impression delImpression(const std::string& iid);
		egosphere::Point getImpressionPosition(const std::string& iid) const;
		
		/* use 'friendship' rather than methods
		std::vector<egosphere::EgoVertex*> getVerts() const;
		egosphere::EgoSearch getSearch() const;
		*/
	
};

};
#endif

