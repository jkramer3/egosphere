/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef EGO_PATH_H
#define EGO_PATH_H

#include <list>
#include <vector>

namespace egosphere {
	
	class StampedBiangle;
	class Time;
	
	/** A container of \c StampedBiangle objects that represent a path
	 * on the surface of a unit sphere (not necessarily an \a egosphere).
	 * This is mostly a convenience to provide a standard API, as it is
	 * essentially a wrapper around a C++ STL \c list. */
	class EgoPath {
		public:
			EgoPath();
			EgoPath(std::list<StampedBiangle> steps);
			EgoPath(const EgoPath& other)
					: m_path(other.m_path) { }
			friend void swap(EgoPath& first, EgoPath& second) {
				using std::swap;
				swap(first.m_path, second.m_path);
			}
			EgoPath& operator=(EgoPath other) {
				swap(*this, other);
				return *this;
			}
			
			std::list<StampedBiangle>::iterator begin();
			std::list<StampedBiangle>::const_iterator begin() const;
			std::list<StampedBiangle>::iterator end();
			std::list<StampedBiangle>::const_iterator end() const;
			bool empty() const;
			unsigned int size() const;
			const StampedBiangle& peekFront() const;
			const StampedBiangle& peekBack() const;
			void clear();
			StampedBiangle popFront();
			int popFront(int n);
			StampedBiangle popBack();
			int popBack(int n);
			void pushFront(const StampedBiangle &b);
			void pushFront(const std::list<StampedBiangle> &l);
			void pushBack(const StampedBiangle &b);
			void pushBack(const std::list<StampedBiangle> &l);
			// TODO: not sure I want to allow arbitrary insert/erase
			//void insert(unsigned int pos, const StampedBiangle &b);
			//void erase(unsigned int pos);
			
			Time timespan();
			void toVector(std::vector<StampedBiangle> &v) const;
			
		private:
			std::list<StampedBiangle> m_path;
			
	};
	
};
#endif

