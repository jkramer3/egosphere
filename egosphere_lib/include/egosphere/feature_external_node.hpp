/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef FEATURE_EXTERNAL_NODE_H
#define FEATURE_EXTERNAL_NODE_H

#include <ros/ros.h>
#include <egosphere/feature_external.hpp>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <std_msgs/String.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <map>

/** The architecture seems to be breaking down into 3 conceptual items:
 * 1. An egosphere (vector of vertices and related search)
 * 2. Representation of external-to-egosphere items
 * 3. A container that stores/maintains externals/egosphere relations
 * This class is the ROS front-end for (3), with the underlying, non-ROS
 * code in the 'FeatureExternal' class. */
namespace egosphere_demo {
	
	typedef geometry_msgs::Pose Pose;
	typedef geometry_msgs::PoseStamped PoseStamped;
	typedef visualization_msgs::Marker Marker;
	typedef visualization_msgs::MarkerArray MarkerArray;
	
	namespace ext {
		// TODO: stand-in for 'external' data; this data should be in its
		// own class. The intent is to store and maintain data about items
		// of interest that are external to an egosphere, akin to NASA's
		// 'SES events', which could be physical objects, sensory impressions,
		// or something else. At this point, while figuring out appropriate
		// attributes, organization, and representation, assume physical
		// entities with 3 visual traits:
		// 1. REP, a representation of the item itself (out in 3d space)
		// 2. BI, some representation of the ray connecting the egosphere
		//    center to the item (possibly just a sprite on the egosphere
		//    surface)
		// 3. FACE, a highlight marking of the egosphere vertex associated
		//    with the item.
		// The 3 traits are stored as Markers in a MarkerArray, using the
		// indices specified in the InfoIx enumeration. */
		enum InfoIx { REP, BI, ARROW, FACE };
		enum InfoStat { UNKNOWN, ADD, READY, UPDATE, REMOVE, DELETED };
		/** Information (i.e., Markers) related to an item external to an
		 * egosphere. */
		class ExternalInfo {
			// TODO: how do I handle an external that has disappeared?
			public:
				ExternalInfo(const std::string &egoFrame,
				             const std::string &extFrame);
				~ExternalInfo();
				void updateMarkers(const geometry_msgs::Pose &repPose,
				                   const geometry_msgs::Pose &biPose,
				                   const geometry_msgs::Pose &facePose,
				                   const std::vector<geometry_msgs::Point> &facePoints);
				const InfoStat getStatus() const;
				const MarkerArray& getMarkerArr() const;
				const std::string& getFrame() const;
				const geometry_msgs::Pose& getPose() const;
			private:
				// because pub & updates coordinate from timers, no mutex needed
				//std::mutex lock;
				InfoStat status;
				MarkerArray externals;
				void addExtMarker(const std::string &egoFrame,
				                  const std::string &extFrame,
				                  const geometry_msgs::Pose &pose);
				void addEgoMarkers(const std::string &egoFrame,
				                   const std::string &extFrame);
		};
	}
	
	/** ROS interface for \c FeatureExternal, including RViz functionality. */
	class FeatureExternalNode {
		public:
			FeatureExternalNode(ros::NodeHandle& nh);
			~FeatureExternalNode();
		
		private:
			// **********************************
			// *** infrastructure members/methods
			// **********************************
			ros::NodeHandle m_nh;            // ROS nodehandle
			std::string m_name;              // ROS node name
			ros::Timer m_timerPub;           // publishing timer
			ros::Duration m_timerPubRate;    // publishing rate
			ros::Timer m_timerTf;            // tf broadcast/lookup timer
			ros::Duration m_timerTfRate;     // tf broadcast/lookup rate
			void showConfig() const;         // loading config spew
			void pubTimer(const ros::TimerEvent &te) const; // publish event
			void tfTimer(const ros::TimerEvent &te) const;  // tf event
			
			// **********************************
			// *** egosphere members/methods
			// **********************************
			FeatureExternal *m_featExternal; // the egosphere
			std::string m_center, m_ground;  // egosphere center/global frames
			double m_radius;                 // egosphere display radius
			MarkerArray    m_egoVertMarkers; // egosphere vertex markers
			ros::Publisher m_egoVertMarkers_pub;    // egosphere marker publisher
			bool           m_egoVertMarkersPublish; // egosphere marker pub control
			void pubEgoVerts() const;        // egosphere marker publish method
			
			// ***********************************
			// *** "External" data members/methods
			// ***********************************
			std::map<std::string, ext::ExternalInfo> m_externals;
			std::shared_ptr<tf::TransformListener> m_tfliPtr;
			ros::Subscriber m_externalsAddFrame_sub;
			ros::Publisher m_externals_pub;
			bool m_externalsPublish;
			/** Callback to add a frame without corresponding broadcasting. */
			void addExternalFrameCb(const std_msgs::String::ConstPtr &msg);
			/** Adds a frame relative to egosphere. */
			bool addExternal(const std::string &frame);
			bool removeExternal(const std::string &frame);
			void updateExternals() const;
			void pubExternals() const;       // 
			
			// ***********************************
			// *** TF input/output members/methods
			// ***********************************
			// note: listener IS used to determine/set broadcast data; however,
			//   in actual setup, broadcast expected to be handled externally
			//std::shared_ptr<tf::TransformListener> m_tfliPtr;
			std::map<std::string, tf::Transform> m_extTfs;
			std::shared_ptr<tf::TransformBroadcaster> m_tfbrPtr;
			ros::Subscriber m_externalsAdd_sub;
			ros::Subscriber m_externalsPointSet_sub;
			ros::Subscriber m_externalsPoseSet_sub;
			ros::Subscriber m_externalsRemove_sub;
			/** Callback to add a frame WITH corresponding broadcasting. */
			void addExternalCb(const std_msgs::String::ConstPtr &msg);
			/** Adds a frame for broadcasting. */
			void addExternalTf(const std::string &extFrame);
			void removeExternalTfCb(const std_msgs::String::ConstPtr &msg);
			void setExternalTfPointCb(const geometry_msgs::PointStamped::ConstPtr &msg);
			void setExternalTfPoseCb(const geometry_msgs::PoseStamped::ConstPtr &msg);
			void broadcast() const;
			void updateExternalTf(const std::string &extFrame,
			                    const geometry_msgs::Point &point);
			void updateExternalTf(const std::string &extFrame,
			                    const geometry_msgs::Pose &pose);
			
			// ***********************************
			// *** utility methods Tf/egosphere
			// ***********************************
			geometry_msgs::Point getPointViaTf(
			                           std::shared_ptr<tf::TransformListener> tf,
			                           const std::string &target,
			                           const std::string &source) const;
			geometry_msgs::Pose getPoseViaTf(
			                           std::shared_ptr<tf::TransformListener> tf,
			                           const std::string &target,
			                           const std::string &source) const;
			/** Get the egosphere surface point corresponding to the provided
			 * pose (that is already relative to egosphere center). */
			geometry_msgs::Pose getPoseRadius(double radius,
			                           const geometry_msgs::Pose &extPose) const;
			/** Get the egosphere face point corresponding to the provided
			 * pose (that is already relative to egosphere center). */
			geometry_msgs::Pose getPoseFace(const FeatureExternal *ego,
			                           const geometry_msgs::Pose &extPose) const;
			std::string tfPointStr(const tf::Point &tfPoint) const;
	};

};
#endif

