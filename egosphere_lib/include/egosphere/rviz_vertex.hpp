/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef RVIZ_VERTEX_H
#define RVIZ_VERTEX_H

#include <egosphere/egosphere_utils.h>
#include <egosphere/ego_vertex.h>
#include <visualization_msgs/Marker.h>

namespace egosphere {
	
	namespace marker_type {
		enum MarkerType { NONE, FACE, SPHERE };
	}
	
	/** An egosphere vertex that incorporates RViz visualization
	 * functionality. */
	class RVizVertex : public EgoVertex {
	public:
		RVizVertex();
		RVizVertex(Biangle& b);
		RVizVertex(Point& p);
		~RVizVertex();
		
		void stuffMarker(visualization_msgs::Marker &m, double radius=1.0,
		                 marker_type::MarkerType t = marker_type::FACE);
		std::string str() const;
		
	private:
		void stuffFace(visualization_msgs::Marker &m, double radius);
		void stuffSphere(visualization_msgs::Marker &m, double radius);
	};

};
#endif

