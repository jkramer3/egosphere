/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef GEODESIC_SEARCH_H
#define GEODESIC_SEARCH_H

#include <egosphere/ego_search.h>
#include <egosphere/ego_vertex.h>
#include <egosphere/geodesic_metainfo.h>
#include <egosphere/geodesic_vertex.h>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <map>

/** Structures and functions to enable search on the egosphere (i.e., the
 * point location problem). The search structure is a data cube organized
 * such that given two angles \a (theta, phi) as input (representing the
 * \a azimuth and \a altiude of some object or occurrence relative to the
 * center of the egosphere), the output is a pointer to the nearest
 * geodesic vertex.
 *
 * In the structure, \a x and \a y are integer indices of \a angle
 * intervals, where the total angle range encompassed by \a x intervals
 * is \a -PI<theta<=PI, total angle range encompassed by \a y intervals
 * is \a -PI/2<phi<=PI/2, and each individual interval is the maximal
 * spherical distance between any two adjacent vertices. The \a z value is
 * an integer index into a (variable length) list of vertex pointers, where
 * each vertex in the list is possibly the shortest spherical distance away
 * from an arbitrary point in the associated \a (theta,phi) interval.
 *
 * As originally written, this structure used variable-sized theta and phi
 * angle intervals based on the borders of vertices' \a voronoi regions
 * calculated during geodesic construction. Determination of the small set
 * of vertices \a near an arbitrary point was performed via two binary
 * searches, one for \a theta followed by one for \a phi. With the
 * appropriate set of vertices found, the spherical distance to each
 * vertex in the set was calculated, yielding the nearest vertex to the
 * given point on the surface of the sphere.
 *
 * Intervals are now of a fixed size (the maximal spherical distance
 * between two adjacent vertices), which allows a simple calculation
 * (index = integer quotient of ((angle - range's lower bound) / interval)
 * minus 1) to replace the binary searches (but not the distance calculations
 * for each vertex in the located list). The pared-down \c EgoSearch class
 * uses the calculation method, while the \c GeoSearch still relies on
 * binary search. */
namespace egosphere {
	
	/** Angle interval object. */
	class AngleInterval {
	public:
		AngleInterval();
		AngleInterval(double l, double h);
		//bool overlap(AngleInterval& other) const;
		int in(double val) const;
		/** Return the results of \c str(2). */
		std::string str() const;
		/** Return a string representation of the angle interval. Returned
		 * value is in the form \c lo,hi using \c dec decimal places. */
		std::string str(int dec) const;
		double lo, hi;//, delta;
	};
	
	/** Phi interval object (for point location). */
	class PhiInter : public AngleInterval {
	public:
		PhiInter();
		PhiInter(double l, double h);
		PhiInter(double l, double h, GeoVertex *v);
		~PhiInter();
		int numVertex() const;
		void addVertex(GeoVertex const *ev);
		
		/** Find the nearest \c GeoVertex to the given \c Biangle. Note that the
		 * underlying algorithm is slightly different for a \c Point; while a
		 * \c Point has to be converted to a \c Biangle for searching (and thus
		 * requires the use of some trig and square root functions), the subsequent
		 * distance comparisons use multiplys rather than trig. While the minimum
		 * search times seem to be comparable, the average and maximum search times
		 * for a \c Point appear to be slightly faster. */
		GeoVertex const * nearest(Biangle& b) const;
		
		/** Find the nearest \c GeoVertex to the given \c Point. Note that the
		 * underlying algorithm is slightly different for a \c Biangle; while a
		 * \c Point has to be converted to a \c Biangle for searching (and thus
		 * requires the use of some trig and square root functions), the subsequent
		 * distance comparisons use multiplys rather than trig. While the minimum
		 * search times seem to be comparable, the average and maximum search times
		 * for a \c Point appear to be slightly faster. */
		GeoVertex const * nearest(Point& p) const;
		
		/** Fill \c vec with the \c EgoVertex* that are associated with the
		 * \c GeoVertex* in the \c verts vector. */
		void map2EgoSearch(std::map<const GeoVertex*, int>& gv2ev,
		                   std::map<Biangle,int>& vec);
		
		/** Return the results of \c str(2). */
		std::string str() const;
		
		/** Return a string representation of the interval and \c GeoVertex
		 * references it contains. Returned value shows \c dec decimal places
		 * and is in the form:
		 * \c (lo, hi) ->
		 *      0x0000000:(theta0, phi0)
		 *      ...
		 *      0x000000n:(thetan, phin) */
		std::string str(int dec) const;
		
	private:
		std::vector<const GeoVertex*> verts;
	};
	
	/** Theta interval object (for point location). */
	class ThetaInter : public AngleInterval {
	public:
		ThetaInter();
		ThetaInter(double l, double h);
		~ThetaInter();
		void makePhiIntervals(double& minPhi, double& maxPhi, double& span);
		void addVertex(double& minPhi, double& maxPhi, const GeoVertex* vert);
		unsigned int phiSize() const;
		void phiVertexStats(int (&arr)[3]);
		void sortPhi();
		int numVertex() const;
		GeoVertex const * findVertex(Biangle& b) const;
		GeoVertex const * findVertex(Point& p, Biangle& b) const;
		/** Fill \c vec2 with the \c EgoVertex* that are associated with the
		 * \c GeoVertex* in the \c gv2ev map. */
		void map2EgoSearch(std::map<const GeoVertex*, int>& gv2ev,
		                   std::vector< std::map<Biangle,int> >& vec2);
		/** Return the results of \c str(2). */
		std::string str() const;
		/** Return a string representation of the interval and the set of
		 * intervals comprising the second dimension of search. Returned value
		 * shows \c dec decimal places and is in the form:
		 * \c Theta interval: (lo, hi) with phi intervals:
		 *    <results of PhiInter::str(dec)> */
		std::string str(int dec) const;
	private:
		std::vector<PhiInter*> phiInterVec;
		bool sorted;
	};
	
	/** The point-location data structure. */
	class GeoSearch {
	public:
		GeoSearch(GeosphereMetainfo& mi, std::vector<GeoVertex*>& verts);
		~GeoSearch();
		
		/** Return a pointer to the egosphere vertex closest to the 2d
		 * \a (theta,phi) spherical coordinate \c Biangle \a b. Note that the
		 * underlying calculation is different than that with a \c Point
		 * parameter. */
		GeoVertex const * nearest(Biangle& b) const;
		
		/** Return a pointer to the egosphere vertex closest to the 3d
		 * \a (x,y,z) Cartesian coordinate \c Point \a p. Note that the
		 * underlying calculation is different than that with a \c Biangle
		 * parameter. */
		GeoVertex const * nearest(Point& p) const;
		
		/** Fill the 3d vector \c vec3 with the \c EgoVertex* associated with
		 * the \c GeoVertex* in the \c gv2ev map. The 3d vector will provide
		 * a \a point-location search structure where the first dimension is
		 * \a theta, the second dimension is \a phi, and the third dimension
		 * is \c EgoVertex*. */
		void map2EgoSearch(std::map<const GeoVertex*, int>& gv2ev,
		                   std::vector< std::vector< std::map<Biangle,int> > >& vec3);
		
		std::string str() const;
		std::string str(int dec) const;
		std::string sum_str() const;
		std::string sum_str(bool incInter, int dec) const;
	private:
		/** Create the vectors used for point-location searching. Each
		 * vector element represents an angle interval that is derived from
		 * the geosphere's meta-information (e.g., maximum distance between
		 * any vertices). */
		void initSearchVecs(GeosphereMetainfo& metainfo,
		                    std::vector<ThetaInter*>& thvec);
		
		/** Put the egosphere vertices into the search structure. */
		void fillSearchVecs(std::vector<GeoVertex*>& verts,
		                    double& dist, double& buf,
		                    std::vector<ThetaInter*>& thvec);
		
		/** Calculate the \a bounds (min/max of theta/phi angles) of an
		 * \c GeoVertex, putting the results in array \a arr. This function
		 * allows some extra manipulation and adjustments to the set of angle
		 * intervals in which a vertex is placed. */
		void getBounds(GeoVertex *ev, double& d, double& buf, double (&arr)[4]);
		
		// min/max/span values available in metainfo; ranges are:
		//      theta=2*M_PI, phi=M_PI
		//double minTheta, maxTheta; // mi.minTheta, mi.maxTheta
		//double minPhi, maxPhi;     // mi.minPhi, mi.maxPhi
		//double spanTheta, spanPhi; // mi.maxVertD, mi.maxVertD
		std::vector<ThetaInter*> interThetaVec;
		
	};
};

#endif
