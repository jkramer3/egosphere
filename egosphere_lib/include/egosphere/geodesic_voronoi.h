/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef GEODESIC_VORONOI_H
#define GEODESIC_VORONOI_H

#include <egosphere/egosphere_utils.h>
#include <egosphere/geodesic_vertex.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>

namespace egosphere {
	
	class GeoVoronoiVertex {
	public:
		GeoVoronoiVertex(GeoVertex *v1, GeoVertex *v2, GeoVertex *v3);
		~GeoVoronoiVertex();
		bool operator<(const GeoVoronoiVertex &other);
		std::string str() const;
		std::string vert_str() const;
		double minVertexDistance() const;
		double maxVertexDistance() const;
		GeoVertex* nearestGeoVertex(Biangle const *b);
	//private:
		Biangle *biangle, *m12, *m13, *m23;
		GeoVertex *ev1, *ev2, *ev3;
		bool wrap;
	};

	struct compareGeoVoronoiVertexPtrs {
		bool operator ()(GeoVoronoiVertex *lhs, GeoVoronoiVertex *rhs) {
			return *lhs < *rhs;
		}
	};
	
};

#endif
