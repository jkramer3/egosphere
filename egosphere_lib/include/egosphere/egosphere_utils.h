/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef EGOSPHERE_UTILS_H
#define EGOSPHERE_UTILS_H

//#include <geometry_msgs/Point.h>
//#include <geometry_msgs/Pose.h>
//#include <geometry_msgs/Quaternion.h>
#include <cmath>
#include <cstdio>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

/** Utility structures and functions. */
namespace egosphere {
	
	class EgoPath;
	
	/** Returns values of -1.0, 0.0, or 1.0 to represent the sign of the
	 * given double, interpreting -0.0 as negative. */
	inline
	double sign(double d) {
		if (d == 0.0) {
			return fabs(d) != d ? -1.0 : 1.0;
		}
		return d < 0.0 ? -1.0 : 1.0;
	}
	
	/** Return \a true if \c d>=0.0 and \a false otherwise. Note that
	 * \c d=-0.0 will recognize the negative and return false. */
	inline
	bool isPositive(double d) {
		return sign(d) < 0.0 ? true : false;
	}
	
	/** Return the value provided, but make zeros positive. */
	inline
	double unsignZero(double d) {
		return d != 0.0 ? d : fabs(0.0);
	}
	
	/** Convert degrees to radians. */
	inline
	double toRad(double deg) { return deg * M_PI / 180.0; }
	
	/** Convert radians to degrees. */
	inline
	double toDeg(double rad) { return rad * 180.0 / M_PI; }
	
	/** Normalize an angle -limit < ang <= limit. */
	inline
	double norm_ang(const double rad, const double limit) {
		const double wid = 2 * limit;
		const double off = rad + limit;
		const double dif = (off - (floor(off / wid) * wid));
		return (dif == 0 ? limit : dif-limit);
	}
	
	/** Normalize an angle -M_PI < ang <= M_PI. */
	inline
	double norm_ang(const double rad) {
		double ang = rad;
		while (ang > M_PI) ang -= (2*M_PI);
		while (ang <= -M_PI) ang += (2*M_PI);
		return ang;
	}
	
	/** Given two angles (\a angSrc and \a angDest, in radians), return the
	 * difference between them (where \c -PI < \a result <= \a PI). */
	inline
	double angleBetween(const double &angSrc, const double &angDest) {
		double angResult = norm_ang(angDest) - norm_ang(angSrc);
		angResult += (angResult>M_PI) ? -2*M_PI :
		                     (angResult<=-M_PI) ? 2*M_PI : 0.0 ;
		return angResult;
	}
	
	/** Check whether (radian) angles \c a1 and \c a2 are within
	 * \c rad radians of one another. */
	inline
	bool anglesWithin(const double& a1, const double& a2, double rad) {
		return fabs(angleBetween(a1, a2)) <= fabs(rad);
	}
	
	/** Check whether the (radian) angles \c a1 and \c a2 are aligned with
	 * radian angle \c offset, allowing \c rad slop. */
	inline
	bool anglesAligned(const double& a1, const double& a2,
	                  double offset, double rad) {
		double dang = angleBetween(a1, a2);
		bool align = dang >= (offset - rad) && dang <= (offset + rad);
		return align;
	}
	
	/** Time representation. */
	typedef struct Time {
		unsigned int sec, nsec;
		Time() { sec=nsec=0; }
		Time(unsigned int s, unsigned int n) { sec=s; nsec=n; }
		Time(double d) {
			sec = (unsigned int)floor(d);
			nsec = (unsigned int)round((d-sec) * 1e9);
		}
		friend void swap(Time& first, Time& second) {
			using std::swap;
			swap(first.sec, second.sec);
			swap(first.nsec, second.nsec);
		}
		Time& operator=(Time other) {
			swap(*this, other);
			return *this;
		}
		Time& operator+=(const Time &rhs) {
			unsigned long ssum = (unsigned long)(this->sec) +
			                     (unsigned long)(rhs.sec);
			unsigned long nsum = (unsigned long)(this->nsec) +
			                     (unsigned long)(rhs.nsec);
			this->sec = (unsigned int)ssum; this->nsec = (unsigned int)nsum;
			return *this;
		}
		Time& operator-=(const Time &rhs) {
			this->sec -= rhs.sec; this->nsec -= rhs.nsec;
			return *this;
		}
		bool operator==(const Time &rhs) const {
			return sec == rhs.sec && nsec == rhs.nsec;
		}
		bool operator!=(const Time &rhs) const {
			//return sec != rhs.sec || nsec != rhs.nsec;
			return !(*this == rhs);
		}
		bool operator<(const Time &rhs) const {
			if (sec < rhs.sec) return true;
			return (sec == rhs.sec && nsec < rhs.nsec ? true : false);
		}
		bool operator>(const Time &rhs) const {
			if (sec > rhs.sec) return true;
			return (sec == rhs.sec && nsec > rhs.nsec ? true : false);
		}
		bool operator<=(const Time &rhs) const {
			if (sec < rhs.sec) return true;
			return (sec == rhs.sec && nsec <= rhs.nsec ? true : false);
		}
		bool operator>=(const Time &rhs) const {
			if (sec > rhs.sec) return true;
			return (sec == rhs.sec && nsec >= rhs.nsec ? true : false);
		}
		double toDouble() const {
			return (double)sec + 1e-9*(double)nsec;
		}
		std::string str() const {
			std::ostringstream oss;
			oss << "[" << sec << ":" << nsec << "]";
			return oss.str();
		}
	} Time;
	inline Time operator+(Time lhs, const Time &rhs) {
		lhs += rhs;
		return lhs;
	}
	inline Time operator-(Time lhs, const Time &rhs) {
		lhs -= rhs;
		return lhs;
	}
	
	/** Cartesian 3d point. */
	typedef struct Point {
		double x, y, z;
		Point() { x=y=z=0.0; }
		Point(double x1, double y1, double z1) : x(x1), y(y1), z(z1) { }
		Point(const Point& other) :
				x(other.x), y(other.y), z(other.z) { }
		friend void swap(Point& first, Point& second) {
			using std::swap;
			swap(first.x, second.x);
			swap(first.y, second.y);
			swap(first.z, second.z);
		}
		Point& operator=(Point other) {
			swap(*this, other);
			return *this;
		}
		bool operator==(const Point &rhs) const {
			double dx, dy, dz, EPS=2.0e-8;
			dx = fabs(x - rhs.x);
			dy = fabs(y - rhs.y);
			dz = fabs(z - rhs.z);
			return dx <= EPS && dy <= EPS && dz <= EPS;
		}
		bool operator!=(const Point &rhs) const {
			return !(*this == rhs);
		}
		Point& operator+=(const Point &rhs) {
			this->x += rhs.x; this->y += rhs.y; this->z += rhs.z;
			return *this;
		}
		Point& operator-=(const Point &rhs) {
			this->x -= rhs.x; this->y -= rhs.y; this->z -= rhs.z;
			return *this;
		}
		void invalidate() {
			x = NAN; y = NAN; z = NAN;
		}
		bool invalid() const { return !(x==x && y==y && z==z); }
		std::string str() const { return str(2); }
		std::string str(int dec) const {
			std::ostringstream oss;
			oss << std::fixed << std::setprecision(dec);
			oss << x << ", " << y << ", " << z;
			return oss.str();
		}
	} Point;
	inline Point operator+(Point lhs, const Point &rhs) {
		lhs += rhs;
		return lhs;
	}
	inline Point operator-(Point lhs, const Point &rhs) {
		lhs -= rhs;
		return lhs;
	}
	
	/** Cartesian 3d point with time stamp. */
	typedef struct StampedPoint : public Point {
		Time t;
		StampedPoint() { }
		StampedPoint(double x1, double y1, double z1) : Point(x1, y1, z1) { }
		StampedPoint(const Point& other) : Point(other) { }
		StampedPoint(const StampedPoint& other) : Point(other), t(other.t) { }
		friend void swap(StampedPoint& first, StampedPoint& second) {
			using std::swap;
			swap(static_cast<Point&>(first), static_cast<Point&>(second));
			swap(first.t, second.t);
		}
		StampedPoint& operator=(StampedPoint other) {
			swap(*this, other);
			return *this;
		}
		std::string str() const { return str(2); }
		std::string str(int dec) const {
			std::ostringstream oss;
			oss << t.str() << " ";
			oss << "(" << Point::str(dec) << ")";
			return oss.str();
		}
	} StampedPoint;
	
	/** Spherical coordinate object (theta, phi). Note that the sin and cos
	 * of the angles is also stored to eliminate repeated calculation (as it
	 * is expected the values will be used often). */
	typedef struct Biangle {
		double theta, phi, mag;
		double costh, sinth, cosph, sinph;
		Biangle() {
			theta=0.0; phi=0.0; mag=1.0;
			costh=1.0; sinth=0.0;
			cosph=1.0; sinph=0.0;
		}
		Biangle(double t, double p, bool norm=true) {
			init_biangle(t, p, 1.0, norm);
		}
		Biangle(double t, double p, double m, bool norm=true) {
			init_biangle(t, p, m, norm);
		}
		Biangle(const Biangle& oth) :
				theta(oth.theta), phi(oth.phi), mag(oth.mag),
				costh(oth.costh), sinth(oth.sinth),
				cosph(oth.cosph), sinph(oth.sinph)
		{ }
		friend void swap(Biangle& first, Biangle& second) {
			using std::swap;
			swap(first.theta, second.theta);
			swap(first.phi, second.phi);
			swap(first.mag, second.mag);
			swap(first.costh, second.costh);
			swap(first.sinth, second.sinth);
			swap(first.cosph, second.cosph);
			swap(first.sinph, second.sinph);
		}
		Biangle& operator=(Biangle other) {
			swap(*this, other);
			return *this;
		}
		bool operator==(const Biangle &rhs) const {
			double dt, dp, EPS=2.0e-8;
			dt = fabs(angleBetween(theta, rhs.theta));
			dp = fabs(angleBetween(phi, rhs.phi));
			return dt <= EPS && dp <= EPS;
		}
		bool operator!=(const Biangle &rhs) const {
			return !(*this == rhs);
		}
		Biangle& operator+=(const Biangle &rhs) {
			double t = this->theta + rhs.theta;
			double p = this->phi + rhs.phi;
			init_biangle(t, p, this->mag, true);
			return *this;
		}
		Biangle& operator-=(const Biangle &rhs) {
			double t = this->theta - rhs.theta;
			double p = this->phi - rhs.phi;
			init_biangle(t, p, this->mag, true);
			return *this;
		}
		bool operator<(const Biangle &other) const {
			if (theta != other.theta) {
				return theta < other.theta;
			}
			return phi < other.phi;
		}
		void invalidate() {
			theta = NAN; phi = NAN; mag = NAN;
			costh = NAN; sinth = NAN;
			cosph = NAN; sinph = NAN;
		}
		bool invalid() const { return !(theta==theta && phi==phi); }
		std::string str() const { return str(2); }
		std::string str(int dec) const {
			std::ostringstream oss;
			oss << std::fixed << std::setprecision(dec);
			oss << "t=" << toDeg(theta) << ", ";
			oss << "p=" << toDeg(phi) << ", ";
			oss << "m=" << mag;
			return oss.str();
		}
		private:
		void init_biangle(double t, double p, double m, bool norm) {
			// NOTE: normalizing angle breaks the search structure, as
			// the bounds need to exceed the usual limits
			if (norm) {
				theta=norm_ang(t); phi=norm_ang(p, M_PI/2);
			} else {
				theta=t; phi=p;
			}
			costh=cos(theta); sinth=sin(theta);
			cosph=cos(phi); sinph=sin(phi);
			mag=m;
		}
	} Biangle;
	inline Biangle operator+(Biangle lhs, const Biangle &rhs) {
		lhs += rhs;
		return lhs;
	}
	inline Biangle operator-(Biangle lhs, const Biangle &rhs) {
		lhs -= rhs;
		return lhs;
	}
	
	/** Spherical coordinate object (theta, phi) with time stamp. */
	typedef struct StampedBiangle : public Biangle {
		Time t;
		StampedBiangle() { }
		StampedBiangle(double t, double p, bool norm=true)
				: Biangle(t, p, norm) { }
		StampedBiangle(double t, double p, double m, bool norm=true)
				: Biangle(t, p, m, norm) { }
		StampedBiangle(const Biangle& other) : Biangle(other) { }
		StampedBiangle(const StampedBiangle& other)
				: Biangle(other), t(other.t) { }
		friend void swap(StampedBiangle& first, StampedBiangle& second) {
			using std::swap;
			swap(static_cast<Biangle&>(first), static_cast<Biangle&>(second));
			swap(first.t, second.t);
		}
		StampedBiangle& operator=(StampedBiangle other) {
			swap(*this, other);
			return *this;
		}
		std::string str() const { return str(2); }
		std::string str(int dec) const {
			std::ostringstream oss;
			oss << t.str() << " ";
			oss << "(" << Biangle::str(dec) << ")";
			return oss.str();
		}
	} StampedBiangle;
	
	/** Comparison method that can be used with 'std::sort'. Specifically,
	 * this comparison relies on the \c Biangle's \c < operator, which is
	 * used in setting up the \c EgoSearch object. Another type of sort
	 * is to use the \c bearing value to obtain a (counter-)clockwise
	 * sort around a specific vertex, as is done in ordering a vertex's
	 * neighbors. */
	struct compareBiangles {
		bool operator ()(Biangle &lhs, Biangle &rhs) {
			return lhs < rhs;
		}
	};
	
	/** Calculate the hypotenuse of a triangle with legs \a x and \a y. */
	inline
	double hypot(double x, double y) {
		return sqrt((x*x) + (y*y));
	}
	
	/** Calculate the squared distance between two 3d points. */
	inline
	double distance_sq(Point& p1, const Point& p2) {
		return pow(p1.x-p2.x, 2) + pow(p1.y-p2.y, 2) + pow(p1.z-p2.z, 2);
	}
	
	/** Calculate the distance between the origin (0,0,0) and a 3d point. */
	inline
	double distance(Point& p) {
		return sqrt(pow(p.x, 2) + pow(p.y, 2) + pow(p.z, 2));
	}
	
	/** Calculate the (Euclidean) distance between two 3d points. */
	inline
	double distance(Point& p1, Point& p2) {
		return sqrt(pow(p1.x-p2.x, 2) + pow(p1.y-p2.y, 2) + pow(p1.z-p2.z, 2));
	}
	
	/** Calculate the distance (on the great circle) between two Biangles
	 * (0 <= return value <= M_PI). */
	inline
	double hypot(const Biangle &b1, const Biangle &b2) {
		// TODO: could use Haversine for very-near biangles...
		// calculations performed using the spherical law of cosines rather
		// than Haversine formula; simpler, less computation, and should be
		// well-conditioned for angle differences greater than ~0.5 degrees,
		// which should be better than we need. Leaving the haversine alg
		// here if its ever necessary...
		// haversine
		//double dth, dph, dth_ele, dph_ele, a;
		//dth = angleBetween(b1.theta, b2.theta);
		//dph = angleBetween(b1.phi, b2.phi);
		//dth_ele = sin(dth/2);
		//dph_ele = sin(dph/2);
		//a = dph_ele * dph_ele +
		//    dth_ele * dth_ele *
		//    cos(b1.phi) * cos(b2.phi);
		//return 2 * atan2(sqrt(a), sqrt(1-a));
		//
		// spherical law of cosines (assume r=1)
		//std::cout << "utils::hypot: b1=[" << b1.str() << "]";
		//std::cout << ", b2=[" << b2.str() << "]" << std::endl;
		return acos((b1.sinph * b2.sinph) +
		            (b1.cosph * b2.cosph) *
		            cos(angleBetween(b1.theta, b2.theta)));
	}
	
	class Utils {
	public:
		/** Calculate the euclidean distance of two \c Biangles separated by
		 * angular distance \c angD at distance \c radius. */
		static
		double distance(double angD, double radius);
		
		/** Calculate the distance between the origin (0,0,0) and the endpoint
		 * of a \c Biangle at distance \c radius. */
		static
		double distance(const Biangle& b, double radius);
		
		/** Calculate the (Euclidean) distance between the endpoints of two
		 * Biangles at the specified \c radius (distance from center). */
		static
		double distance(const Biangle& b1, Biangle& b2, double radius);
		
		/** Calculate the Cartesian 3d from spherical coordinate. */
		static
		void calcPoint(const Biangle& src, Point& dst, double radius=1.0, bool wMag=true);
		
		/** Calculate the Cartesian 3d from spherical coordinate. */
		static
		Point* calcPoint(Biangle const *b, double radius=1.0);
		
		/** Calculate the spherical from Cartesian 3d coordinate. */
		static
		void calcBiangle(const Point& src, Biangle& dst);
		
		/** Calculate the spherical from Cartesian 3d coordinate. */
		static
		Biangle* calcBiangle(Point *p);
		
		/** Calculate whether all coordinates of \a Points \c p1 and
		 * \c p2 are within \c d length along their respective axes.
		 * Note that the length \c d is NOT the 3d Eculidian distance,
		 * but rather half the side length(s) of a bounding box around
		 * \a Point \c p2. */
		static
		bool within(Point const *p1, Point const *p2, double d);
		
		/** Calculate whether \a theta and \a phi values of \a Biangles
		 * \c b1 and \c b2 are within \c rad radians of each other. Note
		 * that \c rad is NOT the great-circle distance on a sphere's
		 * surface, but rather half the angle between them. */
		static
		bool within(Biangle const *b1, Biangle const *b2, double rad);
		
		/** Check whether \c Biangles \a b1 and \a b2 are aligned with
		 * \c theta \a thoffset and \c phi \a phoffset, allowing \c theta
		 * \a thdiff and \c phi \a phdiff slop. */
		static
		bool aligned(Biangle const *b1, Biangle const *b2,
		             double thoffset, double thdiff,
		             double phoffset, double phdiff);
		
		/** Calculate the \a midpoint between two \c Biangles. Calls
		 * \c fracpoint with a fraction of 0.5. */
		static
		Biangle* midpoint(Biangle const *b1, Biangle const *b2);
		
		/** Calculate a point \a frac percent between two \c Biangles.
		 * Implementation of 'Intermediate points on a great circle' from
		 * http://williams.best.vwh.net/avform.htm. Note that midpoint of
		 * antipodes is supposed to be undefined (as there are two
		 * solutions), but this method will simply return one of them. */
		static
		Biangle* fracpoint(Biangle const *b1, Biangle const *b2, double frac);
		
		/** Calculate the bearing from \c b1 to \c b2; use the boolean \c type
		 * parameter to choose between the \a initial or \a final bearing
		 * \c true and \c false, respectively. Return value is in the range
		 * -M_PI < d < M_PI (result of \c atan2 ). Formula found at
		 * http://www.movable-type.co.uk/scripts/latlong.html. See the
		 * \c course function, which I believe does the same thing in a
		 * different way. */
		static
		double bearing(Biangle const *b1, Biangle const *b2, bool type=true);
		
		/** Calculate the point a given distance from an initial point at a
		 * particular \a bearing. Implementation of 'Lat/lon given radial and
		 * distance' from http://williams.best.vwh.net/avform.htm. */
		static
		Biangle* destpoint(Biangle const *b, double ad, double bear);
		
		/** Calculate the 'cross-track distance' of a \c Biangle relative to
		 * a great-circle path defined by two other \c Biangles. Return a
		 * value that is: \a negative if the \c Biangle is counter-clockwise
		 * to the line, \a zero if the point is on the line, or \c positive
		 * if the point is clockwise to the line. Note that this does the
		 * same calculation as \c direction, using the \c bearing function
		 * rather than \c course. */
		static
		double xtrack(Biangle const *ep1, Biangle const *ep2, Biangle const *b);
		
		/** Calculate the 'course between points' (from \c b1 to \c b2). Formula
		 * found at http://williams.best.vwh.net/avform.htm. See the \c bearing
		 * function, which I believe does the same thing in a different way. */
		static
		double course(Biangle const *b1, Biangle const *b2);
		
		/** Return a double indicating the \a direction of a \c Biangle relative
		 * to a line defined by two other \c Biangles. This is an interpretation
		 * of 'Cross track error' from http://williams.best.vwh.net/avform.htm
		 * (requires the 'course' function). Return a value that is: \a negative
		 * if the \c Biangle is counter-clockwise to the line, \a zero if the
		 * point is on the line, or \c positive if the point is clockwise to the
		 * line.  Note that this does the same calculation as \c xtrack, using
		 * the \c course function rather than \c bearing. */
		static
		double direction(Biangle const *ep1, Biangle const *ep2, Biangle const *b);
		
		/** Calculate the \c Biangle intersection of 'lines' \c b1->b2
		 * and \c b3->b4 by converting to 3d Cartesian coordinates and
		 * back again. If the 'lines' do not intersect, the returned
		 * \c Biangle* will be 0. */
		static
		Biangle* intersection(Biangle const *b1, Biangle const *b2,
		                      Biangle const *b3, Biangle const *b4);
		
		/** Calculate a sequence of \c Biangle locations on the great circle
		 * between \c Biangle \a b1 and \a b2 spaced \a at \a most \a dist
		 * apart. Note that the actual distance between steps may be less
		 * than \a dist to obtain even spacing. Because it is expected that
		 * paths will often be added to the end of other paths, the returned
		 * sequence excludes \a b1 and includes \a b2. */
		static
		EgoPath getPath(Biangle const *b1, Biangle const *b2, double dist);
		
		/** Duplicates \c getPath(Biangle,Biangle) functionality, with the
		 * addition of evenly distributing the timespan between \a b1 and
		 * \a b2 among the returned path elements. */
		static
		EgoPath getPath(StampedBiangle const *b1,
		                StampedBiangle const *b2, double dist);
		
	};
};

#endif
