/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef EGOSPHERE_ROS_UTILS_H
#define EGOSPHERE_ROS_UTILS_H

#include <egosphere/egosphere_utils.h>
//#include <egosphere/egosphere_path.hpp>
// not sure I need/want vid message conversions
//#include <egosphere/VidMsg.h>
//#include <egosphere/StampedVidMsg.h>
//#include <egosphere/StampedVidMsgArray.h>
#include <egosphere_msgs/BiangleMsg.h>
#include <egosphere_msgs/Coordinate.h>
#include <egosphere_msgs/StampedBiangleMsg.h>
#include <egosphere_msgs/StampedBiangleMsgArray.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Quaternion.h>
#include <ros/time.h>
#include <std_msgs/ColorRGBA.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

/** ROS-related utility structures and functions. IMPORTANT NOTE: an
 * egosphere uses left-hand-rule orientation, whereas ROS standardized
 * on right-hand. */
namespace egosphere {
	
	typedef visualization_msgs::Marker Marker;
	typedef visualization_msgs::MarkerArray MarkerArray;
	
	class EgoPath;
	class EgoSearch;
	class EgoVertex;
	
	/** Marker types for coloration. */
	namespace highlite {
		enum MarkerType { VERTEX, DIRECTION, DIR_SHAFT, DIR_ARROW, CENTER, FACE, READING, IMPRESSION, SPHERES };
	}
	
	class RosUtils {
	public:
		/** Fill a ROS \c Vid message from a vertex ID. */
		//static
		//void vidToVidMsg(unsigned int vid, egosphere::VidMsg &msg);
		//
		/** Fill a ROS \c StampedVid message from a vertex ID. */
		//static
		//void vidToVidMsg(unsigned int vid, egosphere::VidMsg &msg);
		
		/** Convenience front-end for \c showOrientations using
		 * \c thInc=PI/2 and \c phInc=PI/4. */
		static
		std::string showOrientations(bool deg=true, bool rpy=true);
		
		/** Display biangle/quaternion translations for full \a theta
		 * rotations. Displayed translations will increment \a theta by
		 * \c thInc and run from elevation \a phi=0 to \a phi=PI/2,
		 * incrementing \a phi by \c phInc. */
		static
		std::string showOrientations(double thInc, double phInc,
		                             bool deg=true, bool rpy=true);
		
		/** Fill a ROS \c Time item (e.g., as found in a ROS \c Header message)
		 * with values from a \c Time item. */
		static
		void timeToRosTime(const Time& ego, ros::Time& ros);
		
		/** Fill a \c Time item with values from a ROS \c Time item (e.g.,
		 * as found in a ROS \c Header message). */
		static
		void timeRosToTime(const ros::Time& ros, Time& ego);
		
		/** Return a string representation of a \c Coordinate message. */
		static
		std::string coordinateStr(const egosphere_msgs::Coordinate &msg,
		                          bool nomag=false);
		
		/** Return a string representation of a pointer to a
		 * \c Coordinate message. */
		static
		std::string coordinateStr(const egosphere_msgs::Coordinate* msg,
		                          bool nomag=false);
		
		/** Fill a ROS \c Point message from an egosphere \c Point. Note
		 * this adjusts for egosphere's left-hand-rule to ROS's
		 * right-hand-rule (by negating the pitch). */
		static
		void pointToPointMsg(const Point &p, geometry_msgs::Point &msg);
		
		/** Fill a ROS \c Point message from an egosphere \c Point.  Note
		 * this adjusts for egosphere's left-hand-rule to ROS's
		 * right-hand-rule (by negating the pitch).*/
		static
		void pointToPointMsg(Point const *p, geometry_msgs::Point &msg);
		
		/** Fill an egosphere \c Point from a ROS \c Point message. Note
		 * this adjusts for egosphere's left-hand-rule to ROS's
		 * right-hand-rule (by negating the pitch). */
		static
		void pointMsgToPoint(geometry_msgs::Point const &msg, Point &p);
		
		/** Fill a ROS \c PointStamped message from an egosphere \c StampedPoint.
		 * Note this adjusts for egosphere's left-hand-rule to ROS's
		 * right-hand-rule (by negating the pitch). */
		static
		void stampedPointToPointStampedMsg(const StampedPoint &p,
		                                   geometry_msgs::PointStamped &msg);
		
		/** Fill an egosphere \c StampedPoint from a ROS \c PointStamped
		 * message. Note this adjusts for egosphere's left-hand-rule to ROS's
		 * right-hand-rule (by negating the pitch). */
		static
		void pointStampedMsgToStampedPoint(const geometry_msgs::PointStamped &msg,
		                                   StampedPoint &p);
		
		/** Fill a ROS \c Point message from an egosphere \c Biangle. Note
		 * this adjusts for egosphere's left-hand-rule to ROS's
		 * right-hand-rule (by negating the pitch). */
		static
		void biangleToPointMsg(Biangle const *b, geometry_msgs::Point &msg,
		                       double radius=1.0);
		
		/** Convenience method to fill a ROS \c Point message from an
		 * \c EgoVertex.  Note this adjusts for egosphere's left-hand-rule
		 * to ROS's right-hand-rule (by negating the pitch).*/
		static
		void vertToPointMsg(EgoVertex const *vert, geometry_msgs::Point &msg,
		                    double radius=1.0);
		
		/** Fill a ROS \c Point message from an egosphere \c BiangleMsg.
		 * Note this adjusts for egosphere's left-hand-rule to ROS's
		 * right-hand-rule (by negating the pitch). */
		static
		void biangleMsgToPointMsg(const egosphere_msgs::BiangleMsg &b,
		                          geometry_msgs::Point &msg, double radius=1.0);
		
		/** Fill a ROS \c Quaternion message from an egosphere \c Biangle.
		 * Note: this adjusts for egosphere's left-hand-rule to ROS's
		 * right-hand-rule (by negating both the pitch and yaw). */
		static
		void biangleToQuaternionMsg(Biangle const *b,
		                            geometry_msgs::Quaternion &msg);
		
		/** Fill a ROS \c Quaternion message from an egosphere \c BiangleMsg.
		 * Note: this adjusts for egosphere's left-hand-rule to ROS's
		 * right-hand-rule (by negating both the pitch and yaw). */
		static
		void biangleMsgToQuaternionMsg(const egosphere_msgs::BiangleMsg &b,
		                            geometry_msgs::Quaternion &msg);
		
		/** Convenience method to fill a ROS \c Quaternion message from an
		 * \c EgoVertex. Note: this adjusts for egosphere's left-hand-rule
		 * to ROS's right-hand-rule (by negating both the pitch and yaw). */
		static
		void vertToQuaternionMsg(EgoVertex const *vert,
		                         geometry_msgs::Quaternion &msg);
		
		/** Get the roll, pitch, and yaw values from a \c Biangle. Note:
		 * this adjusts for egosphere's left-hand-rule to ROS's
		 * right-hand-rule (by negating both the pitch and yaw). Also,
		 * it uses ROS's TF rotation matrix to obtain RPY. */
		static
		void biangleToRPY(Biangle const *b,
		                  double &r, double &p, double &y);
		
		/** Get the roll, pitch, and yaw values from a \c BiangleMsg. Note:
		 * this adjusts for egosphere's left-hand-rule to ROS's
		 * right-hand-rule (by negating both the pitch and yaw). Also,
		 * it uses ROS's TF rotation matrix to obtain RPY. */
		static
		void biangleMsgToRPY(const egosphere_msgs::BiangleMsg &b,
		                     double &r, double &p, double &y);
		
		/** Fill a ROS \c Pose message from an egosphere \c Biangle. */
		static
		void biangleToPoseMsg(Biangle const *b, geometry_msgs::Pose &msg,
		                      double radius=1.0);
		
		/** Fill a ROS \c Pose message from an egosphere \c BiangleMsg. */
		static
		void biangleMsgToPoseMsg(const egosphere_msgs::BiangleMsg &b,
		                         geometry_msgs::Pose &msg, double radius=1.0);
		
		/** Convenience method to fill a ROS \c Pose message from an
		 * \c EgoVertex. */
		static
		void vertToPoseMsg(EgoVertex const *vert, geometry_msgs::Pose &msg,
		                   double radius=1.0);
		
		/** Return a string representation of a \c Biangle message. */
		static
		std::string biangleMsgStr(const egosphere_msgs::BiangleMsg &msg,
		                          bool nomag=false);
		
		/** Fill a \c BiangleMsg from a \c Biangle. Note that \c StampedBiangle
		 * is a subclass of \c Biangle. */
		static
		void biangleMsgToBiangle(const egosphere_msgs::BiangleMsg &msg,
		                         Biangle &b);
		
		/** Fill a \c Biangle from a \c BiangleMsg. Note that \c StampedBiangle
		 * is a subclass of \c Biangle. */
		static
		void biangleToBiangleMsg(const Biangle &b,
		                         egosphere_msgs::BiangleMsg &msg);
		
		/** Return a string representation of a \c StampedBiangle message. */
		static
		std::string stampedBiangleMsgStr(const egosphere_msgs::StampedBiangleMsg &msg,
		                                 bool nomag=false);
		
		/** Fill a \c StampedBiangleMsg from a \c StampedBiangle. */
		static
		void stampedBiangleToStampedBiangleMsg(const StampedBiangle &sb,
	   	                                    egosphere_msgs::StampedBiangleMsg &msg);
		
		/** Fill a \c StampedBiangle instance from a \c StampedBiangleMsg. */
		static
		void stampedBiangleMsgToStampedBiangle(const egosphere_msgs::StampedBiangleMsg &msg,
		                                       StampedBiangle &sb);
		
		/** Fill a \c StampedBiangleMsgArray from an \c EgoPath. */
		static
		void egoPathToStampedBiangleMsgArray(const EgoPath &ep,
		                                     egosphere_msgs::StampedBiangleMsgArray &msg);
		
		/** Fill an \c EgoPath from a \c StampedBiangleMsgArray message. */
		static
		void stampedBiangleMsgArrayToEgoPath(const egosphere_msgs::StampedBiangleMsgArray &msg,
		                                     EgoPath &ep);
		
		/** */
		static
		void fillFaceMarkerPoints(const Point& egoPoint,
		                          const std::vector<Point>& neighbors,
		                          Marker& faceMarker);
		
		/** Fill a \c Point vector that can be used to represent the \a face
		 * around the given \c EgoVertex. The vector will contain groups of 3
		 * points that form triangles of \c EgoVertex \a ev and 2 adjacent
		 * neighbor vertices. Ordering of the points affects the lighting
		 * display in RViz: this ordering will highlight the exterior side
		 * of the \a face (reversing the order highlights the interior). */
		static
		void fillFacePointMsgVec(EgoVertex const *ev,
		                         std::vector<geometry_msgs::Point> &vec);
		
		/** Create an RViz \c Marker representing the \a face around an
		 * \c EgoVertex located at biangle \c bMe and point \c pMe, whose
		 * neighbors are located at the 3d euclidian \c Points contained
		 * in the \c neighbors vector (likely created using the \c
		 * neighborPoints function in the \c EgoVertex class. */
		//static
		//void fillMarkerFace(const std::string &frame,
		//                    Biangle const *bMe, const Point &pMe,
		//                    const std::vector<Point> &neighbors,
		//                    Marker &m);
		static
		void fillMarkerFace(EgoVertex const *ev, Marker &m);
		
		/** Create an RViz \c MarkerArray representing the \a vertices
		 * of the egosphere specified by \c verts and \c search located at
		 * center \c Pose with the specified radius. */
		static
		void fillMarkerEgoVerts(const std::string &frame, double radius,
		                        const std::vector<EgoVertex*> &verts,
		                        const EgoSearch &search,
		                        MarkerArray &markers);
		                        //Biangle const *bMe, const Point &pMe,
		                        //const geometry_msgs::Pose &p, double radius,
		
		/** Sets selected fields within a \c Marker according to the type of
		 * marker. Useful for standardization and code cleanliness; fields
		 * set include header frame, namespace, type, action, scale, pose,
		 * and color. */
		static
		void initMarker(highlite::MarkerType type, const std::string &frame,
		               const geometry_msgs::Pose &pose, Marker &m);
		
		/** Returns standardized coloration for defined MarkerTypes (with
		 * possible alpha-transparency decay). */
		static
		std_msgs::ColorRGBA getColor(highlite::MarkerType type,
			                          const ros::Duration &decay,
			                          const ros::Duration &tleft);
		
		/** Returns standardized scaling for defined MarkerTypes (with
		 * possible alpha-transparency decay). */
		static
		geometry_msgs::Vector3 getScale(highlite::MarkerType type, double unit);
		
	};
	
};
#endif

