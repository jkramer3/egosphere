/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef EGOSPHERE_IMPRESSION_H_
#define EGOSPHERE_IMPRESSION_H_

#include <egosphere/egosphere_utils.h>
//#include <map>

namespace egosphere_demo {

class MovableEgosphere; // friend class

class Impression {
	public:
		friend class MovableEgosphere;
		Impression();
		Impression(const egosphere::StampedPoint& impLocal);
		~Impression();
		
		//std::string str() const;
		
	private:
		egosphere::Biangle m_biangle;
		egosphere::Time m_stamp;
		//unsigned int m_vertexId;

		void setFromStampedPoint(const egosphere::StampedPoint& impLocal);
	};

};
#endif

