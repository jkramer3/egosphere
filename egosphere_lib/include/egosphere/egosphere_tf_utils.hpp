/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef EGOSPHERE_TF_UTILS_H
#define EGOSPHERE_TF_UTILS_H

#include <egosphere/egosphere_utils.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_listener.h>

/** ROS-related utility structures and functions. */
namespace egosphere {
	
	class TfUtils {
	public:
		/** Get a string of a \c tf::Point, formatted \c [x,y,z]. */
		static
		std::string tfPointStr(const tf::Point &tfPoint);
		
		/** Get a string of a \c tf::Pose, formatted \c [px,py,pz][qx,qy,qz,qw].
		 * Note that \c tf::Pose is \a typedef \c tf::Transform. */
		static
		std::string tfPoseStr(const tf::Pose &tfPose);
		
		/** Return the \c Point representing the transform from frame \c source
		 * to frame \c target. This method can throw any exception encountered
		 * by \c tf::lookupTransform. */
		static
		geometry_msgs::Point getPointMsgViaTf(
		                           std::shared_ptr<tf::TransformListener> tf,
		                           const std::string &target,
		                           const std::string &source);
		
		/** Return the \c PointStamped representing the transform from frame
		 * \c source to frame \c target. This method can throw any exception
		 * encountered by \c tf::lookupTransform. */
		static
		geometry_msgs::PointStamped getPointStampedMsgViaTf(
		                           std::shared_ptr<tf::TransformListener> tf,
		                           const std::string &target,
		                           const std::string &source);
		
		/** Return the \c Pose representing the transform from frame \c source
		 * to frame \c target. This method can throw any exception encountered
		 * by \c tf::lookupTransform. */
		static
		geometry_msgs::Pose getPoseMsgViaTf(
		                           std::shared_ptr<tf::TransformListener> tf,
		                           const std::string &target,
		                           const std::string &source);
		
		/** Get the pose at the specified radius/distance corresponding
		 * to the provided \c Point (assumed relative to egosphere center). */
		static
		geometry_msgs::Pose getPoseMsgRadius(double radius,
		                                     const Point& extPoint);
		
		/** Get the pose at the specified radius/distance corresponding
		 * to the provided pose (assumed relative to egosphere center). */
		static
		geometry_msgs::Pose getPoseMsgRadius(double radius,
		                           const geometry_msgs::Pose &extPose);
		
		/** Get the egosphere face point corresponding to the provided
		 * pose (that is already relative to egosphere center). */
		//static
		//geometry_msgs::Pose getPoseFace(const FeatureExternal *ego,
		//                           const geometry_msgs::Pose &extPose) const;
		
		/** Convenience method of \c offsetPoseInOtherFrame that
		 * returns a \c Pose. Drops the resultant pose's header;
		 * tailored for use with direct calls (e.g., an RViz
		 * interactive server's \c setPose method). */
		static
		geometry_msgs::Pose offsetPoseInOtherFrame(
		                std::shared_ptr<tf::TransformListener> tf,
		                const std::string &srcFrame,
		                const geometry_msgs::Pose &srcPose,
		                const std::string &offFrame,
		                const geometry_msgs::Pose &offPose);	
		
		/** Convenience method of \c offsetPoseInOtherFrame that
		 * returns a \c PoseStamped message. */
		static
		void offsetPoseInOtherFrame(
		                std::shared_ptr<tf::TransformListener> tf,
		                const std::string &srcFrame,
		                const geometry_msgs::Pose &srcPose,
		                const std::string &offFrame,
							 const geometry_msgs::Pose &offPose,
							 geometry_msgs::PoseStamped &newPose);
		
		/** Given a pose \c srcPose in frame of reference \c srcFrame,
		 * shift its position/orientation by the offset \c offPose
		 * as applied in frame of reference \c offFrame, updating
		 * \c newPose (in frame of reference \c srcFrame).
		 * If a \c TransformException occurs, the returned pose is
		 * unchanged (same as \c srcPose). */
		static
		void offsetPoseInOtherFrame(
		                std::shared_ptr<tf::TransformListener> tf,
		                const std::string &srcFrame,
		                const tf::Pose &srcPose,
		                const std::string &offFrame,
							 const tf::Pose &offPose,
							 tf::Stamped<tf::Pose> &newPose);
		
	};
	
};
#endif

