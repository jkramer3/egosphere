/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef RVIZ_EGOSPHERE_H
#define RVIZ_EGOSPHERE_H

#include <egosphere/rviz_vertex.hpp>
#include <egosphere/egosphere.hpp>
#include <std_msgs/ColorRGBA.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

namespace egosphere {
	
	typedef visualization_msgs::Marker Marker;
	typedef visualization_msgs::MarkerArray MarkerArray;
	
	struct CompassInfo {
		unsigned int cpIx;
		std_msgs::ColorRGBA color;
	};
	
	/** Egosphere subclass that provides RViz Markers. */
	class RVizEgosphere : public Egosphere<RVizVertex> {
		public:
			RVizEgosphere(int f=14);
			void stuffMarker(const int id, Marker &m, double radius=1.0,
			                 marker_type::MarkerType t=marker_type::FACE);
			void fillEgoVertsMarkerArray(const std::string &frame,
			                             MarkerArray &marr,
			                             double radius=1.0);
		
		private:
			void getCompassVerts(std::vector<CompassInfo> &ixvec);
	};

};
#endif
