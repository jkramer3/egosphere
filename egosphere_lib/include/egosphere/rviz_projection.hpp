/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef RVIZ_PROJECTION_H
#define RVIZ_PROJECTION_H

#include <egosphere/rviz_egosphere.hpp>
#include <egosphere/rviz_vertex.hpp>
#include <egosphere_msgs/CoordinateStampedArray.h>
#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <std_msgs/ColorRGBA.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <deque>
//#include <map>

namespace egosphere_demo {
	
	namespace vertex_generation {
		enum InactiveType { NONE, RANDOM, PATH, SEQUENCE, BEARING };
		std::string genTypeStr(InactiveType type) {
			switch (type) {
			case NONE:     return "none";
			case RANDOM:   return "random";
			case PATH:     return "path";
			case SEQUENCE: return "sequence";
			case BEARING:  return "bearing";
			default: return "unknown";
			}
		}
	}
	
	namespace vertex_highlite {
		enum InterimType { VERTICES, READING, INTERPOLATE, FACE, SEG_SURF, SEG_MAG };
	}

	typedef geometry_msgs::Pose Pose;
	typedef visualization_msgs::Marker Marker;
	typedef visualization_msgs::MarkerArray MarkerArray;
	typedef vertex_generation::InactiveType GenType;
	
	/** Structure used to store step information between message reception
	 * and marker publishing. */
	struct InterimInfo {
		vertex_highlite::InterimType type; // controls vertex display
		ros::Duration        decay;        // retention time
		geometry_msgs::Point point;        // start point
		geometry_msgs::Point endpoint;     // end point (if necessary)
		unsigned int         ego_index;    // vertex number
		std::string          category;
		std::string          traits;
		std::string          descs;
		std::string          text;
	};
	
	/** Highlights locations from an \c EgosphereGroup by monitoring
	 * specific TF frames (from skeleton tracking) that project onto
	 * the egosphere surface. When no tracking frames are present,
	 * generates vertex highlights according to various schemes
	 * (e.g., random, sequence, etc.). */
	class RVizProjection {
		public:
			RVizProjection(ros::NodeHandle &nh);
			~RVizProjection();
			
		private:
			// egosphere members
			egosphere::RVizEgosphere* m_egosphere;
			double                    m_egoRadius;
			MarkerArray               m_egoVertMarkers;
			bool                      m_pubVertMarkers;
			
			// egosphere center frame members
			ros::Subscriber     m_centerSub;
			std::string         m_center, m_ground;
			geometry_msgs::Pose m_centerPose;
			bool                m_allowRotation;
			/** Move the egosphere's centered to the given pose. */
			void centerPoseCb(const geometry_msgs::Pose::ConstPtr &msg);
			
			// external frame display members
			bool                       m_externalActive;
			ros::Subscriber            m_externalSub;
			//std::string                m_external;
			geometry_msgs::PoseStamped m_externalPose;
			void externalPoseCb(const geometry_msgs::PoseStamped::ConstPtr &msg);
			
			// surface point-locations
			ros::Subscriber m_pathSub;
			void pathCb(const egosphere_msgs::CoordinateStampedArray::ConstPtr &msg);
			void path2Interims(const egosphere::StampedBiangle &b1,
			                   const egosphere::StampedBiangle &b2);
			egosphere::StampedBiangle coordSt2BiangleSt(const egosphere_msgs::CoordinateStamped &msg);
			InterimInfo biangle2Interim(const egosphere::Biangle &msg,
			                            const vertex_highlite::InterimType,
			                            const ros::Duration decay);
			
			// screen-saver mode
			bool                     m_genInactiveVerts;
			GenType                  m_genType;
			unsigned int             m_lastGenIx;
			ros::Time                m_lastGenTime;
			ros::Duration            m_genInterval;
			double                   m_bearing;
			std::deque<unsigned int> m_vertIxs;
			/** Determine a vertex to display in screen saver mode by taking
			 * the front of the m_vertIxs deque (which is filled as needed
			 * by a call to 'fillGenVerts'). */
			void handleInactive();
			/** Check for an empty vertex deque, choosing vertexes when the
			 * deque is empty (according to the genType). */
			void fillGenVerts(std::deque<unsigned int> &vixs);
			/** Change the vertex generation type. */
			GenType nextGenType(GenType cur, bool incNone);
			/** Extract vertex information for later use in conversion to
			 * a Marker. */
			InterimInfo vert2Interim(unsigned int vix);
			
			// publishing members
			ros::Duration           m_decay;         // time to engage screen saver
			ros::Duration           m_decayInterval; // alpha fade update
			bool                    m_readingsOnly;  // 
			ros::Time               m_lastMsgTime;   // screen saver mark
			ros::Timer              m_timer;         // event generator
			std::deque<InterimInfo> m_interims;      // vertex -> marker data
			ros::Publisher          m_markerPub;     // marker publisher

			/** Utility method marking external message reception. Resets
			 * screen saver mode and clears related data structures. */
			void gotCallback();
			/** Event loop that checks for and handles screen saver mode
			 * initialization, then creates the MarkerArray for publishing. */
			void pubTimer(const ros::TimerEvent &te);
			/** Converts member of the interim deque (about vertices), filling a
			 * MarkerArray for publishing. */
			void processInterims(std::deque<InterimInfo> &ideq, MarkerArray &marr);
			/** Converts a single interim data item into a Marker. */
			void processInterim(const InterimInfo &info, Marker &m);
			/** Modifies a Marker to display as a certain visual type. */
			void setMarker(vertex_highlite::InterimType type, Marker &m);
			/** Returns a color for a certain visual type. */
			std_msgs::ColorRGBA getColor(vertex_highlite::InterimType type,
			                             const ros::Duration &tleft);
			
	};

}
#endif
