/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef EGOSPHERE_H
#define EGOSPHERE_H

#include <egosphere/egosphere_utils.h>
#include <egosphere/ego_search.h>
#include <egosphere/ego_vertex.h>
#include <egosphere/ego_path.hpp>
#include <egosphere/egosphere_path.hpp>
#include <exception>
#include <vector>

namespace egosphere {
	
	/** An \a egosphere class for convenience and demonstration: provides
	 * an API based on vertex IDs (rather than raw pointers) in addition
	 * to being templated for subclasses of \c EgoVertex. This class can
	 * be subclassed, typically when using a subclass of \c EgoVertex
	 * (e.g., the \a RVizEgosphere class).
	 *
	 * Alternatively, it may be desirable to have more direct interaction
	 * with vertices, in which case, an implementation should include two
	 * members, a \a std::vector<EgoVertex*> and an \a EgoSearch. */
	template<class T>
	class Egosphere {
		// NOTE: templated class! There's a cpp #include at end!
		public:
			Egosphere(int f=14);
			~Egosphere();
			
			/** Return the \a frequency of this egosphere. */
			int frequency() const;
			
			/** Return the number of vertices in this egosphere. */
			int size() const;
			
			/** Return the \a minimum angular distance between the
			 * closest-together vertices. Because egosphere vertices are \a not
			 * equidistant, this is the angle separating the two vertices
			 * that are \a closest to one another. All vertices are
			 * guaranteed to be this distance or more from their neighbors. */
			double getMinAngle() const;
			
			/** Return the \a maximum angular distance between the
			 * furthest-apart vertices. Because egosphere vertices are \a not
			 * equidistant, this is the angle separating the two vertices
			 * that are \a furthest from one another. All vertices are
			 * guaranteed to be within this distance from their neighbors. */
			double getMaxAngle() const;
			
			/** Return a pointer to a new \c Biangle instance matching that of
			 * vertex \a id (\c NULL if \a id does not exist). Caller is
			 * responsible for deletion. */
			Biangle* getBiangleOf(unsigned int id) const;
			
			/** Fill \c Biangle \a b with values that match those of vertex
			 * \a id. Returns \c false and sets internal values to \c NAN if
			 * vertex \a id does not exist. */
			bool getBiangleOf(unsigned int id, Biangle& b) const;
			
			/** Return a pointer to a new \c Point instance matching that of
			 * vertex \a id (\c NULL if \a id does not exist). Caller is
			 * responsible for deletion. */
			Point* getPointOf(unsigned int id, double radius=1.0) const;
			
			/** Fill \c Point \a p with values that match those of vertex
			 * \a id. Returns \c false and sets internal values to \c NAN if
			 * vertex \a id does not exist. */
			bool getPointOf(unsigned int id, Point& p, double radius=1.0) const;
			
			/** Locate and return the ID of the vertex closest in spherical
			 * distance to \c Biangle \a b. */
			unsigned int nearest(Biangle const &b) const;
			
			/** Locate and return the ID of the vertex closest in
			 * Cartesian-squared distance to \c Point \a p. */
			unsigned int nearest(Point const &p) const;
			
			/** Return the number of neighbors associated with vertex \a id. */
			int numNeighbors(int id) const;
			
			/** Return a vector of vertex IDs neighboring vertex \a id. */
			std::vector<int> getNeighbors(int id) const;
			
			/** Test whether vertices \a id1 and \a id2 are neighbors.
			 * While the relation is reflexive, internally checks if \a id2
			 * is a neighbor of \a id1. Note that a vertex is never its
			 * own neighbor. */
			bool isNeighbor(int id1, int id2) const;
			
			/** Get the \a bearing of vertex \a n_id relative to \a v_id.
			 * Returns \c NAN if \a v_id \c == \a n_id, or either \a v_id
			 * or \a n_id do not exist. */
			double getNeighborBearing(int v_id, int n_id) const;
			
			/** Get the vertex ID of the neighbor closest to \a bearing
			 * relative to vertex \a id. Note that it is possible for multiple
			 * neighbors to have the same bearing (e.g., at the poles).
			 * Returns -1 if \a id does not exist. */
			int getNeighborClosestToBearing(int id, double bearing) const;
			
			/** Return an \c EgoPath object (containing a sequence of
			 * \c Biangles) that represents a path along the great circle
			 * from \c Biangle \a b1 to \a b2 (excluding \a b1 and including
			 * \a b2). Note that this method has a \a O(n) running time, where
			 * \a n is the (angular) distance between \a b1 and \a b2 divided
			 * by this egosphere's minimum inter-vertex angular distance. */
			EgoPath getPath(const Biangle &b1, const Biangle &b2) const;
			
			/** Return an \c EgoPath object (containing a sequence of
			 * \c StampedBiangles) that represents a path along the great
			 * circle from \c StampedBiangle \a b1 to \a b2 (excluding \a b1
			 * and including \a b2). This method duplicates the
			 * \c getPath(Biangle,Biangle) functionality with the addition
			 * of equally distributing the time between \a b1 and \a b2
			 * among the path elements. */
			EgoPath getPath(const StampedBiangle &b1,
			                const StampedBiangle &b2) const;
			
			/** Return an \c EgospherePath object (containing a sequence of
			 * vertex IDs) that represents a path along the great circle
			 * from vertex IDs \a id1 to \a id2 (with or without repeated
			 * IDs, excluding \a id1 and including \a id2). */
			EgospherePath getPath(unsigned int id1, unsigned int id2,
			                      bool dups=false) const;
			
			// TODO: return an EgoPath rather than vector?
			/** Fill list \a l with a sequence of vertex IDs that represent
			 * a path along the great circle from vertex \a id1 to \a id2
			 * (inclusive). Note that this method runs in \a O(nlogn), where
			 * \a n is the number of vertices necessary to cover the distance
			 * between \a id1 and \a id2, using a divide & conquer algorithm
			 * that repeatedly halves the distance until a neighboring vertex
			 * is found, then re-combining into the returned list. */
			void getPath(unsigned int id1, unsigned int id2,
			             std::list<unsigned int> &l) const;
			
			/** Fill list \a l with a sequence of vertex IDs that represent
			 * a path along the great circle from the vertex nearest to
			 * \c Biangle \a b1 to the vertex nearest to \a b2. Note that
			 * this is a convenience front-end to the \c getPath method with
			 * vertex ID parameters. */
			void getPath(const Biangle &b1, const Biangle &b2,
			             std::list<unsigned int> &l) const;
			
			// do I want to supply a string function?
			//std::string str() const;
			
		protected:
			std::vector<T*>  m_egoVerts;
			EgoSearch        m_egoSearch;
			bool isId(int id) const;
			
		private:
			int    m_freq;
			double m_minAngle, m_maxAngle;
			/** Recursive call used for divide and conquer method of
			 * calculation for \c getPath methods. */
			void getPathHelper(const Biangle *b1, const Biangle *b2,
			                   std::list<unsigned int> &l) const;
		
	};

};
#include "egosphere.cpp"
#endif

