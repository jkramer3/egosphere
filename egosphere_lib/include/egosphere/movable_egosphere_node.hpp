/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef EGOSPHERE_MOVABLE_NODE_H_
#define EGOSPHERE_MOVABLE_NODE_H_

#include <ros/ros.h>
#include <egosphere/movable_egosphere.hpp>
#include <geometry_msgs/Pose.h>
#include <std_msgs/String.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

namespace egosphere_demo {
	
	typedef visualization_msgs::Marker Marker;
	typedef visualization_msgs::MarkerArray MarkerArray;
	
	/** Impression \c MarkerArray indices. */
	namespace marr_ix {
		//enum MarkerIndex { BIANGLE, DIR_ARROW, FACE_FULL, DIR_SHAFT, FACE_HALF };
		enum MarkerIndex { BIANGLE, DIRECTION, FACE_FULL };
	}
	
	/** ROS interface to the \c MovableEgosphere class. The intent of the
	 * "movable egosphere" example is to continue working through an
	 * appropriate system architecture and class structure that enables
	 * use of an egosphere as a discretizing filter of external items.
	 * This is the third development step; the first being \c RvizProjection
	 * and the second the \c FeatureExternal demo. Specifically, this
	 * demo provides a movable egosphere and maintains its relationship
	 * to stationary external "impressions", as displayed in RViz.
	 *
	 * Note that due to the use of ROS timer events as the trigger for
	 * update events, data integrity is guaranteed through the sequential
	 * processing of the ROS message queue. */
	class MovableEgosphereNode {
		public:
			MovableEgosphereNode(ros::NodeHandle& nh);
			~MovableEgosphereNode();
		
		private:
			// **********************************
			// *** infrastructure members/methods
			// **********************************
			ros::NodeHandle m_nh;            // ROS nodehandle
			std::string m_name;              // ROS node name
			ros::Timer m_timerPub;           // publishing timer
			ros::Duration m_timerPubRate;    // publishing rate
			ros::Timer m_timerTf;            // tf broadcast/lookup timer
			ros::Duration m_timerTfRate;     // tf broadcast/lookup rate
			std::shared_ptr<tf::TransformListener> m_tfliPtr;
			std::shared_ptr<tf::TransformBroadcaster> m_tfbrPtr;
			void showConfig() const; // loading config spew
			void pubTimer(const ros::TimerEvent &te) const; // publish event
			void tfTimer(const ros::TimerEvent &te) const;  // tf event
			
			// **********************************
			// *** egosphere members/methods
			// **********************************
			MovableEgosphere* m_egosphere;      // the egosphere
			std::string m_center, m_ground;     // egosphere center/global frames
			double              m_radius;       // egosphere display radius
			MarkerArray         m_egoVerts;     // egosphere vertex markers
			ros::Publisher      m_egoVerts_pub; // egosphere marker publisher
			ros::Subscriber     m_center_sub;   // egosphere center mod
			geometry_msgs::Pose m_centerPose;   // egosphere center pose
			void pubEgoVerts() const;           // egosphere marker publishing
			void centerPoseCb(const geometry_msgs::Pose::ConstPtr &msg);
			void broadcast() const;             // center frame TF broadcast
			void updateImpressions() const;     // lookup TF updates
			void pubImpressions() const;        // publish relative highlights
			/** Lookup (via TF) the relative position of \c iid (can throw any
			 * exception from \c tf::lookupTransform). */
			egosphere::StampedPoint impressionPosition(const std::string& iid) const;
			
			// ***********************************
			// *** "Impression" interface
			// ***********************************
			ros::Subscriber m_impAdd_sub;
			ros::Subscriber m_impRemove_sub;
			ros::Publisher  m_imp_pub;
			std::map<std::string,MarkerArray> m_impMarkers;
			void impAddCb(const std_msgs::String::ConstPtr &msg);
			void impRemoveCb(const std_msgs::String::ConstPtr &msg);
			MarkerArray createImpressionMarker(const std::string& iid) const;
			void updateImpressionMarker(const std::string& iid) const;
			const MarkerArray& getImpressionMarker(const std::string& iid) const;
			
	};

};
#endif

