/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef EGOSPHERE_PATH_H
#define EGOSPHERE_PATH_H

#include <egosphere/ego_path.hpp>
#include <egosphere/egosphere_ros_utils.hpp>
#include <egosphere_msgs/StampedBiangleMsgArray.h>
#include <list>
#include <vector>

namespace egosphere {
	
	template<class T>
	class Egosphere;
	class StampedBiangle;
	class Time;
	
	/** A container of \c EgoVertex IDs that represent a path
	 * on the surface of an egosphere. This is mostly a convenience to
	 * provide a standard API, as it is essentially a wrapper around a
	 * standard library \c list. */
	class EgospherePath {
		public:
			EgospherePath();
			template <class T>
			EgospherePath(const Egosphere<T> &egosphere,
			              const std::list<StampedBiangle> &steps) {
				std::list<StampedBiangle>::const_iterator it;
				for (it=steps.begin(); it!=steps.end(); it++) {
					m_path.push_back(egosphere.nearest(*it));
				}
			}
			template <class T>
			EgospherePath(const Egosphere<T> &egosphere,
			              const egosphere_msgs::StampedBiangleMsgArray &msg) {
				for (int i=0; i<msg.biangles.size(); i++) {
					StampedBiangle sb;
					RosUtils::stampedBiangleMsgToStampedBiangle(msg.biangles[i], sb);
					m_path.push_back(egosphere.nearest(sb));
				}
			}
			template <class T>
			EgospherePath(const Egosphere<T> &egosphere,
			              const EgoPath &ep, bool dups=true) {
				std::vector<StampedBiangle> sb_v;
				ep.toVector(sb_v);
				for (unsigned int i=0; i<sb_v.size(); i++) {
					if (dups) {  // allow duplicates, just push
						m_path.push_back(egosphere.nearest(sb_v[i]));
					} else {     // no dups, check ID vs list back
						unsigned int nextId = egosphere.nearest(sb_v[i]);
						if (m_path.back() != nextId) {
							m_path.push_back(nextId);
						}
					}
				}
			}
			//EgospherePath(const std::list<unsigned int> &steps);
			EgospherePath(const EgospherePath& other)
					: m_path(other.m_path) { }
			friend void swap(EgospherePath& first, EgospherePath& second) {
				using std::swap;
				swap(first.m_path, second.m_path);
			}
			EgospherePath& operator=(EgospherePath other) {
				swap(*this, other);
				return *this;
			}
			
			std::list<unsigned int>::iterator begin();
			std::list<unsigned int>::const_iterator begin() const;
			std::list<unsigned int>::iterator end();
			std::list<unsigned int>::const_iterator end() const;
			bool empty() const;
			unsigned int size() const;
			unsigned int peekFront() const;
			unsigned int peekBack() const;
			void clear();
			unsigned int popFront();
			int popFront(int n);
			unsigned int popBack();
			int popBack(int n);
			void pushFront(unsigned int vid);
			void pushFront(const EgospherePath &ep);
			template <class T>
			void pushFront(const Egosphere<T> &e, const StampedBiangle &b) {
				this->m_path.insert(m_path.begin(), e.nearest(b));
			}
			template <class T>
			void pushFront(const Egosphere<T> &e, const std::list<StampedBiangle> &l) {
				this->m_path.insert(m_path.begin(), l.begin(), l.end());
			}
			template <class T>
			void pushFront(const Egosphere<T> &e, const EgoPath &ep) {
				std::vector<StampedBiangle> sb_v;
				ep.toVector(sb_v);
				for (int i=sb_v.size()-1; i>=0; i--) {
					m_path.push_front(e.nearest(sb_v[i]));
				}
			}
			void pushBack(unsigned int vid);
			void pushBack(const EgospherePath &ep);
			template <class T>
			void pushBack(const Egosphere<T> &e, const StampedBiangle &b) {
				this->m_path.insert(m_path.end(), e.nearest(b));
			}
			template <class T>
			void pushBack(const Egosphere<T> &e, const std::list<StampedBiangle> &l) {
				this->m_path.insert(m_path.end(), l.begin(), l.end());
			}
			template <class T>
			void pushBack(const Egosphere<T> &e, const EgoPath &ep) {
				std::vector<StampedBiangle> sb_v;
				ep.toVector(sb_v);
				for (int i=0; i<sb_v.size(); i++) {
					m_path.push_back(e.nearest(sb_v[i]));
				}
			}
			// TODO: not sure I want to allow arbitrary insert/erase
			//void insert(unsigned int pos, const StampedBiangle &b);
			//void erase(unsigned int pos);
			
			//Time timespan();
			void toVector(std::vector<unsigned int> &v) const;
			template <class T>
			void toVector(const Egosphere<T> &e, std::vector<StampedBiangle> &v) const {
				v.clear();
				v.reserve(m_path.size());
				std::list<unsigned int>::const_iterator it;
				for (it=m_path.begin(); it!=m_path.end(); it++) {
					StampedBiangle sb;
					if (e.getBiangleOf(*it, sb)) {
						v.push_back(sb);
					}
				}
			}
			template <class T>
			void toEgoPath(const Egosphere<T> &e, EgoPath &ep) const {
				ep.clear();
				std::list<unsigned int>::const_iterator it;
				for (it=m_path.begin(); it!=m_path.end(); it++) {
					StampedBiangle sb;
					if (e.getBiangleOf(*it, sb)) {
						ep.pushBack(sb);
					}
				}
			}
			
		private:
			std::list<unsigned int> m_path;
			
	};
	
};
#endif

