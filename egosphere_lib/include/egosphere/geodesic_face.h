/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef GEOFACE_H
#define GEOFACE_H

#include <egosphere/geodesic_vertex.h>
#include <egosphere/egosphere_utils.h>
#include <vector>
#include <cmath>
#include <algorithm>

namespace egosphere {
	
	/* Class representing a planar face of the egosphere. This class is
	 * essentially a temporary structure used while constructing the
	 * egosphere's geodesic sphere. It provides the foundation for
	 * identifying and maintaining items such as adjacent faces and
	 * subdivided edges. */
	class GeoFace {
	public:
		GeoFace();
		GeoFace(GeoVertex *v1_ptr, GeoVertex *v2_ptr, GeoVertex *v3_ptr);
		~GeoFace();
		
		/** Returns an integer representing the 'side' this face shares
		 * with another. A return value of 0 indicates no shared side,
		 * values of 1, 2, or 3 indicate two shared (somewhat arbitrarily
		 * labelled) vertices, where side 1 links (v1,v2), side 2 links
		 * (v2,v3), and side 3 links (v1,v3). */
		int adjacent(GeoFace *other);

		/** Marks the given side as adjacent to the given face. */
		void setAdjacent(int side, GeoFace *other);

		/** Stores the given vector of vertices as the subdivision of
		 * the line between vertices p1 and p2. */
		void setSubVerts(GeoVertex *p1, GeoVertex *p2, std::vector<GeoVertex*>& subverts);

		/** Puts the vertices subdividing the line between vertices p1 and
		 * p2 into the \c subverts vector. */
		void getSubVerts(GeoVertex *p1, GeoVertex *p2, std::vector<GeoVertex*>& subverts);

		/** Marks the given side as 'subdivided' with the ability to propagate
		 * the subdivision to an adjacent face. */
		void setDivided(int side, bool propagate);
		
		GeoVertex *v1, *v2, *v3;   // vertex corners
		GeoFace *a1, *a2, *a3;     // adjacent faces
		bool m1, m2, m3;           // subdivided side flag

	private:
		std::vector<GeoVertex*> s1_subverts, s2_subverts, s3_subverts;
	};
	
};

#endif

