/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef EGO_SEARCH_H
#define EGO_SEARCH_H

#include <egosphere/egosphere_utils.h>
#include <map>
#include <vector>

namespace egosphere {
	
	class EgoSearch {
	public:
		EgoSearch();
		EgoSearch(double minTh, double maxTh, double spanTh,
		          double minPh, double maxPh, double spanPh,
					 std::vector< std::vector< std::map<Biangle,int> > > vs);
		~EgoSearch();
		
		/** Locate and return the index of the vertex closest in spherical
		 * distance to \c Biangle \a b from the \a verts structure. */
		int nearest(Biangle const &b) const;
		
		/** Locate and return the index of the vertex closest in
		 * Cartesian-squared distance to \c Point \a p from the \a verts
		 * structure. */
		int nearest(Point const &p) const;
		
		// might want to remove after debugging
		std::string str() const;
		
	private:
		double minTheta, maxTheta;
		double minPhi, maxPhi;
		double bucketSizeTheta, bucketSizePhi;
		std::vector< std::vector< std::map<Biangle,int> > > verts;
	};

};
#endif

