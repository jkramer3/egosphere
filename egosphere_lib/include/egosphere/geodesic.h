/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef GEODESIC_H
#define GEODESIC_H

#include <egosphere/egosphere_utils.h>
#include <egosphere/ego_search.h>
#include <egosphere/ego_vertex.h>
#include <egosphere/geodesic_face.h>
#include <egosphere/geodesic_search.h>
#include <egosphere/geodesic_vertex.h>
#include <egosphere/geodesic_metainfo.h>
#include <egosphere/geodesic_voronoi.h>
#include <vector>
#include <iostream>
#include <fstream>

namespace egosphere {
	
	/** Class for constructing a geodesic sphere from an icosahedron (convex
	 * polyhedron with 12 vertices, 30 edges, and 20 (triangular) faces; these
	 * form 12 pentagons, each with a vertex at its center). For the egosphere,
	 * all that's really of interest is the set of vertices (and the structure
	 * necessary to efficiently \a search); it is expected that an instance of
	 * this class will be destructed after the vertices are extracted.
	 *
	 * Geodesics are classified by their \a frequency (\c N), which
	 * specifies the number of arc segments between pentagon centers (a
	 * frequency \c N=1 is the basic icosahedron). Each frequency increment
	 * adds a new vertex to each edge (\a subdividing the edges into \c N
	 * segments). The new vertices are then connected to their neighbors;
	 * each triangular face is thus split into \a 2^(N+1) triangles. An
	 * even-valued frequency has the characteristic of providing an
	 * "equator" line that divides the sphere into upper and lower
	 * hemispheres; the vertices of an egosphere with an odd-valued
	 * frequency alternate above and below the equator line.
	 * 
	 * Egosphere vertices are spatially located by their \a altitude
	 * (\a phi, the angle above or below the x-y plane ranging from
	 * \c -PI\2 to \c PI\2) and \a azimuth (\a theta, the angle from the
	 * \c x axis ranging from \c -PI to \c PI). For egosphere purposes,
	 * the icosahedron is oriented so that two vertices have 'pole'
	 * positions, establishing vertices both directly 'under' and
	 * 'overhead' the origin (\c x and \c y coordinates of zero, \c z of
	 * +/- unit length, \a phi of +/- PI and \a theta of 0).
	 *
	 * In addition to the geodesic sphere's vertices, the set of \a Voronoi
	 * vertices is calculated (the sphere's vertices are a Delaunay
	 * triangulation). A \a Voronoi vertex is located in the center of a
	 * subdivided triangle face, equidistant from each of the three face
	 * vertices. Connecting the voronoi vertices surrounding a sphere
	 * vertex yields a convex polygon such that every location within the
	 * polygon is closest to that sphere vertex, which can be used to
	 * answer \a point-location queries (via the \c GeoSearch class). */
	class Geosphere {
	public:
		Geosphere();
		Geosphere(int f);
		~Geosphere();
		
		/** Copy the \c Geoshere vertex and associated search structures,
		 * translating the \c GeoVertex* vector into
		 * a the \c verts vector. */
		template<typename T>
		EgoSearch getEgoStructs(std::vector<T*>& verts) {
			EgoVertex *baseT = new T(); // T *MUST BE* EgoVertex subclass
			std::vector<GeoVertex*>::iterator gvit;
			std::vector<GeoVertex*>::const_iterator gvneighit;
			std::map<const GeoVertex*, int> gv2ev;
			std::map<const GeoVertex*, int>::iterator mapit;
			std::vector<GeoVertex*> neighs;
			std::vector< std::vector< std::map<Biangle,int> > > vec3;
			unsigned int evix;
			
			// create the pared-down EgoVertices and mapping
			verts.clear();
			evix = 0;
			for (gvit=vertices.begin(); gvit<vertices.end(); gvit++) {
				Biangle gv_b( *((*gvit)->getBiangle()) );
				T *ev = new T(gv_b);
				ev->setIndex(evix);
				verts.push_back(ev);
				gv2ev.insert( std::pair<GeoVertex*, int>(*gvit, evix++) );
			}
			// fill in the neighbors
			for (gvit=vertices.begin(); gvit<vertices.end(); gvit++) {
				(*gvit)->getNeighbors(neighs);
				evix = gv2ev[(*gvit)];
				for (gvneighit=neighs.begin(); gvneighit<neighs.end(); gvneighit++) {
					verts[evix]->addNeighbor(verts[gv2ev[(*gvneighit)]]);
				}
			}
			// map the GeoVertex search to EgoVertex
			geoSearch->map2EgoSearch(gv2ev, vec3);
			EgoSearch es(metainfo.minTheta, metainfo.maxTheta, metainfo.maxVertD,
			             metainfo.minPhi, metainfo.maxPhi, metainfo.maxVertD,
		   	          vec3);
			delete baseT;
			return es;
		}
		
		/** Return the \a frequency of the geosphere. */
		int frequency() const;
		
		/** Returns selected metainformation about the constructed geosphere. */
		GeosphereMetainfo getMetainfo() const;
		
		/** Copy the \c GeoVertex* into the \c verts vector. */
		void getVertices(std::vector<GeoVertex*>& verts);
		
		/** Return the GeoVertex* that is the minimum distance from given
		 * location, performed in an exhaustive manner (for
		 * debugging/confirming operation). */
		GeoVertex* minDistanceVertexExhaustive(double theta, double phi);
		
		/** Minimum distance from given location to nearest vertex performed
		 * in an exhaustive manner (for debugging/confirming operation). */
		double minDistanceExhaustive(double theta, double phi);
		
		/** Display the information for the closest GeoVertex to the given
		 * \a theta and \a phi angles. This will become a 'findNearest'
		 * function that returns an GeoVertex* when it's working. Return
		 * \c true if the search yielded the same answer as an exhaustive
		 * calculation, \c false otherwise. */
		bool printNearest(double theta, double phi);
		
		/** Return a string containing information about all the vertices
		 * in this geodesic sphere. */
		std::string vert_str() const;
		
		/** Return a string containing information about all the voronoi
		 * vertices in this geodesic sphere. */
		std::string voro_str() const;
		
		/** Return a string containing the \a meta-information for this
		 * geosphere. */
		std::string metainfo_str() const;
		
		/** Return a string of the search structure. */
		std::string search_str() const;
		std::string search_str(int dec) const;
		std::string search_sum_str() const;
		
		/** Output the vertex coordinates (and face indices) to files. File
		 * names will have the suffix '_[freq].xyz' and '_[freq].xyzf' appended
		 * to the given name. */
		void writeFiles(std::string finm) const;
		
		/** Run a test of \a n randomized point locations. */
		void test_search(int n);
		
	private:
		/** Creates the structure representing a basic icosahedron. */
		void generateIcosahedron();
		
		/** Subdivide icosahedron triangle faces into \a numdiv^2 triangles
		 * to increase the surface resolution. */
		void subdivide(int numdiv);
		
		/** Subdivision helper function that subdivides one of the icosahedron
		 * edges. New vertices are placed in \a ev, a copy of which is also
		 * stored in GeoFace \c f for later retrieval/comparison by an adjacent
		 * face. */
		void subdiv_ico_edge(GeoFace *f, int numdiv,
		                     int side, std::vector<GeoVertex*>& ev);
		
		/** Subdivision helper function that subdivides the edge between
		 * vertices \a u and \a v into \a numdiv segments by creating the
		 * requisite new vertices (then stored/returned in \a addv). */
		void subdiv_help(GeoVertex *u,
		                 GeoVertex *v,
		                 int numdiv,
		                 std::vector<GeoVertex*>& addv);
		
		/** Calculates adjacent triangle faces. */
		void fillAdjacents(std::vector<GeoFace*>& faces);
		
		/** Returns the \c GeoVoronoiVertex; that is, the \c Biangle location
		 * that is equidistant from the given GeoVertex locations. */
		GeoVoronoiVertex* calcVoronoi(GeoVertex *v1, GeoVertex *v2, GeoVertex *v3);
		
		/** Perform sundry operations to finish the geodesic construction
		 * (e.g., sort and assign array indices). */
		void finishConstruction();
		
		/** Write two files: the Cartesian coordinates of the GeoVertices
		 * and the three indices that comprise a triangular face. */
		void writeFile_Faces(std::string filename) const;
		
		/** Utility function that stuffs icosahedron faces. */
		void stuffBasicFaces(std::vector<GeoFace*> &faces);
		
		/** Pristine vertex storage. */
		std::vector<GeoVertex*> vertices;
		
		/** GeoVoronoi vertices (GeoVertex vertices form Delaunay). */
		std::vector<GeoVoronoiVertex*> voronoi;
		
		/** The geodesic /a frequency (number of facial subdivisions). */
		int freq;
		
		/** The \a meta-information about this geosphere. */
		GeosphereMetainfo metainfo;
		
		/** The object that supplies nearest vertex (i.e., \a point-location)
		 * facilities. */
		GeoSearch *geoSearch;
		
	};
	
};

#endif

