/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef GEODESIC_VERTEX_H
#define GEODESIC_VERTEX_H

#include <egosphere/egosphere_utils.h>
#include <egosphere/ego_vertex.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>

namespace egosphere {
	
	/** Class representing a \a vertex of an \a egosphere, located in space
	 * relative to the egosphere origin at angles \a phi and \a theta (the
	 * altitude and azimuth). Note that this is meant as an interim class,
	 * used to \a build and egosphere, but being replaced after egosphere
	 * construction by instances of \c EgoVertex. */
	class GeoVertex {
	public:
		GeoVertex();
		GeoVertex(double t, double p);
		GeoVertex(Point* p_ptr);
		GeoVertex(const GeoVertex& other);
		~GeoVertex();
		GeoVertex& operator=(GeoVertex other);
		bool operator==(const GeoVertex &other);
		bool operator==(EgoVertex &other);
		bool operator<(const GeoVertex &other);
		
		// TODO: why does 'friend' cause a 'no declaration' compile error?
		//friend void swap(GeoVertex& first, GeoVertex& second);
		void swap(GeoVertex& first, GeoVertex& second);
		Biangle const * getBiangle() const;
		Point const * getPoint() const;
		void setIndex(int ix);
		
		int numNeighbors();
		void addNeighbor(GeoVertex *v_ptr);
		void addNeighbor(GeoVertex *v_ptr, bool propagate);
		void getNeighbors(std::vector<GeoVertex*>& neighs);
		
		void setVoroAngs(Biangle const *b, bool wrap);
		double getVoroAng(bool min, bool phi) const;
		bool getVoroWrap() const;
		double distance(double theta, double phi) const;
		double distance(Biangle& other) const;
		double distance(GeoVertex *other) const;
		double minNeighborDistance() const;
		double maxNeighborDistance() const;
		std::string str() const;
		std::string str(int dec) const;
		std::string ang_str() const;
		std::string ang_str(int dec) const;
		std::string point_str() const;
		std::string point_str(int dec) const;
		std::string voro_str() const;
		std::string voro_str(int dec) const;
		std::string neigh_str() const;
		std::string neigh_str(int dec) const;
		
	private:
		Biangle* biangle;
		Point* point;
		int index;
		std::vector<GeoVertex*> neighbors;
		std::vector<double> voroThetas;
		double minVoroTheta, maxVoroTheta;
		double minVoroPhi, maxVoroPhi;
		bool voroWrap;
		void calcPoint();
		void calcAngles();
		bool cwGreaterThan(GeoVertex *g1, GeoVertex *g2, bool print=false);
	};
	
	struct compareGeoVertexPtrs {
		bool operator ()(GeoVertex *lhs, GeoVertex *rhs) {
			return *lhs < *rhs;
		}
	};
	
	struct compareGeoVertexPtrThetas {
		bool operator ()(GeoVertex *lhs, GeoVertex *rhs) {
			return lhs->getBiangle()->theta < rhs->getBiangle()->theta;
		}
	};
	
	struct compareGeoVertexPtrPhis {
		bool operator ()(GeoVertex *lhs, GeoVertex *rhs) {
			return lhs->getBiangle()->phi < rhs->getBiangle()->phi;
		}
	};
	
};

#endif

