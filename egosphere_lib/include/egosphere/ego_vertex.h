/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef EGOSPHERE_VERTEX_H
#define EGOSPHERE_VERTEX_H

#include <egosphere/egosphere_utils.h>
#include <vector>

namespace egosphere {
	
	/** Basic information for an EgoSphere vertex. This class is meant
	 * to be subclassed, providing additional functionality as needed.
	 * See documentation in 'geodesic' header files for more, particularly
	 * 'geodesic.h' for structure(s) construction. */
	class EgoVertex {
	public:
		friend class Geosphere;
		EgoVertex();
		EgoVertex(double t, double p);
		EgoVertex(Biangle& b);
		EgoVertex(Point& p);
		EgoVertex(const EgoVertex& other);
		virtual ~EgoVertex();
		friend void swap(EgoVertex& first, EgoVertex& second);
		EgoVertex& operator=(EgoVertex other);
		
		unsigned int getId() const;
		Biangle* getBiangle() const;
		void getBiangle(Biangle& b) const;
		Point* getPoint(double radius=1.0) const;
		void getPoint(Point& p, double radius=1.0) const;
		/** Returns the number of neighbors (may be 5 or 6). */
		int numNeighbors() const;
		
		/** Check if vertex \a vid is a neighbor of this vertex (a vertex
		 * is never its own neighbor). Note that \a vid is a vertex ID,
		 * which is different than a neighbor index. */
		bool isNeighbor(unsigned int vid) const;
		
		/** Returns a neighbor identifier in the range [0, #neighbors) if
		 * \c ev is a neighbor, or -1 otherwise. Note that a vertex is
		 * never its own neighbor. */
		int isNeighbor(EgoVertex* ev) const;
		
		/** Return a pointer to neighbor number \a nix, or NULL if no such
		 * neighbor exists. Note that 0 <= \a nix < \c numNeighbors(), which
		 * is different than a vertex ID. */
		EgoVertex* getNeighborByIndex(int nix) const;
		
		/** Return a pointer to neighbor vertex \a nid, or NULL if no such
		 * neighbor exists. Note that \a nid is a vertex ID (bounded by the
		 * number of vertices in a particular egosphere), which is different
		 * than a neighbor number. */
		EgoVertex* getNeighborById(unsigned int nid) const;
		
		/** Return the neighbor number for vertex ID \a nid, or -1 if \a nid
		 * is not a neighbor. Note that 0 <= \a nix < \c numNeighbors(), which
		 * is different than a vertex ID. */
		int getNeighborNumber(unsigned int nid) const;
		
		/** Return the vertex ID of neighbor number \a nix. Throws an
		 * \c std::out_of_range exception unless 0 <= \a nix < numNeighbors. */
		unsigned int getNeighborId(int nix) const;
		
		/** Fill a vector with the \c Points of this vertex's neighbors.
		 * Utility function for the purposes of demarcating the
		 * neighborhood boundary in 3d Euclidian space. */
		void neighborPoints(std::vector<Point> &nvec) const;
		
		// TODO: I think the 'bearing' stuff should be removed from
		// this class and maintained either at a 'higher' level or
		// in a subclass (if desired)
		/** Return the \c bearing of neighbor \c ev to this vertex. Return
		 * values are in the range [-M_PI, M_PI] or NaN if \c ev is not a
		 * neighbor (a vertex is never its own neighbor). */
		double neighborBearing(EgoVertex* ev) const;
		
		/** Front-end to \c neighborBearing that accepts vertex ID \a nid
		 * as the parameter. Returns NAN if \a nid is not a neighbor (a
		 * vertex is never its own neighbor). Note a vertex ID is different
		 * than a neighbor number. */
		double neighborBearing(unsigned int nid) const;
		
		/** Front-end to \c neighborBearing that accepts neighbor number
		 * \a nix as the parameter. Returns NAN if \a nix < 0 or \a nix >=
		 * \c numNeighbors() (a neighbor number is different than a
		 * vertex ID). */
		double neighborBearing(int nix) const;
		
		// remove after debugging
		double distance(Biangle& other) const;
		
		std::string str() const;
		std::string str(int dec) const;
		std::string ang_str() const;
		std::string ang_str(int dec) const;
		std::string point_str() const;
		std::string point_str(int dec) const;
		std::string neigh_str() const;
		std::string neigh_str(int dec) const;
		
	protected:
		Biangle biangle;
		Point point;
		std::vector<EgoVertex*> neighbors;
		std::vector<double> neighbor_bearing;
		unsigned int vix;
		
	// private methods *only* called by friend Geosphere::getEgoStructs
	private:
		void setIndex(unsigned int ix);
		void addNeighbor(EgoVertex* ev);
		
	};

};
#endif

