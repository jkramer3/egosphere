/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef GEODESIC_METAINFO_H
#define GEODESIC_METAINFO_H

#include <string>

namespace egosphere {
	
	/** Meta-information about a constructed geosphere. */
	class GeosphereMetainfo {
	public:
		GeosphereMetainfo();
		std::string str() const;
		
		unsigned int freq, numVerts, numVoros;
		double minTheta, maxTheta, minPhi, maxPhi;
		double minVertD, maxVertD;
		double minVoroD, maxVoroD;
		double minVertVoroD, maxVertVoroD;
	};
	
};

#endif
