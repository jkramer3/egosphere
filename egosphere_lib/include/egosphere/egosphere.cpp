/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <egosphere/geodesic.h>
#include <iostream>

namespace egosphere {
	
	// NOTE: templated class! The hpp file includes this cpp!
	template<class T>
	Egosphere<T>::Egosphere(int f) {
		EgoVertex *baseT = new T(); // T *MUST BE* EgoVertex subclass
		Geosphere geo(f);
		m_freq = geo.frequency();
		GeosphereMetainfo mi = geo.getMetainfo();
		m_minAngle = mi.minVertD;
		m_maxAngle = mi.maxVertD;
		m_egoSearch = geo.getEgoStructs(m_egoVerts);
		//for (unsigned int i=0; i<m_egoVerts.size(); i++) {
		//	m_vertIxMap[m_egoVerts[i]] = i;
		//}
		std::cout << "Created Egosphere: frequency of "<< m_freq << ", ";
		std::cout << m_egoVerts.size() << " vertices" << std::endl;
		std::cout << "                   min/max angle: " << toDeg(m_minAngle);
		std::cout << ", " << toDeg(m_maxAngle) << " degrees" << std::endl;
		delete baseT;
	}
	
	template<class T>
	Egosphere<T>::~Egosphere() {
		while (!m_egoVerts.empty()) {
			delete m_egoVerts.back();
			m_egoVerts.pop_back();
		}
	}
	
	template<class T>
	int Egosphere<T>::frequency() const {
		return m_freq;
	}
	
	template<class T>
	int Egosphere<T>::size() const {
		return m_egoVerts.size();
	}
	
	template<class T>
	double Egosphere<T>::getMinAngle() const {
		return m_minAngle;
	}
	
	template<class T>
	double Egosphere<T>::getMaxAngle() const {
		return m_maxAngle;
	}
	
	template<class T>
	Biangle* Egosphere<T>::getBiangleOf(unsigned int id) const {
		if (!isId(id)) return NULL;
		return m_egoVerts[id]->getBiangle();
	}
	
	template<class T>
	bool Egosphere<T>::getBiangleOf(unsigned int id, Biangle &b) const {
		bool retval = false;
		if (!isId(id)) {
			b.invalidate();
		} else {
			m_egoVerts[id]->getBiangle(b);
			retval = true;
		}
		return retval;
	}
	
	template<class T>
	Point* Egosphere<T>::getPointOf(unsigned int id, double radius) const {
		if (!isId(id) || radius <= 0.0) return NULL;
		return m_egoVerts[id]->getPoint(radius);
	}
	
	template<class T>
	bool Egosphere<T>::getPointOf(unsigned int id, Point &p, double radius) const {
		bool retval = false;
		if (!isId(id) || radius <= 0.0) {
			p.invalidate();
		} else {
			m_egoVerts[id]->getPoint(p, radius);
			retval = true;
		}
		return retval;
	}
	
	template<class T>
	unsigned int Egosphere<T>::nearest(Biangle const &b) const {
		return m_egoSearch.nearest(b);
	}
	
	template<class T>
	unsigned int Egosphere<T>::nearest(Point const &p) const {
		return m_egoSearch.nearest(p);
	}
	
	template<class T>
	int Egosphere<T>::numNeighbors(int id) const {
		if (!isId(id)) return -1;
		return m_egoVerts[id]->numNeighbors();
	}
	
	template<class T>
	std::vector<int> Egosphere<T>::getNeighbors(int id) const {
		std::vector<int> vec;
		if (isId(id)) {
			T *v = m_egoVerts[id], *n;
			for (int i=0; i<v->numNeighbors(); i++) {
				n = (T*)v->getNeighborByIndex(i);
				vec.push_back((int)n->getId());
			}
		}
		return vec;
	}
	
	template<class T>
	bool Egosphere<T>::isNeighbor(int id1, int id2) const {
		bool retval = false;
		if (isId(id1) && isId(id2)) {
			T* v = m_egoVerts[id1];
			retval = (v->isNeighbor(m_egoVerts[id2]) >= 0);
		}
		return retval;
	}
	
	template<class T>
	double Egosphere<T>::getNeighborBearing(int v_id, int n_id) const {
		double retval = NAN;
		if (isId(v_id) && isId(n_id)) {
			T* v = m_egoVerts[v_id];
			retval = v->neighborBearing(m_egoVerts[n_id]);
		}
		return retval;
	}
	
	template<class T>
	int Egosphere<T>::getNeighborClosestToBearing(int id, double bearing) const {
		int n_id = -1, nix = -1;
		if (isId(id)) {
			double tmp, low = M_PI;
			T* v = m_egoVerts[id];//, *n;
			for (int i=0; i<v->numNeighbors(); i++) {
				tmp = v->neighborBearing(i);
				if (fabs(angleBetween(tmp, bearing)) < low) {
					low = tmp;
					nix = i;
				}
			}
			if (nix > -1) {
				// TODO: this is ugly, modify the EgoVertex API to avoid
				// the 'm_egoSearch.nearest' call
				//EgoVertex* n = v->getNeighbor(nix);
				//Biangle b;
				//n->getBiangle(b);
				//n_id = m_egoSearch.nearest(b);
				n_id = (int)v->getNeighborId(nix);
			}
		}
		return n_id;
	}
	
	template<class T>
	EgospherePath Egosphere<T>::getPath(unsigned int id1, unsigned int id2,
	                                    bool dups) const {
		Biangle b1, b2;
		if (!getBiangleOf(id1, b1)) {
			EgospherePath path;
			return path;
		}
		if (!getBiangleOf(id2, b2)) {
			EgospherePath path;
			return path;
		}
		EgospherePath path(*this, getPath(b1, b2), dups);
		return path;
	}
	
	template<class T>
	EgoPath Egosphere<T>::getPath(const Biangle &b1, const Biangle &b2) const {
		EgoPath path = Utils::getPath(&b1, &b2, m_minAngle);
		// TODO: winnow path to remove biangles with same nearest vertex
		return path;
	}
	
	template<class T>
	EgoPath Egosphere<T>::getPath(const StampedBiangle &b1,
	                              const StampedBiangle &b2) const {
		EgoPath path = Utils::getPath(&b1, &b2, m_minAngle);
		// TODO: winnow path to remove biangles with same nearest vertex
		return path;
	}
	
	template<class T>
	void Egosphere<T>::getPathHelper(const Biangle *b1, const Biangle *b2,
	                   std::list<unsigned int> &l) const {
		unsigned int id1=nearest(*b1), id2=nearest(*b2);
		//std::cout << "getPathHelper: id1=" << id1 << " (" << b1->str() << "), id2=" << id2 << " (" << b2->str() << ")" << std::endl;
		//if (hypot(*b1, *b2) < m_maxAngle) {
		if (id1 == id2) {
			//std::cout << "  id1 == id2! pushing " << id1 << std::endl;
			l.push_back(id1);
		} else if (isNeighbor(id1, id2)) {
			//l.push_back(nearest(*b1));
			//l.push_back(nearest(*b2));
			//std::cout << "  id1, id2 neighbors! pushing " << id1 << ", " << id2 <<std::endl;
			l.push_back(id1);
			l.push_back(id2);
		} else {
			std::list<unsigned int> lL, lR;
			//std::cout << "  midpoint: ";
			// TODO: use 'midpoint' or 'getBiangleOf(nearest(midpoint))'?
			Biangle *bm = Utils::midpoint(b1, b2);
			//std::cout << "(" << bm->str() << ")" << std::endl;
			Biangle *b3 = getBiangleOf(nearest(*bm));
			//std::cout << "  nearest (" << b3->str() << ")" << std::endl;
			//unsigned int idm = nearest(*bm);
			getPathHelper(b1, b3, lL);
			l.insert(l.begin(), lL.begin(), lL.end());
			getPathHelper(b3, b2, lR);
			// note: use ++lR.begin() as 1st element is same as lL.end()
			l.insert(l.end(), ++lR.begin(), lR.end());
			delete b3;
			delete bm;
		}
	}
	
	template<class T>
	void Egosphere<T>::getPath(unsigned int id1, unsigned int id2,
	                           std::list<unsigned int> &l) const {
		l.clear();
		if (id1 == id2) {
			l.push_back(id1);
		} else {
			Biangle *b1 = getBiangleOf(id1);
			Biangle *b2 = getBiangleOf(id2);
			getPathHelper(b1, b2, l);
			delete b1;
			delete b2;
		}
	}
	
	template<class T>
	void Egosphere<T>::getPath(const Biangle &b1, const Biangle &b2,
	                           std::list<unsigned int> &l) const {
		unsigned int id1 = nearest(b1); 
		unsigned int id2 = nearest(b2);
		getPath(id1, id2, l);
	}
	
	template<class T>
	bool Egosphere<T>::isId(int id) const {
		return (id >= 0 && id < (int)m_egoVerts.size());
	}
	
};

