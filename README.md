# README #

Repository containing the code for the `egosphere`, a data structure
based on a geodesic sphere used for storing spatio-temporal data.
* [Background](#background)
* [Pre-requisite software](#pre-requisite-software)
* [ROS Workspace](#ros-workspace)
* [Cloning](#cloning)
* [Installation](#installation)
* [Examples](#examples)

## Background
Development of the egosphere began with the DARPA Robotics Challenge, with the intent of using it as the basis for gesture recognition used to control a robot in virtual reality. While a rudimentary gesture recognition setup was successfully developed, the VR supporting software needed was extensive and the VR platforms had not yet reached an appropriate level of maturity (Oculus DK1; this was back in 2013). And then, the project ended (the TRACLabs team ultimately placed 9th in the Finals). But the egosphere is a standalone data structure, designed to be extensible for other uses (e.g., sensor fusion, navigation, episodic memory).

An _egosphere_ is
a data structure that represents 3d space as a 2d spherical surface
centered on a particular coordinate origin that is able to maintain
spatio-temporal relationships of arbitrary data in an environment.
Points on the surface, referred to as _vertices_, are indexed
using _azimuth_ and _elevation_ angles (&theta;, &phi;)
from their polar coordinate representation.

A desirable egosphere characteristic is a spatially uniform
division of external space among vertices, to group nearby
objects with a single vertex. This is equivalent to a uniform
distribution of vertices on the sphere's surface; although
there is no known general solution to this _point packing_
problem, the scheme for vertex distribution here relies on
a _geodesic sphere_ created by subdividing the triangular
faces of an icosahedron into smaller triangles.

As a data structure, an egosphere must supply efficient search
capabilities for both _storage_ and _recall_.
Data storage requires finding the vertex closest to particular
(&theta;, &phi;)$ values, accomplished by a nearest neighbor search.
Data recall, on the other hand, consists of locating
one or more specific vertices, either for the relative
location as an area of interest itself or due to the
associations with one or more particular items.

## Pre-requisite software
A standard ROS desktop installation should be sufficient.

## ROS Workspace:
If you do not have an existing ROS workspace, create one. To follow the ROS tutorial directions, see: [Installing and Configuring ROS Environment](http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment#Create_a_ROS_Workspace) page. Our lab has a slightly different convention, essentially assuming complex projects that clone multiple already-configured repositories. To replicate, first add the following to your `.bashrc`:

```bash
# catkin shell function; change 'kinetic' hard-coding as needed
catkin() {
    if [[ $1 == "config" ]]; then
        command catkin config -DCMAKE_BUILD_TYPE=Release --extend /opt/ros/kinetic
    else
        command catkin "$@"
    fi
}
```

Then, create a workspace directory, initialize it, create the `src` directory, and clone desired ROS packages/repositories. Note that building 
```bash
$ mkdir -p project_ws/src
$ cd project_ws
$ catkin init
$ cd src
# proceed to clone ROS packages/repositories
```

## Cloning:
Clone this repository into a ROS workspace with the command (assumes you have SSH keys set up):
```bash
git clone git@gitlab.com:jkramer3/egosphere.git
```

## Installation
It is expected that the egosphere will be incorporated into larger projects; it is just a data structure, and is built as a library. Include it as you would any other ROS package into your ROS workspace. After cloning, follow the ROS tutorial directions for [Building Your Package](http://wiki.ros.org/ROS/Tutorials/BuildingPackages#ROS.2FTutorials.2Fcatkin.2FBuildingPackages.Building_Your_Package) or, if using the `catkin()` function from above, change to the workspace directory and build:
```bash
# assumes current directory is 'project_ws/src'
$ cd ..
$ catkin build
```

To confirm successful compilation and operation, the provided test program
can be run, which creates an egosphere and supporting search structures,
prints meta-information about the construction to the console, then executes
and confirms 10,000 searches of random (theta, phi) values. Using default
values, the tests should run in a fraction of a second.
```bash
# default test execution
$ rosrun egosphere_lib geosphere_test
```
```bash
# run test using frequency 4 output to ego_4.xyz and ego_4.xyzf
$ rosrun egosphere_lib geosphere_test -f 4 -o ego
```
The `-f` switch sets the egosphere &quot;frequency&quot; (the number of
divisions of triangle sides), while the `-o` switch writes 2 files to disk
containing vertex location information. By default, the frequency is set
to 2 and the files are not written.

## Examples
A few demonstrative example applications are included (the `egosphere` package
must be installed/recognized/built in your ROS workspace). These were created
to explore various data representations and structure, while producing useful
code; as such, they should be considered beta-level software: mostly stable,
but not fully tested, and likely should not be considered as templates for
subsequent code!

### RViz Projection
This is a visualization tool I used for developing a gesture recognition
system, tracking projections from the egosphere center to the sphere's
surface. While implementing, I thought it would be fun to put in a timer
that detected inactivity and went into &quot;screen saver&quot; mode.

To run the `RViz Projection` demo, type the following:
```bash
$ roslaunch egosphere_lib rviz_projection.launch
```

At startup, an egosphere with a frequency of 12 and a radius of 1.0 is
displayed. After a short amount of time (~10s), "screen saver" mode will
be engaged, which cycles through some internally defined algorithms that
highlight various egosphere "faces". "Screen saver" mode is interrupted
when external input (that does not move the egosphere) is received. Some
things to try:
#### Try the following from the command-line to display a path on the surface:
```bash
$ rostopic pub /rviz_projection/path egosphere_msgs/CoordinateStampedArray "coords: [ {sec: 0, nsec: 0, coord: {theta: 1.5, phi: 0.0, mag: 0.0}}, {sec: 4, nsec: 0, coord: {theta: -1.5, phi: 0.1, mag: 0.0}}]" -1
```  
#### Try the following from the command-line to re-center the egosphere:
```bash
$ rostopic pub /rviz_projection/center_pose geometry_msgs/Pose "{position: {x: 0.0, y: -1.0, z: -0.5}, orientation: {x: 0.0, y: 0.0, z: 0.0, w: 1.0}}" -1
```
#### Try the following from the command-line to rotate the egosphere 45deg:
```bash
$ rostopic pub /rviz_projection/center_pose geometry_msgs/Pose "{position: {x: 0.0, y: -1.0, z: -0.5}, orientation: {x: 0.0, y: 0.0, z: 0.3827, w: -0.9239}}" -1
```

### External Item Tracking
This is proof-of-concept code for using an egosphere to store data about
and track external objects.  To run the `External Item Tracking` demo,
type the following:
```bash
$ roslaunch egosphere_lib external.launch
```

At startup, an egosphere with a frequency of 8 and a radius of 1.0 is
displayed, with 3 arrows indicating the orientation (following ROS
conventions, red is forward, green is left, and blue is up). ROS messages
can be sent that register external items with the egosphere to track. When
"registered", a cylinder representing the item will appear, with "tracking"
represented in 3 ways:
1. an arrow will extend from the egosphere center to the item
2. a small sphere will be displayed where the ray crosses the egosphere
surface
3. the egosphere face containing the point where the ray crosses the
egosphere surface is highlighted.

Note that external objects within the egosphere radius will not be
tracked. Some things to try:
#### Try the following from the command-line for registering the external frame `ext` with the egosphere (and begin TF broadcasting):
```bash
$ rostopic pub /feature_external/external_add std_msgs/String "data: 'cylinder'" -1
```
#### Then, change the pose of `cylinder`:
```bash
$ rostopic pub /feature_external/external_location geometry_msgs/PointStamped "{header: {seq: 0, stamp: {secs: 0, nsecs: 0}, frame_id: 'cylinder'}, point: {x: 1.0, y: 1.0, z: 0.25}}" -1
```

The eventual goal is to have multiple items (i.e., frames) that can be
added and moved dynamically as desired.

### Who do I talk to? ###
Jim Kramer, jkramer00uh05nd@gmail.com
